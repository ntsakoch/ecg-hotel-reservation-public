//package za.org.ecg.service;
//
//import org.junit.*;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.ApplicationContext;
//import za.org.ecg.bookings.BookingStatus;
//import za.org.ecg.dao.entity.*;
//import za.org.ecg.dao.exception.EcgDAOException;
//import za.org.ecg.security.increption.EncryptionHandler;
//import za.org.ecg.springmvc.service.*;
//import za.org.ecg.test.BaseTestCase;
//import za.org.ecg.utils.DateUtils;
//
//import java.text.ParseException;
//
//import java.util.HashSet;
//import java.util.List;
//import java.util.Set;
//
//
///**
// * Created by F4817273 on 2016/09/23.
// */
//public class CustomerBookingServiceTest extends BaseTestCase {
//
//    @Autowired
//    private CustomerBookingService customerBookingService;
//
//    private CustomerPreferenceService customerPreferenceService;
//    private RoomService roomService ;
//    private PreferenceService preferenceService;
//    private ApplicationPermissionService applicationPermissionService;
//    private CurrencyService currencyService;
//
//    @BeforeClass
//    public static void setUpClass() {
//        BaseTestCase.setUpClass();
//    }
//
//    @Before
//    public void setUp() {
//        super.setup();
//        System.out.printf("At Setup....");
//        customerBookingService=(CustomerBookingService)getBean("customerBookingService");
//        customerPreferenceService=(CustomerPreferenceService)getBean("customerPreferenceService");
//        roomService=(RoomService)getBean("roomService");
//        preferenceService=(PreferenceService)getBean("preferenceService");
//        applicationPermissionService=(ApplicationPermissionService)getBean("applicationPermissionService");
//        currencyService=(CurrencyService)getBean("currencyService");
//    }
//
//    @After
//    public void tearDown() {
//        super.tearDown();
//    }
//
//    @AfterClass
//    public static void tearDownClass() {
//    }
//
//
//    @Test
//    @Ignore
//    public void testCanFindAllOutstanding()
//    {
//        try {
//            System.out.println("**********************Finding all outstanding********************");
//            for (CustomerBooking booking : customerBookingService.findAllOutstandingBookings(340d)) {
//                System.out.println("Booking: " + booking);
//            }
//        }
//        catch (EcgDAOException exc) {
//            exc.printStackTrace();
//        }
//    }
//
//    @Test
//    @Ignore
//    public void testFindByCustomerIdNumber()
//    {
//        try {
//            System.out.println("*****************Finding by id number***********************");
//            for (CustomerBooking booking : customerBookingService.findByCustomerIdNumber("9011023215081")) {
//                System.out.println("Booking: " + booking);
//            }
//        }
//        catch (EcgDAOException exc) {
//            exc.printStackTrace();
//        }
//    }
//
//    @Test
//    @Ignore
//    public void testCreateBooking()
//    {
//        try {
//            double totalCost=0d;
//            Room roomToBook=roomService.findRoomById(1);
//            User customerDetails = new User();
//            CustomerAccount customerAccount=new CustomerAccount();
//            customerAccount.setCurrency(currencyService.findCurrencyById(1));
//            customerAccount.setAvailableBalance(0);
//
//            customerDetails.setFirstName("Ntsako");
//            customerDetails.setLastName("Test Booking ");
//            customerDetails.setDateOfBirth(DateUtils.getInstance().parse("02/11/1990",""));
//
//            PostalAddress pa = new PostalAddress();
//            pa.setAddress("PO BOX 3319");
//            pa.setCity("GIYANI");
//            pa.setPostalCode("0826");
//
//            PhysicalAddress pha = new PhysicalAddress();
//            pha.setAddressLine1("Orlando West");
//            pha.setCity("Joborg");
//            pha.setSuburb("Orlando");
//            pha.setStreetName("Vilakazi");
//            pha.setStreetNumber("254");
//            pha.setPostalCode("1430");
//
//
//            customerDetails.setPostalAddress(pa);
//            customerDetails.setPhysicalAddress(pha);
//
//            pa.setUserInfo(customerDetails);
//            pha.setUserInfo(customerDetails);
//
//            UserCredential credentials=new UserCredential();
//            credentials.setActive(true);
//            credentials.setRetryCount(0);
//
//            credentials.setUsername("booking three");
//            credentials.setPassword(EncryptionHandler.getInstance().encrypt("1234"));
//            customerDetails.setUserCredential(credentials);
//            credentials.setUserInfo(customerDetails);
//
//            Set<UserPermission> userPermissions=new HashSet<>();
//            UserPermission userPermission=new UserPermission();
//
//            userPermission.setApplicationPermission(applicationPermissionService.findApplicationPermissionById(3));
//            userPermission.setUserInfo(customerDetails);
//            userPermissions.add(userPermission);
//
//            userPermission=new UserPermission();
//            userPermission.setUserInfo(customerDetails);
//            userPermission.setApplicationPermission(applicationPermissionService.findApplicationPermissionById(4));
//            userPermissions.add(userPermission);
//
//
//            customerDetails.setUserPermissions(userPermissions);
//            Customer customer=new Customer();
//
//            customer.setCustomerDetails(customerDetails);
//
//            List<Preference> preferences=preferenceService.findAllPreferences();
//
//            CustomerBooking booking=new CustomerBooking();
//            Set<CustomerPreference> customerPreferences=new HashSet<>();
//            for(Preference preference:preferences)
//            {
//                CustomerPreference customerPreference=new CustomerPreference();
//                customerPreference.setPreferenceDetails(preference);
//                customerPreferences.add(customerPreference);
//                totalCost+=preference.getPreferenceCost().getAmount();
//            }
//            totalCost+=roomToBook.getRoomCost().getAmount();
//
//            for(RoomFeature roomFeature: roomToBook.getFeatures())
//            {
//                Feature feature=roomFeature.getFeature();
//                totalCost+=feature.getFeatureCost().getAmount();
//            }
//            System.out.printf("Booking Cost: "+totalCost );
//            customerAccount.setAvailableBalance(totalCost-(totalCost*2));
//            customerAccount.setOutstandingBalance(totalCost);
//            customer.setAccount(customerAccount);
//
//            customerAccount.setCustomerDetails(customer);
//
//            Set<BookingCheckin> bookingCheckins=new HashSet<>();
//
//            BookingCheckin bookingCheckin=new BookingCheckin();
//            bookingCheckin.setCustomerBooking(booking);
//
//            bookingCheckin.setCustomerBooking(booking);
//            bookingCheckin.setFullName("Ntsako");
//            bookingCheckin.setIdentityNumber("3214569874587");
//            bookingCheckin.setCheckInStatus("Booking awaiting approval");
//            bookingCheckins.add(bookingCheckin);
//
//
//
//
//            //  CustomerInvoice invoice=new CustomerInvoice();
//            //  invoice.setDateCreated(DateParser.getInstance().parse("2016/09/27 15:44:05","yyyy/MM/dd HH:mm:ss"));
//
//
//            booking.setStatus("incomplete");
//            booking.setCheckinDate(DateUtils.getInstance().parse("2016/09/27",""));
//            booking.setCheckoutDate(DateUtils.getInstance().parse("2016/09/30",""));
//            booking.setDateCreated(DateUtils.getInstance().parse("2016/09/27 15:44:05","yyyy/MM/dd HH:mm:ss"));
//            booking.setRoomId(roomToBook.getRoomId());
//
//            booking.setBookedRoom(roomToBook);
//            booking.setPreferences(customerPreferences);
//            booking.setCheckIns(bookingCheckins);
//
//
//            booking.setCustomer(customer);
//
//
//         /*   System.out.println(booking);
//            //System.out.println(customerDetails);
//           //System.out.println(customer);
//            System.out.println(bookingCheckins);
//          //  System.out.println(userPermissions);
//            System.out.println(bookedRoom);*/
//
//            Integer bookingId=customerBookingService.saveCustomerBooking(booking);
//
//            System.out.println("generated booking id: "+bookingId);
//
//        } catch (ParseException | EcgDAOException exc) {
//            exc.printStackTrace();
//        }
//
//    }
//
//    @Test
//    public void testCanRetrieveUnAvailableRooms()
//    {
//        try {
//            System.out.println("*****************testCanRetrieveUnAvailableRooms()***********************");
//            List<Room> unAvailableRooms=roomService.getAllAvailableRooms(DateUtils.getInstance().parse("2017/03/02",""),
//                    DateUtils.getInstance().parse("2017/03/10",""),1,3);
//            for(Room room:unAvailableRooms)
//            {
//                System.out.println(room);
//                System.out.println(room.getRoomLocation().getLocationAddress());
//                System.out.println(room.getRoomLocation().getLocationAddress().getCountryCity());
//                System.out.println(room.getAdultCost());
//                System.out.println(room.getChildrenCost());
//                System.out.println(room.getRoomCost());
//               // System.out.println();
//            }
//            System.out.println("Status: "+ BookingStatus.CHECKED_OUT);
//        } catch (EcgDAOException e) {
//            e.printStackTrace();
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//    }
//}
