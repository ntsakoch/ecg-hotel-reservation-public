///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package za.org.ecg.service;
//
//import org.apache.log4j.xml.DOMConfigurator;
//import org.junit.*;
//import za.org.ecg.BaseTestCase;
//import za.org.ecg.ECGHotelBooking;
//import za.org.ecg.dao.cache.EntityCacheException;
//import za.org.ecg.dao.entity.*;
//import za.org.ecg.dao.exception.EcgDAOException;
//import za.org.ecg.security.increption.EncryptionHandler;
//import za.org.ecg.utils.DateParser;
//
//import java.util.HashSet;
//import java.util.List;
//import java.util.Set;
//
//import static org.junit.Assert.assertEquals;
//
///**
// *
// * @author F4817273
// */
//public class CustomerServiceTest extends BaseTestCase {
//
//
//    @BeforeClass
//    public static void setUpClass() {
//        DOMConfigurator.configure(ECGHotelBooking.class.getResource("/za/org/ecg/log4j.xml"));
//    }
//
//    @Before
//    public void setup() {
//    }
//
//    @After
//    public void tearDown() {
////
//    }
//
//    @AfterClass
//    public static void tearDownClass() {
//       // hibernateSessionUtil.destroySession();
//    }
//
//    @Test
//    public void canStartHibernateSession() {
//        assertEquals("failed to start hibernate session", hibernateSessionUtil.isInitialized(), true);
//    }
//
//    @Test
//    //@Ignore
//    public void canLoadAllCustomers() {
//        try {
//            List<Customer> result = customerManager.findAll();
//            System.out.println("*************************findAllCustomers*******************************");
//            for (Customer customer : result) {
//               // Customer customer = (Customer) entity;
//
//                if (customer.getCustomerDetails() != null) {
//                    System.out.println(customer.getCustomerId());
//                    System.out.println(customer.getCustomerDetails().getFirstName());
//                    System.out.println(DateParser.getInstance().format(customer.getCustomerDetails().getDateOfBirth(),""));
//                    if (customer.getCustomerDetails().getPhysicalAddress() != null) {
//                        System.out.println(customer.getCustomerDetails().getPhysicalAddress().getAddressLine1());
//                    }
//                    if (customer.getCustomerDetails().getPostalAddress() != null) {
//                        System.out.println(customer.getCustomerDetails().getPostalAddress().getAddress());
//                    }
//
//                    System.out.println("Number of bookings: " + customer.getBookings().size());
//                }
//
//                for (CustomerBooking booking : customer.getBookings()) {
//                    System.out.println("Booking From: " + booking.getCheckinDate() + "-" + booking.getCheckoutDate());
//                }
//            }
//            assertEquals("Failed to retrive customers from db", !result.isEmpty(), true);
//        }
//        catch (EcgDAOException exc) {
//            exc.printStackTrace();
//        }
//
//    }
//
//     @Test
//    //@Ignore
//    public void canAddCustomer() {
//        try {
//            Customer customer=new Customer();
//            User user = new User();
//
//            user.setFirstName("Customer");
//            user.setLastName("MyTest");
//           // user.setDateOfBirth("02/11/1990");
//            user.setUserId(100);
//
//            PostalAddress pa = new PostalAddress();
//            pa.setAddress("PO BOX 3319");
//            pa.setCity("GIYANI");
//            pa.setPostalCode("0826");
//
//            PhysicalAddress pha = new PhysicalAddress();
//            pha.setAddressLine1("Orlando West");
//            pha.setCity("Joborg");
//            pha.setSuburb("Orlando");
//            pha.setStreetName("Vilakazi");
//            pha.setStreetNumber("254");
//            pha.setPostalCode("1430");
//
//
//            user.setPostalAddress(pa);
//            user.setPhysicalAddress(pha);
//
//            pa.setUserInfo(user);
//            pha.setUserInfo(user);
//
//            customer.setCustomerDetails(user);
//
//            UserCredential credentials=new UserCredential();
//            credentials.setActive(1);
//            credentials.setRetryCount(0);
//
//            credentials.setUsername("ntsakoch");
//            credentials.setPassword(EncryptionHandler.getInstance().encrypt("12345"));
//            user.setUserCredential(credentials);
//            credentials.setUserInfo(user);
//
//            Set<UserPermission> userPermissions=new HashSet<UserPermission>();
//            UserPermission permission=new UserPermission();
//
////            permission.setApplicationPermission((ApplicationPermission)applicationPermissionDAO.findById(2));
//     //       permission.setUserInfo(user);
//         //   userPermissions.add(permission);
//
//     //       permission=new UserPermission();
//      //      permission.setUserInfo(user);
//        //    permission.setApplicationPermission((ApplicationPermission)applicationPermissionDAO.findById(1));
//        //    userPermissions.add(permission);
//
//
//            user.setUserPermissions(userPermissions);
//            customerManager.merge(customer);
//
//        } catch (EcgDAOException|EntityCacheException exc) {
//            exc.printStackTrace();
//        }
//    }
//
//    @Test
//    public void mustDeleteCustomer()
//    {
//        try {
//
//            Customer customer =  customerManager.findById(8);
//            customerManager=new CustomerManager();
//            customerManager.remove(customer);
//        }
//        catch (Exception exc) {
//            exc.printStackTrace();
//        }
//    }
//}
