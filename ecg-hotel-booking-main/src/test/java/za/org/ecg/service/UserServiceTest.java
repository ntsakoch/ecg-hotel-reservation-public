///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package za.org.ecg.service;
//
//import org.junit.*;
//import org.springframework.beans.factory.annotation.Autowired;
//import za.org.ecg.dao.cache.EntityCacheException;
//import za.org.ecg.dao.cache.EntityCacheManagerProvider;
//import za.org.ecg.dao.entity.*;
//import za.org.ecg.dao.exception.EcgDAOException;
//import za.org.ecg.security.increption.EncryptionHandler;
//import za.org.ecg.springmvc.service.UserService;
//import za.org.ecg.test.BaseTestCase;
//
//import java.util.*;
//
///**
// *
// * @author F4817273
// */
//
//public class UserServiceTest  extends BaseTestCase {
//
//    @Autowired
//     private  UserService userService;
//
//    @BeforeClass
//    public static void setUpClass() {
//        System.out.println("Setup");
//        BaseTestCase.setUpClass();
//    }
//
//    @Before
//    public void setUp() {
//        System.out.println("\nAt setup");
//        super.setup();
//       userService=(UserService)getBean("userServiceProxy");
//    }
//
//    @After
//    public void tearDown() {
//    }
//
// @Test
// public void testInitCache()
// {
//     try {
//         EntityCacheManagerProvider.getInstance().getCacheManager().init();
//     } catch (EntityCacheException e) {
//         e.printStackTrace();
//     }
// }
//    @Test
//    @Ignore
//    public void testCanLoadAllUsers() {
//        try {
//            System.out.println("*************************findAllUsers********************************");
//            userService.authenticateUser("ntsakoch",EncryptionHandler.getInstance().encrypt("1234"));
//            Thread concurrentTasks=new Thread(new Runnable() {
//                @Override
//                public void run() {
//                    try {
//                        userService.authenticateUser("ntsako",EncryptionHandler.getInstance().encrypt("1234"));
//                        System.out.println("From Cache: "+userService.findUserById(2));
//                        assertEquals("Failed to retrieve users from db", true, true);
//                    } catch (EcgDAOException e) {
//                        e.printStackTrace();
//                    }
//
//                }
//            });
//           concurrentTasks.start();
//            Date startTime=new Date();
//            System.out.println("start time: "+startTime);
//            List<User> result = userService.findAllUsers();
//            Date endTime=new Date();
//            System.out.println("End Time: "+endTime+", difference: "+(startTime.getTime()-endTime.getTime()));
//            for (User user: result) {
//                System.out.println(user.getUserId() + ": " + user.getFirstName());
//                if (user.getPhysicalAddress() != null) {
//                    System.out.println(user.getPhysicalAddress().getAddressLine1());
//                }
//                if (user.getPostalAddress() != null) {
//                    System.out.println(user.getPostalAddress().getAddress());
//                }
//                for (UserPermission permission : user.getUserPermissions()) {
//                    System.out.println("permission: " + permission.getApplicationPermission().getDescription());
//                }
//            }
//
//        }
//        catch (EcgDAOException exc)
//        {
//            exc.printStackTrace();
//        }
//    }
//
//    @Test
//    @Ignore
//    public void canLoadUser() {
//        try {
//            User user = userService.findUserById(1);
//            if (user != null) {
//                System.out.println("**********************loadUser******************************");
//                System.out.println(((User) user).getFirstName());
//                System.out.println(((User) user).getPhysicalAddress().getAddressLine1());
//                System.out.println(((User) user).getPostalAddress().getAddress());
//            }
//            assertEquals("Failed to retrive user from db", user != null, true);
//        }
//        catch (EcgDAOException exc)
//        {
//           exc.printStackTrace();
//        }
//    }
//
//    @Test
//    @Ignore
//    public void testCanRunSQLQuery() {
//        try {
//            String query = "select user_id,first_name,last_name from user where user_id=:userid or last_name like :surname";
//            Map<String, Object> params = new HashMap<String, Object>();
//            params.put("userid", 1);
//            params.put("surname", "%Chabalala%");
//            List<Object[]> result = userService.executeSQLQuery(query, params);
//            System.out.println("****************runsqlQuery*****************");
//
//            for (Object[] data : result) {
//                System.out.println(data[0] + ": " + data[1] + " " + data[2]);
//            }
//            assertEquals("failed to run sql query:", !result.isEmpty(), true);
//        }
//        catch (EcgDAOException exc)
//        {
//            exc.printStackTrace();
//        }
//    }
//
//    @Test
//    @Ignore
//    public void testAddUser() {
//        try {
//            User user = new User();
//
//            user.setFirstName("Tsakani");
//            user.setLastName("MyTest");
//           // user.setDateOfBirth("02/11/1990");
//            user.setUserId(0);
//
//            PostalAddress pa = new PostalAddress();
//            pa.setAddress("PO BOX 3319");
//            pa.setCity("GIYANI");
//            pa.setPostalCode("0826");
//
//            PhysicalAddress pha = new PhysicalAddress();
//            pha.setAddressLine1("Orlando West");
//            pha.setCity("Joborg");
//            pha.setSuburb("Orlando");
//            pha.setStreetName("Vilakazi");
//            pha.setStreetNumber("254");
//            pha.setPostalCode("1430");
//
//
//            user.setPostalAddress(pa);
//            user.setPhysicalAddress(pha);
//
//            pa.setUserInfo(user);
//            pha.setUserInfo(user);
//
//            UserCredential credentials=new UserCredential();
//            credentials.setActive(true);
//            credentials.setRetryCount(0);
//
//            credentials.setUsername("ntsakoch");
//            credentials.setPassword(EncryptionHandler.getInstance().encrypt("12345"));
//            user.setUserCredential(credentials);
//            credentials.setUserInfo(user);
//
//            Set<UserPermission> userPermissions=new HashSet<UserPermission>();
//            UserPermission permission=new UserPermission();
//
//          //  permission.setApplicationPermission((ApplicationPermission)applicationPermissionDAO.findById(2));
//            permission.setUserInfo(user);
//            userPermissions.add(permission);
//
//            permission=new UserPermission();
//            permission.setUserInfo(user);
//           // permission.setApplicationPermission((ApplicationPermission)applicationPermissionDAO.findById(1));
//            userPermissions.add(permission);
//
//
//         //   user.setUserPermissions(userPermissions);
//
//            Integer id=userService.saveUser(user);
//            System.out.println("Generated ID: "+id);
//
//        } catch (Exception hexc) {
//            hexc.printStackTrace();
//        }
//    }
//
//    @Test
//    @Ignore
//    public void canLogin()
//    {
//        try {
//            User user = userService.authenticateUser("ntsakoch", "112345");
//            System.out.println(user.getUserId());
//            if (user.getPhysicalAddress() != null) {
//                System.out.println(user.getPhysicalAddress().getAddressLine1());
//                System.out.println(user.getPostalAddress().getAddress());
//            }
//        }
//     catch (EcgDAOException exc) {
//        exc.printStackTrace();
//    }
//    }
//
//    @Test
//    @Ignore
//    public void canCount() {
//        try {
//            System.out.println("*****************countUsers********************");
//            int expeceted = 3;
//            long actual = userService.countUsers();
//            System.out.println("countUsers: " + actual);
//            assertEquals("countUsers mismatch", expeceted, actual);
//        }
//        catch (EcgDAOException exc) {
//            exc.printStackTrace();
//        }
//    }
//
//    @Test
//    @Ignore
//    public void canFindRange() {
//        try {
//            System.out.println("*****************findRange********************");
//            List<User> result = userService.findRange(1, 2);
//            for (User user : result) {
//          //      User user = (User) entity;
//                System.out.println(user.getFirstName() + " " + user.getLastName());
//
//            }
//            assertEquals("range overlapps", result.size(), 2);
//        }
//        catch (EcgDAOException exc) {
//            exc.printStackTrace();
//        }
//    }
//
//    @Test
//    @Ignore
//    public void canUpdate() {
//        try {
//            User user = userService.findUserById(new Integer(2));
//            user.setUserId(2);
//            user.setFirstName("User 2 Updated");
//            //User user = (User) entity;
//            user.setCitizenship("rsa");
//            user.setIdentityNumber("9011023215081");
//            user.setContactNumber("0834217701");
//
//            PostalAddress pa = new PostalAddress();
//            pa.setAddress("PO BOX 3319");
//            pa.setCity("GIYAN");
//            pa.setPostalCode("0826");
//
//            PhysicalAddress pha = user.getPhysicalAddress();
//            pha.setAddressLine1("Orlando West");
//            pha.setCity("Johannesburg");
//            pha.setSuburb("456 Orlando");
//            pha.setStreetName("Vilakazi");
//            pha.setStreetNumber("254");
//            pha.setPostalCode("1430");
//
//            //user.setPostalAddress(pa);
//            //pa.setUserId(user.getUserId());
//           // user.setPhysicalAddress(pha);
//            userService.update(user);
//        }
//        catch (EcgDAOException exc) {
//            exc.printStackTrace();
//        }
//    }
//
//    @Test
//    @Ignore
//    public void mustDeleteUser() {
//        try {
//            //EntityBean entity = userDAO.findById(new Integer(12));
//            User user =  userService.findUserById(new Integer(1));
//
////        System.out.println("\nF Name: "+user.getUserCredential().getUserId());
//            //  user.getUserPermissions().clear();
//            userService.remove(user);
//
//        }
//        catch (EcgDAOException exc) {
//            exc.printStackTrace();
//        }
//
//    }
//
//}
