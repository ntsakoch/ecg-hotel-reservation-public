package za.org.ecg.security.increption;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by F4817273 on 2016/09/05.
 */
public class EncryptionHandler {
    private static final String DIGEST_ALGORITHM="MD5";
    private static final String SALT_PROVIDER="SUN";
    private static final String SALT_ALGORITHM="SHA1PRNG";
    private byte[] saltt;
    private static EncryptionHandler instance=new EncryptionHandler();
    private Lock lock=new ReentrantLock();
    private EncryptionHandler(){}

    public static EncryptionHandler getInstance()
    {
        return instance;
    }
    public  String encrypt(String toEncrypt)
    {
        lock.lock();
        try
        {
            MessageDigest md=MessageDigest.getInstance(DIGEST_ALGORITHM);
            md.update(toEncrypt.getBytes());
            byte[] bytes=md.digest();
            StringBuilder sb=new StringBuilder();
            for(int i=0;i<bytes.length;i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            return sb.toString();
        }
        catch (NoSuchAlgorithmException exc) {
        }
        finally {
            lock.unlock();
        }
        return toEncrypt;
    }


    public static void main(String[] args) {
        try {
           System.out.println(new EncryptionHandler().encrypt("password"));
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
}
