/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package za.org.ecg.config;

import org.apache.log4j.extras.DOMConfigurator;
import za.org.ecg.ECGHotelBooking;
import za.org.ecg.dao.cache.EntityCacheManagerProvider;
import za.org.ecg.dao.cache.EntityCacheException;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 *
 * @author Ntsako
 */
public class ApplicationConfig implements ServletContextListener{
    private static final String LOG4J_CONFIG="/za/org/ecg/log4j.xml";

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        DOMConfigurator.configure(ECGHotelBooking.class.getResource(LOG4J_CONFIG));
        try {
            EntityCacheManagerProvider.getInstance().getCacheManager().init();
        } catch (EntityCacheException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
    }
    
}
