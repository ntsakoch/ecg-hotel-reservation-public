package za.org.ecg.config.app;

/**
 * Created by Ntsako on 2017/01/27.
 */
public interface AppPropertiesConstants {
    String APP_DEFAULT_LOCALE="DEFAULT_LOCALE";
    String DEFAULT_CURRENCY_CODE="ZAR";
}
