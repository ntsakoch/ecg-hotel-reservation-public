package za.org.ecg.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ntsako on 2016/09/26.
 */
public class DateUtils {
    private static SimpleDateFormat defaultFormat=new SimpleDateFormat("yyyy/MM/dd");
    private static DateUtils instance=new DateUtils();

    public static DateUtils getInstance()
    {
        return instance;
    }
   private DateUtils(){}
    public synchronized Date parse(String toParse,String format) throws ParseException
    {
        try {
            Date formatted=getDateFormat(format).parse(toParse);
            return formatted;
        } catch (ParseException e) {
            throw new ParseException("Error parsing date: "+toParse+"["+format+"]:",e.getErrorOffset());
        }
    }

    public synchronized   Date parse(Date toParse,String format) throws ParseException
    {
        try {
            Date formated=getDateFormat(format).parse(toParse.toString());
            return formated;
        } catch (ParseException e) {
            throw new ParseException("Error parsing date: "+toParse+"["+format+"]:",e.getErrorOffset());
        }
    }

    public synchronized String format(Date toFormat,String format)
    {
        if(toFormat==null)
        {
            return getDateFormat("").format(new Date());
        }
        SimpleDateFormat df=getDateFormat(format);
        return df.format(toFormat);
    }
    private static SimpleDateFormat getDateFormat(String format)
    {
      return format!=null&&!format.trim().isEmpty()?new SimpleDateFormat(format):defaultFormat;
    }

}
