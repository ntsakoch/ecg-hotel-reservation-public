package za.org.ecg.utils;

import za.org.ecg.exception.InvalidIDNumberException;

/**
 * Created by root on 2016/10/18.
 */
public class SAIDNumberUtil {

 private String idNumber;
public SAIDNumberUtil(String idNumber)
{
    this.idNumber=idNumber;
}

    public boolean isValidID() {
        if(idNumber == null) {
            return false;
        } else if(idNumber.length() != 13) {
            return false;
        } else {
            int month;
            for(month = 0; month < idNumber.length(); ++month) {
                char day = idNumber.charAt(month);
                if(day < 48 || day > 57) {
                    return false;
                }
            }

            month = Integer.parseInt(idNumber.substring(2, 4));
            if(month >= 1 && month <= 12) {
                int var12 = Integer.parseInt(idNumber.substring(4, 6));
                if(var12 >= 1 && var12 <= 31) {
                    int x1 = 0;

                    for(int sb = 0; sb < idNumber.length() - 1; sb += 2) {
                        x1 += Integer.parseInt("" +idNumber.charAt(sb));
                    }

                    StringBuffer var13 = new StringBuffer();

                    int x2;
                    for(x2 = 1; x2 < idNumber.length(); x2 += 2) {
                        var13.append(idNumber.charAt(x2));
                    }

                    x2 = Integer.parseInt(var13.toString()) * 2;
                    String s3 = "" + x2;
                    int x3 = 0;

                    int x4;
                    for(x4 = 0; x4 < s3.length(); ++x4) {
                        x3 += Integer.parseInt("" + s3.charAt(x4));
                    }

                    x4 = x1 + x3;
                    int x5 = x4 % 10;
                    int answer = (10 - x5) % 10;
                    int lastDigit = Integer.parseInt("" +idNumber.charAt(idNumber.length() - 1));
                    if(answer == lastDigit) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }
    private void checkValidIdNumber() throws InvalidIDNumberException
    {
        if(!isValidID())
            throw new InvalidIDNumberException("ID Number: "+this.idNumber+" is invalid!");
    }
    public boolean isSACitizen() throws InvalidIDNumberException {
        checkValidIdNumber();
        return Integer.parseInt("" + this.idNumber.charAt(10)) == 0;
    }

    public String getGender() throws InvalidIDNumberException {
        checkValidIdNumber();
        return idNumber.charAt(6)>4?"Male":"Female";
    }

    public String getIdNumber() throws InvalidIDNumberException{
        checkValidIdNumber();
        return this.idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getDateOfBirth() throws InvalidIDNumberException
    {
        checkValidIdNumber();
        String yy=idNumber.substring(0,2);
        String mm=idNumber.substring(2,4);
        String dd=idNumber.substring(4,6);
        return yy+"/"+mm+"/"+dd;
    }

    public static void main(String[] args) {
        SAIDNumberUtil saidNumberUtil=new SAIDNumberUtil("9011025675081");
        try {
                System.out.println("IS SA Citizen: "+saidNumberUtil.isSACitizen());
                System.out.println("ID NO: "+saidNumberUtil.getIdNumber());
                System.out.println("Is ID Valid: "+saidNumberUtil.isValidID());
                System.out.println("Gender: "+saidNumberUtil.getGender());
                System.out.println("DOB: "+saidNumberUtil.getDateOfBirth());
        } catch (InvalidIDNumberException e) {
            e.printStackTrace();
        }
    }
}
