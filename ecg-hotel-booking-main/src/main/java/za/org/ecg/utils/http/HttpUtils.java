package za.org.ecg.utils.http;

/**
 * Created by Ntsako on 2017/02/19.
 */

import java.util.Date;

public class HttpUtils {

    private static final String DEFUALT_CLIENT_ERROR_REQUEST_MSG = "An error occurred while processing your request, We apologise for any inconvenience. " +
            "Should you wish to report this error please quote the text below and email to admin@ecg-bookings<br/><br/>{\"%s\",\"%s\"}" +
            "OR alternatively you can %s to report this error now.";

    public static String getDefaultClientErrorRequestMsg(String sessionId)
    {
        return String.format(DEFUALT_CLIENT_ERROR_REQUEST_MSG,new Date(),sessionId,"%s");
    }



    public interface HTMLTags {
        String OPENING_ANCHOR_TAG = "<a";
        String CLOSING_ANCHOR_TAG = "</a>";
        String TAG_ENCLOSURE = ">";
        String PARAGRAPH_BREAK_TAG = "<br/>";
        String DOUBLE_PARAGRAPH_BREAK_TAG = "<br/><br/>";
    }

    public interface HTMLConstants {
        String LINK_HREF_PROPERTY = "href";
        String LINK_TARGET_PROPERTY = "target";
        String DOUBLE_QUOTE = "\"";
        String SINGLE_QUOTE = "'";
        String BLANK_LINK_TARGET = "_blank";
        String DOUBLE_EQUAL_SIGN = "=";
        String SPACE_CHAR = " ";
    }

    public static void main(String[] args) {
        String str="hello %s %s";
        String result1=String.format(str,"Ntsako","%s");
        System.out.println(result1);
        System.out.println(String.format(result1,"Chabalala"));

    }
}
