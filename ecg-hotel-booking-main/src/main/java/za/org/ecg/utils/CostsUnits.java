package za.org.ecg.utils;

/**
 * Created by Ntsako on 2017/02/24.
 */
public class CostsUnits {
  public static final  String PER_ITEM_PER_STAY="PER_ITEM_PER_STAY";
  public static final  String PER_ITEM_PER_DAY="PER_ITEM_PER_DAY";
  public static final String PER_INDIVIDUAL_PER_DAY="PER_INDIVIDUAL_PER_DAY";
  public static final String PER_NIGHT="PER_INDIVIDUAL_PER_STAY";
}
