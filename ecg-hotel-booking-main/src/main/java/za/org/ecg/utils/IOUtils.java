package za.org.ecg.utils;

import org.apache.log4j.Logger;

import java.io.Closeable;
import java.io.IOException;

/**
 * Created by Ntsako on 2016/11/14.
 */
public class IOUtils {
    private static final Logger LOG=Logger.getLogger(IOUtils.class);

    public static void closeIOStream(Closeable toClose,String source)
    {
        if(toClose!=null)
        {
            try {
                toClose.close();
            }
            catch (IOException exc)
            {
                LOG.warn(source+ " An error occurred closing IO Stream, ",exc);
            }
        }
    }
}
