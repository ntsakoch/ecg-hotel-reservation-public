package za.org.ecg.utils.http;

/**
 * Created by Ntsako on 2017/02/19.
 */
public class HttpClientError {
    private String errorMessage;
    private String operationName;
    private String referenceString;

    public HttpClientError(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public HttpClientError(String errorMessage, String operationName) {
        this.errorMessage = errorMessage;
        this.operationName = operationName;
    }

    public HttpClientError(String errorMessage, String operationName,String referenceString) {
        this.errorMessage = errorMessage;
        this.operationName = operationName;
        this.referenceString=referenceString;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }


    public String getReferenceString() {
        if(this.referenceString==null||this.referenceString.isEmpty())
        {
            if(errorMessage!=null&&errorMessage.contains("{")&&errorMessage.contains("}"))
            {
                referenceString=errorMessage.substring(errorMessage.indexOf("{")+1,errorMessage.indexOf("}"));
            }
        }
        return referenceString;
    }

    public void setReferenceString(String referenceString) {
        this.referenceString = referenceString;
    }

    public String getOperationName() {
        return operationName;
    }

    public void setOperationName(String operationName) {
        this.operationName = operationName;
    }
}
