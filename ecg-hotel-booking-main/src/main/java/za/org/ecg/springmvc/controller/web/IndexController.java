package za.org.ecg.springmvc.controller.web;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import za.org.ecg.dao.entity.Country;
import za.org.ecg.dao.entity.Room;
import za.org.ecg.dao.exception.EcgDAOException;
import za.org.ecg.springmvc.service.CountryService;
import za.org.ecg.springmvc.service.RoomService;
import za.org.ecg.springmvc.service.UserService;

import java.io.IOException;
import java.util.List;

@Controller
public class IndexController extends HttpRequestController {
    private static final Logger LOGGER=Logger.getLogger(HttpRequestController.class);

    private static final String TOP_LISTED_PAGE ="top-listed.jsp";
    private static final String INDEX_PAGE="index";

    @Autowired
    private UserService userServiceProxy;

    @Autowired
    private CountryService countryService;

    @Autowired
    private RoomService roomService;

    private List<Country> countries;
    private Country clientLocation;
    private List<Room> topListedRooms;
    private List<Room> roomsOnSpecial;

    private final int defaultCountryId=1;

    @RequestMapping(path = "/change-country",method = RequestMethod.POST)
    public String changeCountry() throws IOException {
        String page=INDEX_PAGE;
        try {
             int selectedCountryId=Integer.parseInt(getParameter("clientLocation"));
             page=getParameter("parentPage")!=null?getParameter("parentPage"):page;
            LOGGER.info("Changing client country id to: "+selectedCountryId);
            clientLocation =countryService.findCountryById(selectedCountryId);
            setSessionValue("clientLocation", clientLocation);
            LOGGER.info("Client Location changed to: "+ clientLocation);
        } catch (Exception e) {
            LOGGER.error("Error changing client location: ",e);

        }
        return page;
    }

    @RequestMapping(path = {"/","/index","/home"},method = {RequestMethod.GET,RequestMethod.POST})
    public String getIndexPage() throws IOException {
        try {
            setupClientSession();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(getSessionValue("currentPage")==null) {
            setSessionValue("currentPage", TOP_LISTED_PAGE);
        }
        return INDEX_PAGE;
    }

    private void setupClientSession() throws EcgDAOException {
        if(getSessionValue("clientSessionConfigured")==null||
                !getSessionValue("clientSessionConfigured").toString().equalsIgnoreCase("TRUE")) {
            LOGGER.info("Client Info: "+getRequestLocale().getDisplayCountry()+"--->"+getRequestLocale().getDisplayLanguage());
            LOGGER.debug("GEO Country: "+getClientCountryCode());

            countries = (List<Country>) getSessionValue("countries");
            clientLocation = (Country) getSessionValue("clientLocation");
            if (countries == null) {
                countries = countryService.findAllCountries();
                setSessionValue("countries", countries);
            }
            if (clientLocation == null) {
                clientLocation = countryService.findCountryById(defaultCountryId);
                setSessionValue("clientLocation", clientLocation);
            }
            topListedRooms=roomService.findTopListedRooms();
            roomsOnSpecial=roomService.findRoomsOnSpecial();
            setSessionValue("roomsOnSpecials",roomsOnSpecial);
            setSessionValue("topListedRooms",topListedRooms);
            setSessionValue("clientSessionConfigured","TRUE");
            LOGGER.info("Countries Size Loaded: " + countries.size()+", TopListedRooms Size: "+topListedRooms.size()+", RoomsOnSpecial Size: "+roomsOnSpecial.size());
            LOGGER.info("Client Location: " + clientLocation);
        }
    }

    @RequestMapping(path = "/test",method = RequestMethod.GET)
    public String getTestPage() throws IOException {
        return "test";
    }


}
