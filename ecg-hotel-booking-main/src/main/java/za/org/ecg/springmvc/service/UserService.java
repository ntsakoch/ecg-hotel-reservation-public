package za.org.ecg.springmvc.service;

import za.org.ecg.springmvc.service.audit.Audit;
import za.org.ecg.dao.entity.ActionAudit;
import za.org.ecg.dao.entity.User;
import za.org.ecg.dao.exception.EcgDAOException;
import za.org.ecg.springmvc.service.audit.ActionAuditType;
import za.org.ecg.springmvc.service.audit.EnableAuditing;
import za.org.ecg.springmvc.service.impl.UserServiceImpl;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2016/12/19.
 */
@EnableAuditing(implClass = UserServiceImpl.class)
public interface UserService {

    User findByIdNumber(String id) throws EcgDAOException;

    User authenticateUser(String username, String password) throws EcgDAOException;

    User findByUsername(String username) throws EcgDAOException;

    List<User> findAllUsers() throws EcgDAOException;

    User findUserById(Integer id) throws EcgDAOException;

    void createUser(User user) throws EcgDAOException;

    void update(User newValue) throws EcgDAOException;

    void remove(User user) throws EcgDAOException;

    long countUsers() throws EcgDAOException;

    List<User> findRange(int start, int end) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query) throws EcgDAOException;

    User mergeUser(User user) throws EcgDAOException;

    Integer saveUser(User user) throws EcgDAOException;

    List<ActionAudit> findAllUsersActions(int userId) throws  EcgDAOException;
 
}
