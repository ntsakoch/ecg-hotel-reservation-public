package za.org.ecg.springmvc.service;

import za.org.ecg.dao.entity.CountryCity;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/02/21.
 */
public interface CountryCityService {

    List<CountryCity> findAllCountryCities() throws EcgDAOException;

    List<CountryCity> findAllCountryCitiesByCityName(String cityName) throws EcgDAOException;

    CountryCity findCountryCityById(Integer id) throws EcgDAOException;

    void createCountryCity(CountryCity countryCity) throws EcgDAOException;

    void updateCountryCity(CountryCity newValue) throws EcgDAOException;

    void removeCountryCity(CountryCity countryCity) throws EcgDAOException;

    long countCountryCities() throws EcgDAOException;

    List<CountryCity> findRange(int start, int end) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query) throws EcgDAOException;

    CountryCity mergeCountryCity(CountryCity countryCity) throws EcgDAOException;

    Integer saveCountryCity(CountryCity countryCity) throws EcgDAOException;
}
