package za.org.ecg.springmvc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import za.org.ecg.dao.CustomerPaymentDao;
import za.org.ecg.dao.entity.CustomerPayment;
import za.org.ecg.dao.exception.EcgDAOException;
import za.org.ecg.springmvc.service.CustomerPaymentService;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/16.
 */
@Service("customerPaymentService")
public class CustomerPaymentServiceImpl implements CustomerPaymentService {

    @Autowired
    private CustomerPaymentDao customerPaymentDao;

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<CustomerPayment> findAllCustomerPayments() throws EcgDAOException {
        return customerPaymentDao.findAll();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public CustomerPayment findCustomerPaymentById(Integer id) throws EcgDAOException {
        return customerPaymentDao.findById(id);
    }

    @Override
    @Transactional
    public void createCustomerPayment(CustomerPayment payment) throws EcgDAOException {
        customerPaymentDao.create(payment);
    }

    @Override
    @Transactional
    public void updateCustomerPayment(CustomerPayment newValue) throws EcgDAOException {
        customerPaymentDao.update(newValue);
    }

    @Override
    @Transactional
    public void removeCustomerPayment(CustomerPayment payment) throws EcgDAOException {
        customerPaymentDao.remove(payment);
    }

    @Override
    @Transactional
    public long countCustomerPayments() throws EcgDAOException {
        return customerPaymentDao.count();
    }

    @Override
    @Transactional
    public List<CustomerPayment> findRange(int start, int end) throws EcgDAOException {
        return customerPaymentDao.findRange(start,start);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException {
        return customerPaymentDao.executeSQLQuery(query,params);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query) throws EcgDAOException {
        return customerPaymentDao.executeSQLQuery(query);
    }

    @Override
    @Transactional
    public CustomerPayment mergeCustomerPayment(CustomerPayment payment) throws EcgDAOException {
        return customerPaymentDao.merge(payment);
    }

    @Override
    @Transactional
    public Integer saveCustomerPayment(CustomerPayment payment) throws EcgDAOException {
        return customerPaymentDao.save(payment);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<CustomerPayment> findByCustomerIdNumber(String customerIdNumber) throws EcgDAOException {
        return customerPaymentDao.findByCustomerIdNumber(customerIdNumber);
    }
}
