package za.org.ecg.springmvc.service;

import za.org.ecg.dao.entity.PreferenceCategory;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/16.
 */
public interface PreferenceCategoryService {

    List<PreferenceCategory> findAllPreferenceCategories() throws EcgDAOException;

    PreferenceCategory findPreferenceCategoryById(Integer id) throws EcgDAOException;

    void createPreferenceCategory(PreferenceCategory preferenceCategory) throws EcgDAOException;

    void updatePreferenceCategory(PreferenceCategory newValue) throws EcgDAOException;

    void removePreferenceCategory(PreferenceCategory preferenceCategory) throws EcgDAOException;

    long countPreferenceCategories() throws EcgDAOException;

    List<PreferenceCategory> findRange(int start, int end) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query) throws EcgDAOException;

    PreferenceCategory mergePreferenceCategory(PreferenceCategory preferenceCategory) throws EcgDAOException;

    Integer savePreferenceCategory(PreferenceCategory preferenceCategory) throws EcgDAOException;
}
