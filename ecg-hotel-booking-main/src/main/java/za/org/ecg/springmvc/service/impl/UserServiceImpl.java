package za.org.ecg.springmvc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import za.org.ecg.security.increption.EncryptionHandler;
import za.org.ecg.springmvc.service.audit.ActionAuditType;
import za.org.ecg.springmvc.service.audit.AuditUserIdTracker;
import za.org.ecg.springmvc.service.audit.Audit;
import za.org.ecg.dao.UserDao;
import za.org.ecg.dao.entity.ActionAudit;
import za.org.ecg.dao.entity.User;
import za.org.ecg.dao.exception.EcgDAOException;
import za.org.ecg.springmvc.service.UserService;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2016/12/23.
 */
@Service("userService")
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public User findByIdNumber(String id) throws EcgDAOException {
        return userDao.findByIdNumber(id);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public User authenticateUser(String username, String password) throws EcgDAOException {
        return  userDao.authenticateUser(username, EncryptionHandler.getInstance().encrypt(password));
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public User findByUsername(String username) throws EcgDAOException {
        return userDao.findByUsername(username);
    }

    @Override
    //@Audit(actionType = ActionAuditType.USER_RETRIEVE,actionDescription = "retrieve all users from database")
    @Transactional(isolation =Isolation.READ_COMMITTED)
    public List<User> findAllUsers() throws EcgDAOException {
        return userDao.findAll();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    //@Audit(actionType = ActionAuditType.USER_RETRIEVE,isActionResult = true,actionDescription = "find user by id")
    public User findUserById(Integer id) throws EcgDAOException {
        return userDao.findById(id);
    }

    @Override
    @Transactional
    public void createUser(User user) throws EcgDAOException {
        userDao.create(user);
    }

    @Override
    @Transactional
    public void update(User newValue) throws EcgDAOException {
        userDao.update(newValue);
    }

    @Override
    @Transactional
    public void remove(User user) throws EcgDAOException {
        userDao.remove(user);
    }

    @Override
    @Transactional
    public long countUsers() throws EcgDAOException {
        return userDao.count();
    }

    @Override
    @Transactional
    public List<User> findRange(int start, int end) throws EcgDAOException {
        return userDao.findRange(start,end);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException {
        return userDao.executeSQLQuery(query,params);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query) throws EcgDAOException {
        return userDao.executeSQLQuery(query);
    }

    @Override
    @Transactional
    public User mergeUser(User user) throws EcgDAOException {
        return userDao.merge(user);
    }

    @Override
    @Transactional
    public Integer saveUser(User user) throws EcgDAOException {
        return userDao.save(user);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<ActionAudit> findAllUsersActions(int userId) throws EcgDAOException {
        return userDao.findAllUsersActions(userId);
    }
}
