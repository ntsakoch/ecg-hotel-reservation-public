package za.org.ecg.springmvc.configuration;

import org.apache.log4j.MDC;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class CORSFilter implements Filter {

        @Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		HttpServletResponse response = (HttpServletResponse) res;
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE");
		response.setHeader("Access-Control-Max-Age", "3600");
		response.setHeader("Access-Control-Allow-Headers", "X-requested-with, Content-Type");
		addSessionIdToMDC(req);
		chain.doFilter(req, res);
	}

	private void addSessionIdToMDC(ServletRequest request)
	{
		if(request instanceof  HttpServletRequest)
		{
			MDC.put("sessionId", ((HttpServletRequest)request).getSession().getId());
		}
	}

	public void init(FilterConfig filterConfig) {

	}

	public void destroy() {}

}