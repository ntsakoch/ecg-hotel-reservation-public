package za.org.ecg.springmvc.service;

import za.org.ecg.dao.entity.RoomGallery;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/16.
 */
public interface RoomGalleryService {

    List<RoomGallery> findAllRoomFacilitiesGallery() throws EcgDAOException;

    RoomGallery findRoomGalleryById(Integer id) throws EcgDAOException;

    void createRoomGallery(RoomGallery gallery) throws EcgDAOException;

    void updateRoomGallery(RoomGallery newValue) throws EcgDAOException;

    void removeRoomGallery(RoomGallery gallery) throws EcgDAOException;

    long countRoomFacilitiesGallery() throws EcgDAOException;

    List<RoomGallery> findRange(int start, int end) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query) throws EcgDAOException;

    RoomGallery mergeRoomGallery(RoomGallery gallery) throws EcgDAOException;

    Integer saveRoomGallery(RoomGallery gallery) throws EcgDAOException;
}
