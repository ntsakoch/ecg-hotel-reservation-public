package za.org.ecg.springmvc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import za.org.ecg.dao.DiscountDao;
import za.org.ecg.dao.entity.Discount;
import za.org.ecg.dao.exception.EcgDAOException;
import za.org.ecg.springmvc.service.DiscountService;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/16.
 */
@Service("discountService")
public class DiscountServiceImpl implements DiscountService {

    @Autowired
    private DiscountDao discountDao;

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<Discount> findAllDiscounts() throws EcgDAOException {
        return discountDao.findAll();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public Discount findDiscountById(Integer id) throws EcgDAOException {
        return discountDao.findById(id);
    }


    @Override
    @Transactional
    public void createDiscount(Discount discount) throws EcgDAOException {
        discountDao.create(discount);
    }

    @Override
    @Transactional
    public void updateDiscount(Discount newValue) throws EcgDAOException {
        discountDao.update(newValue);
    }

    @Override
    @Transactional
    public void removeDiscount(Discount discount) throws EcgDAOException {
        discountDao.remove(discount);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public long countDiscounts() throws EcgDAOException {
        return discountDao.count();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<Discount> findRange(int start, int end) throws EcgDAOException {
        return discountDao.findRange(start,end);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException {
        return discountDao.executeSQLQuery(query,params);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query) throws EcgDAOException {
        return discountDao.executeSQLQuery(query);
    }

    @Override
    @Transactional
    public Discount mergeDiscount(Discount discount) throws EcgDAOException {
        return discountDao.merge(discount);
    }

    @Override
    @Transactional
    public Integer saveDiscount(Discount discount) throws EcgDAOException {
        return discountDao.save(discount);
    }
}
