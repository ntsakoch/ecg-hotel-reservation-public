package za.org.ecg.springmvc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import za.org.ecg.dao.CustomerDao;
import za.org.ecg.dao.entity.Customer;
import za.org.ecg.dao.exception.EcgDAOException;
import za.org.ecg.springmvc.service.CustomerService;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/12.
 */
@Service("customerService")
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerDao customerDao;

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<Customer> findAllCustomers() throws EcgDAOException {
        return customerDao.findAll();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public Customer findCustomerById(Integer id) throws EcgDAOException {
        return customerDao.findById(id);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public Customer findByCustomerIdNumber(String idNo) throws EcgDAOException {
        return customerDao.findByIdNumber(idNo);
    }

    @Override
    @Transactional
    public void createCustomer(Customer customer) throws EcgDAOException {
        customerDao.create(customer);
    }

    @Override
    @Transactional
    public void updateCustomer(Customer newValue) throws EcgDAOException {
        customerDao.update(newValue);
    }

    @Override
    @Transactional
    public void removeCustomer(Customer customer) throws EcgDAOException {
        customerDao.remove(customer);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public long countCustomers() throws EcgDAOException {
        return customerDao.count();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<Customer> findRange(int start, int end) throws EcgDAOException {
        return customerDao.findRange(start,end);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException {
        return customerDao.executeSQLQuery(query,params);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query) throws EcgDAOException {
        return customerDao.executeSQLQuery(query);
    }

    @Override
    @Transactional
    public Customer mergeCustomer(Customer customer) throws EcgDAOException {
        return customerDao.merge(customer);
    }

    @Override
    @Transactional
    public Integer saveCustomer(Customer customer) throws EcgDAOException {
        return customerDao.save(customer);
    }
}
