package za.org.ecg.springmvc.service;

import za.org.ecg.dao.entity.ActionAudit;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/21.
 */
public interface ActionAuditService {

    List<ActionAudit> findAllActionAudits() throws EcgDAOException;

    ActionAudit findActionAuditById(Integer id) throws EcgDAOException;

    void createActionAudit(ActionAudit cctionAudit) throws EcgDAOException;

    void updateActionAudit(ActionAudit newValue) throws EcgDAOException;

    void removeActionAudit(ActionAudit cctionAudit) throws EcgDAOException;

    long countActionAudits() throws EcgDAOException;

    List<ActionAudit> findRange(int start, int end) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query) throws EcgDAOException;

    ActionAudit mergeActionAudit(ActionAudit cctionAudit) throws EcgDAOException;

    Integer saveActionAudit(ActionAudit cctionAudit) throws EcgDAOException;

    List<ActionAudit> findActionsByUser(int userId) throws EcgDAOException;

    List<ActionAudit> findActionsByTime(Date startTime, Date endTime) throws  EcgDAOException;

    List<ActionAudit> findActionsByType(String type) throws  EcgDAOException;

    List<ActionAudit> findActionsBySystemProcessName(String processName) throws  EcgDAOException;
}
