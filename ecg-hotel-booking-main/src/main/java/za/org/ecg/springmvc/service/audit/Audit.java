package za.org.ecg.springmvc.service.audit;


import java.lang.annotation.*;

/**
 * Created by Ntsako on 2017/01/21.
 */
@Target(ElementType.METHOD)
@Inherited
@Retention(RetentionPolicy.RUNTIME)
public @interface Audit {
    ActionAuditType actionType() default ActionAuditType.UN_SPECIFIED;
    String actionDescription() default "";
    boolean isActionResult() default false;
}
