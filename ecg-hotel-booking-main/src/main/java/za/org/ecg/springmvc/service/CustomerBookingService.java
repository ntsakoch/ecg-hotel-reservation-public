package za.org.ecg.springmvc.service;

import za.org.ecg.dao.entity.Customer;
import za.org.ecg.dao.entity.CustomerBooking;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/12.
 */
public interface CustomerBookingService {

    List<CustomerBooking> findAllCustomerBookings() throws EcgDAOException;

    CustomerBooking findCustomerBookingById(Integer id) throws EcgDAOException;
    
    void createCustomerBooking(CustomerBooking booking) throws EcgDAOException;

    void updateCustomerBooking(CustomerBooking newValue) throws EcgDAOException;

    void removeCustomerBooking(CustomerBooking booking) throws EcgDAOException;

    long countCustomerBookings() throws EcgDAOException;

    List<CustomerBooking> findRange(int start, int end) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query) throws EcgDAOException;

    CustomerBooking mergeCustomerBooking(CustomerBooking booking) throws EcgDAOException;

    Integer saveCustomerBooking(CustomerBooking booking) throws EcgDAOException;

    List<CustomerBooking> findByStatus(String status) throws EcgDAOException;
    List<CustomerBooking> findByCheckinDate(Date date) throws EcgDAOException;
    List<CustomerBooking> findByCheckoutDate(Date date) throws EcgDAOException;
    List<CustomerBooking> findByCheckoutDateRange(Date from,Date to) throws EcgDAOException;
    List<CustomerBooking> findByCheckinDateRange(Date from,Date to) throws EcgDAOException;
    List<CustomerBooking> findByDateCreationRange(Date from,Date to) throws EcgDAOException;
    List<CustomerBooking> findByCustomerIdNumber(String customerIdNumber) throws EcgDAOException;
    List<CustomerBooking> findAllOutstandingBookings(double minimumAmount) throws EcgDAOException;
}
