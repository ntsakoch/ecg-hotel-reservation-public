package za.org.ecg.springmvc.service;

import za.org.ecg.dao.entity.CustomerDiscount;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/15.
 */
public interface CustomerDiscountService {

    List<CustomerDiscount> findAllCustomerDiscounts() throws EcgDAOException;

    CustomerDiscount findCustomerDiscountById(Integer id) throws EcgDAOException;
    
    void createCustomerDiscount(CustomerDiscount discount) throws EcgDAOException;

    void updateCustomerDiscount(CustomerDiscount newValue) throws EcgDAOException;

    void removeCustomerDiscount(CustomerDiscount discount) throws EcgDAOException;

    long countCustomerDiscounts() throws EcgDAOException;

    List<CustomerDiscount> findRange(int start, int end) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query) throws EcgDAOException;

    CustomerDiscount mergeCustomerDiscount(CustomerDiscount discount) throws EcgDAOException;

    Integer saveCustomerDiscount(CustomerDiscount discount) throws EcgDAOException;
}
