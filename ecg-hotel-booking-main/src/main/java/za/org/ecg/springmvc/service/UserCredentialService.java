package za.org.ecg.springmvc.service;

import za.org.ecg.dao.entity.UserCredential;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/16.
 */
public interface UserCredentialService {

    List<UserCredential> findAllRoomUsersCredential() throws EcgDAOException;

    UserCredential findUserCredentialById(Integer id) throws EcgDAOException;

    void createUserCredential(UserCredential userCredential) throws EcgDAOException;

    void updateUserCredential(UserCredential newValue) throws EcgDAOException;

    void removeUserCredential(UserCredential userCredential) throws EcgDAOException;

    long countRoomUsersCredential() throws EcgDAOException;

    List<UserCredential> findRange(int start, int end) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query) throws EcgDAOException;

    UserCredential mergeUserCredential(UserCredential userCredential) throws EcgDAOException;

    Integer saveUserCredential(UserCredential userCredential) throws EcgDAOException;
}
