package za.org.ecg.springmvc.service;

import za.org.ecg.dao.entity.ContactUsMessageResponse;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2016/12/28.
 */
public interface ContactUsMessageResponseService {

    List<ContactUsMessageResponse> findAllContactUsMessageResponses() throws EcgDAOException;

    ContactUsMessageResponse findContactUsMessageResponseById(Integer id) throws EcgDAOException;

    void createContactUsMessageResponse(ContactUsMessageResponse messageResponse) throws EcgDAOException;

    void updateContactUsMessageResponse(ContactUsMessageResponse newValue) throws EcgDAOException;

    void removeContactUsMessageResponse(ContactUsMessageResponse messageResponse) throws EcgDAOException;

    long countContactUsMessageResponses() throws EcgDAOException;

    List<ContactUsMessageResponse> findRange(int start, int end) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query) throws EcgDAOException;

    ContactUsMessageResponse mergeContactUsMessageResponse(ContactUsMessageResponse messageResponse) throws EcgDAOException;

    Integer saveContactUsMessageResponse(ContactUsMessageResponse messageResponse) throws EcgDAOException;

    List<ContactUsMessageResponse> findByOriginalContactUsMessageId(int originalMessageId) throws EcgDAOException;
}
