package za.org.ecg.springmvc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import za.org.ecg.dao.BookingCheckinDao;
import za.org.ecg.dao.entity.BookingCheckin;
import za.org.ecg.dao.exception.EcgDAOException;
import za.org.ecg.springmvc.service.BookingCheckinService;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/12.
 */
@Service("bookingCheckInService")
public class BookingCheckinServiceImpl implements BookingCheckinService {

    @Autowired
    private BookingCheckinDao bookingCheckInDao;

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<BookingCheckin> findAllBookingCheckins() throws EcgDAOException {
        return bookingCheckInDao.findAll();
    }

    @Override
    public BookingCheckin findBookingCheckinById(Integer id) throws EcgDAOException {
        return bookingCheckInDao.findById(id);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public void createBookingCheckin(BookingCheckin checkin) throws EcgDAOException {
      bookingCheckInDao.create(checkin);
    }

    @Override
    @Transactional
    public void updateBookingCheckin(BookingCheckin newValue) throws EcgDAOException {
        bookingCheckInDao.update(newValue);
    }

    @Override
    @Transactional
    public void removeBookingCheckin(BookingCheckin checkin) throws EcgDAOException {
        bookingCheckInDao.remove(checkin);
    }

    @Override
    @Transactional
    public long countBookingCheckins() throws EcgDAOException {
        return bookingCheckInDao.count();
    }

    @Override
    @Transactional(readOnly = true)
    public List<BookingCheckin> findRange(int start, int end) throws EcgDAOException {
        return bookingCheckInDao.findRange(start,end);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException {
        return bookingCheckInDao.executeSQLQuery(query,params);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query) throws EcgDAOException {
        return bookingCheckInDao.executeSQLQuery(query);
    }

    @Override
    @Transactional
    public BookingCheckin mergeBookingCheckin(BookingCheckin checkin) throws EcgDAOException {
        return bookingCheckInDao.merge(checkin);
    }

    @Override
    @Transactional
    public Integer saveBookingCheckin(BookingCheckin checkin) throws EcgDAOException {
        return bookingCheckInDao.save(checkin);
    }
}
