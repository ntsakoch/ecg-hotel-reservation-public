package za.org.ecg.springmvc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import za.org.ecg.dao.PostalAddressDao;
import za.org.ecg.dao.entity.PostalAddress;
import za.org.ecg.dao.exception.EcgDAOException;
import za.org.ecg.springmvc.service.PostalAddressService;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/16.
 */
@Service("postalAddressService")
public class PostalAddressServiceImpl  implements PostalAddressService{

    @Autowired
    private PostalAddressDao postalAddressDao;

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<PostalAddress> findAllPostalAddresses() throws EcgDAOException {
        return postalAddressDao.findAll();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public PostalAddress findPostalAddressById(Integer id) throws EcgDAOException {
        return postalAddressDao.findById(id);
    }

    @Override
    @Transactional
    public void createPostalAddress(PostalAddress postalAddress) throws EcgDAOException {
        postalAddressDao.create(postalAddress);
    }

    @Override
    @Transactional
    public void updatePostalAddress(PostalAddress newValue) throws EcgDAOException {
        postalAddressDao.update(newValue);
    }

    @Override
    @Transactional
    public void removePostalAddress(PostalAddress postalAddress) throws EcgDAOException {
        postalAddressDao.remove(postalAddress);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public long countPostalAddresses() throws EcgDAOException {
        return postalAddressDao.count();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<PostalAddress> findRange(int start, int end) throws EcgDAOException {
        return postalAddressDao.findRange(start,end);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException {
        return postalAddressDao.executeSQLQuery(query,params);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query) throws EcgDAOException {
        return postalAddressDao.executeSQLQuery(query);
    }

    @Override
    @Transactional
    public PostalAddress mergePostalAddress(PostalAddress postalAddress) throws EcgDAOException {
        return postalAddressDao.merge(postalAddress);
    }

    @Override
    @Transactional
    public Integer savePostalAddress(PostalAddress postalAddress) throws EcgDAOException {
        return postalAddressDao.save(postalAddress);
    }
}
