package za.org.ecg.springmvc.configuration;

import org.springframework.aop.framework.ProxyFactory;
import org.springframework.aop.framework.ProxyFactoryBean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import za.org.ecg.springmvc.service.UserService;
import za.org.ecg.springmvc.service.audit.AuditActionInterceptor;

/**
 * Created by Ntsako on 2017/01/22.
 */
@Configuration
@ComponentScan(basePackages = {"za.org.ecg.springmvc.service"})
public class ProxyInterceptorConfiguration {

    @Autowired
    private UserService userService;

    @Bean
    public UserService userServiceProxy()
    {
        proxyFactory().addAdvice(auditActionInterceptor());
        proxyFactory().setTarget(userService);
        userService=(UserService) proxyFactory().getProxy();
        return userService;
    }

    @Bean
    public AuditActionInterceptor auditActionInterceptor()
    {
        return new AuditActionInterceptor();
    }

    @Bean
    public ProxyFactory proxyFactory()
    {
        return new ProxyFactory();
    }

    private ProxyFactoryBean getProxyFactoryBean()
    {
        return new ProxyFactoryBean();
    }
}
