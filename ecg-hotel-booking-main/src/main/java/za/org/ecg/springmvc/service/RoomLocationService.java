package za.org.ecg.springmvc.service;

import za.org.ecg.dao.entity.RoomLocation;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/02/21.
 */
public interface RoomLocationService {

    List<RoomLocation> findAllRoomLocation() throws EcgDAOException;

    RoomLocation findRoomLocationById(Integer id) throws EcgDAOException;

    void createRoomLocation(RoomLocation roomLocation) throws EcgDAOException;

    void updateRoomLocation(RoomLocation newValue) throws EcgDAOException;

    void removeRoomLocation(RoomLocation roomLocation) throws EcgDAOException;

    long countRoomLocations() throws EcgDAOException;

    List<RoomLocation> findRange(int start, int end) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query) throws EcgDAOException;

    RoomLocation mergeRoomLocation(RoomLocation roomLocation) throws EcgDAOException;

    Integer saveRoomLocation(RoomLocation roomLocation) throws EcgDAOException;
}
