package za.org.ecg.springmvc.service;

import za.org.ecg.dao.entity.RoomFeature;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/16.
 */
public interface RoomFeatureService {

    List<RoomFeature> findAllRoomFacilities() throws EcgDAOException;

    RoomFeature findRoomFeatureById(Integer id) throws EcgDAOException;

    void createRoomFeature(RoomFeature feature) throws EcgDAOException;

    void updateRoomFeature(RoomFeature newValue) throws EcgDAOException;

    void removeRoomFeature(RoomFeature feature) throws EcgDAOException;

    long countRoomFacilities() throws EcgDAOException;

    List<RoomFeature> findRange(int start, int end) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query) throws EcgDAOException;

    RoomFeature mergeRoomFeature(RoomFeature feature) throws EcgDAOException;

    Integer saveRoomFeature(RoomFeature feature) throws EcgDAOException;
}
