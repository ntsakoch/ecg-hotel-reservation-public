package za.org.ecg.springmvc.service;

import za.org.ecg.dao.entity.UserPermission;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/16.
 */
public interface UserPermissionService {

    List<UserPermission> findAllRoomUsersPermission() throws EcgDAOException;

    UserPermission findUserPermissionById(Integer id) throws EcgDAOException;

    void createUserPermission(UserPermission userPermission) throws EcgDAOException;

    void updateUserPermission(UserPermission newValue) throws EcgDAOException;

    void removeUserPermission(UserPermission userPermission) throws EcgDAOException;

    long countRoomUsersPermission() throws EcgDAOException;

    List<UserPermission> findRange(int start, int end) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query) throws EcgDAOException;

    UserPermission mergeUserPermission(UserPermission userPermission) throws EcgDAOException;

    Integer saveUserPermission(UserPermission userPermission) throws EcgDAOException;
}
