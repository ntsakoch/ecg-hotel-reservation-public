package za.org.ecg.springmvc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import za.org.ecg.dao.CustomerInvoiceDao;
import za.org.ecg.dao.entity.CustomerInvoice;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/15.
 */
public interface CustomerInvoiceService {

    List<CustomerInvoice> findAllCustomerInvoices() throws EcgDAOException;

    CustomerInvoice findCustomerInvoiceById(Integer id) throws EcgDAOException;

    void createCustomerInvoice(CustomerInvoice invoice) throws EcgDAOException;

    void updateCustomerInvoice(CustomerInvoice newValue) throws EcgDAOException;

    void removeCustomerInvoice(CustomerInvoice invoice) throws EcgDAOException;

    long countCustomerInvoices() throws EcgDAOException;

    List<CustomerInvoice> findRange(int start, int end) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query) throws EcgDAOException;

    CustomerInvoice mergeCustomerInvoice(CustomerInvoice invoice) throws EcgDAOException;

    Integer saveCustomerInvoice(CustomerInvoice invoice) throws EcgDAOException;
}
