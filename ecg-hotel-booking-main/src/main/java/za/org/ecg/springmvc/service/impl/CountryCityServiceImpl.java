package za.org.ecg.springmvc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import za.org.ecg.dao.CountryCityDao;
import za.org.ecg.dao.entity.CountryCity;
import za.org.ecg.dao.exception.EcgDAOException;
import za.org.ecg.springmvc.service.CountryCityService;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/02/21.
 */
@Service("countryCityService")
public class CountryCityServiceImpl implements CountryCityService {

    @Autowired
    private CountryCityDao countryCityDao;

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED,propagation = Propagation.SUPPORTS)
    public List<CountryCity> findAllCountryCities() throws EcgDAOException {
        return countryCityDao.findAll();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED,propagation = Propagation.SUPPORTS)
    public List<CountryCity> findAllCountryCitiesByCityName(String cityName) throws EcgDAOException {
        return countryCityDao.findByCityName(cityName);
    }
    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED,propagation = Propagation.SUPPORTS)
    public CountryCity findCountryCityById(Integer id) throws EcgDAOException {
        return countryCityDao.findById(id);
    }

    @Override
    @Transactional
    public void createCountryCity(CountryCity countryCity) throws EcgDAOException {
        countryCityDao.create(countryCity);
    }

    @Override
    @Transactional
    public void updateCountryCity(CountryCity newValue) throws EcgDAOException {
        countryCityDao.update(newValue);
    }

    @Override
    @Transactional
    public void removeCountryCity(CountryCity countryCity) throws EcgDAOException {
        countryCityDao.remove(countryCity);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public long countCountryCities() throws EcgDAOException {
        return countryCityDao.count();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<CountryCity> findRange(int start, int end) throws EcgDAOException {
        return countryCityDao.findRange(start,end);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED,readOnly = true)
    public List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException {
        return countryCityDao.executeSQLQuery(query,params);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED,readOnly = true)
    public List<Object[]> executeSQLQuery(String query) throws EcgDAOException {
        return countryCityDao.executeSQLQuery(query);
    }

    @Override
    @Transactional
    public CountryCity mergeCountryCity(CountryCity countryCity) throws EcgDAOException {
        return countryCityDao.merge(countryCity);
    }

    @Override
    @Transactional
    public Integer saveCountryCity(CountryCity countryCity) throws EcgDAOException {
        return countryCityDao.save(countryCity);
    }
}
