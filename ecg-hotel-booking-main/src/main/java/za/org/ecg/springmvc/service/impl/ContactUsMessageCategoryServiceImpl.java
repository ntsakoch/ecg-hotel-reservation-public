package za.org.ecg.springmvc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import za.org.ecg.dao.ContactUsMessageCategoryDao;
import za.org.ecg.dao.entity.ContactUsMessageCategory;
import za.org.ecg.dao.exception.EcgDAOException;
import za.org.ecg.springmvc.service.ContactUsMessageCategoryService;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2016/12/28.
 */
@Service("contactUsMessageCategoryService")
public class ContactUsMessageCategoryServiceImpl implements ContactUsMessageCategoryService {
    @Autowired
    private ContactUsMessageCategoryDao contactUsMessageCategoryDao;

    @Override
    @Transactional
    public List<ContactUsMessageCategory> findAllContactUsMessageCategorys() throws EcgDAOException {
        return contactUsMessageCategoryDao.findAll() ;
    }

    @Override
    @Transactional
    public ContactUsMessageCategory findContactUsMessageCategoryById(Integer id) throws EcgDAOException {
        return contactUsMessageCategoryDao.findById(id);
    }

    @Override
    @Transactional
    public void createContactUsMessageCategory(ContactUsMessageCategory messageCategory) throws EcgDAOException {
        contactUsMessageCategoryDao.create(messageCategory);
    }

    @Override
    @Transactional
    public void updateContactUsMessageCategory(ContactUsMessageCategory newValue) throws EcgDAOException {
        contactUsMessageCategoryDao.update(newValue);
    }

    @Override
    @Transactional
    public void removeContactUsMessageCategory(ContactUsMessageCategory messageCategory) throws EcgDAOException {
        contactUsMessageCategoryDao.remove(messageCategory);
    }

    @Override
    @Transactional
    public long countContactUsMessageCategories() throws EcgDAOException {
        return contactUsMessageCategoryDao.count();
    }

    @Override
    @Transactional
    public List<ContactUsMessageCategory> findRange(int start, int end) throws EcgDAOException {
        return contactUsMessageCategoryDao.findRange(start,end);
    }

    @Override
    @Transactional
    public List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException {
        return contactUsMessageCategoryDao.executeSQLQuery(query,params);
    }

    @Override
    @Transactional
    public List<Object[]> executeSQLQuery(String query) throws EcgDAOException {
        return contactUsMessageCategoryDao.executeSQLQuery(query);
    }

    @Override
    @Transactional
    public ContactUsMessageCategory mergeContactUsMessageCategory(ContactUsMessageCategory messageCategory) throws EcgDAOException {
        return contactUsMessageCategoryDao.merge(messageCategory);
    }

    @Override
    @Transactional
    public Integer saveContactUsMessageCategory(ContactUsMessageCategory messageCategory) throws EcgDAOException {
        return contactUsMessageCategoryDao.save(messageCategory);
    }
}
