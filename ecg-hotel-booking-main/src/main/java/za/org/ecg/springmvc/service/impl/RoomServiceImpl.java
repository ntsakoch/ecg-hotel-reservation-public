package za.org.ecg.springmvc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import za.org.ecg.dao.RoomDao;
import za.org.ecg.dao.entity.Room;
import za.org.ecg.dao.exception.EcgDAOException;
import za.org.ecg.springmvc.service.RoomService;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/16.
 */
@Service("roomService")
public class RoomServiceImpl implements RoomService {

    @Autowired
    private RoomDao roomDao;

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED,propagation = Propagation.SUPPORTS)
    public List<Room> findAllRoomFacilities() throws EcgDAOException {
        return roomDao.findAll();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<Room> findTopListedRooms() throws EcgDAOException {
        return roomDao.getTopListedRooms();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<Room> findRoomsOnSpecial() throws EcgDAOException {
        return roomDao.getRoomsOnSpecial();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public Room findRoomById(Integer id) throws EcgDAOException {
        return roomDao.findById(id);
    }

    @Override
    @Transactional
    public void createRoom(Room room) throws EcgDAOException {
        roomDao.create(room);
    }

    @Override
    @Transactional
    public void updateRoom(Room newValue) throws EcgDAOException {
        roomDao.update(newValue);
    }

    @Override
    @Transactional
    public void removeRoom(Room room) throws EcgDAOException {
        roomDao.remove(room);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public long countRoomFacilities() throws EcgDAOException {
        return roomDao.count();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<Room> findRange(int start, int end) throws EcgDAOException {
        return roomDao.findRange(start,end);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException {
        return roomDao.executeSQLQuery(query,params);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query) throws EcgDAOException {
        return roomDao.executeSQLQuery(query);
    }

    @Override
    @Transactional
    public Room mergeRoom(Room room) throws EcgDAOException {
        return roomDao.merge(room);
    }

    @Override
    @Transactional
    public Integer saveRoom(Room room) throws EcgDAOException {
        return roomDao.save(room);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<Room> getAllAvailableRooms(Date checkinDate, Date checkoutDate) throws EcgDAOException {
        return roomDao.getAllAvailableRooms(checkinDate,checkoutDate);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<Room> getAllAvailableRooms(Date checkinDate, Date checkoutDate, int numberOfAdults, int numberOfChildren) throws EcgDAOException {
        return roomDao.getAllAvailableRooms(checkinDate,checkoutDate,numberOfChildren,numberOfAdults);
}

}
