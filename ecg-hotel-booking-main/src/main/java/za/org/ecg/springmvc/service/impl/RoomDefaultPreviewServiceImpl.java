package za.org.ecg.springmvc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import za.org.ecg.dao.RoomDefaultPreviewDao;
import za.org.ecg.dao.entity.RoomDefaultPreview;
import za.org.ecg.dao.exception.EcgDAOException;
import za.org.ecg.springmvc.service.RoomDefaultPreviewService;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/16.
 */
@Service("roomDefaultPreviewService")
public class RoomDefaultPreviewServiceImpl implements RoomDefaultPreviewService {

    @Autowired
    private RoomDefaultPreviewDao roomDefaultPreviewDao;

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<RoomDefaultPreview> findAllRoomFacilitiesDefaultPreview() throws EcgDAOException {
        return roomDefaultPreviewDao.findAll();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public RoomDefaultPreview findRoomDefaultPreviewById(Integer id) throws EcgDAOException {
        return roomDefaultPreviewDao.findById(id);
    }

    @Override
    @Transactional
    public void createRoomDefaultPreview(RoomDefaultPreview defaultPreview) throws EcgDAOException {
        roomDefaultPreviewDao.create(defaultPreview);
    }

    @Override
    @Transactional
    public void updateRoomDefaultPreview(RoomDefaultPreview newValue) throws EcgDAOException {
        roomDefaultPreviewDao.update(newValue);
    }

    @Override
    @Transactional
    public void removeRoomDefaultPreview(RoomDefaultPreview defaultPreview) throws EcgDAOException {
        roomDefaultPreviewDao.remove(defaultPreview);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public long countRoomFacilitiesDefaultPreview() throws EcgDAOException {
        return roomDefaultPreviewDao.count();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<RoomDefaultPreview> findRange(int start, int end) throws EcgDAOException {
        return roomDefaultPreviewDao.findRange(start,end);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException {
        return roomDefaultPreviewDao.executeSQLQuery(query,params);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query) throws EcgDAOException {
        return roomDefaultPreviewDao.executeSQLQuery(query);
    }

    @Override
    @Transactional
    public RoomDefaultPreview mergeRoomDefaultPreview(RoomDefaultPreview defaultPreview) throws EcgDAOException {
        return roomDefaultPreviewDao.merge(defaultPreview);
    }

    @Override
    @Transactional
    public Integer saveRoomDefaultPreview(RoomDefaultPreview defaultPreview) throws EcgDAOException {
        return roomDefaultPreviewDao.save(defaultPreview);
    }
}
