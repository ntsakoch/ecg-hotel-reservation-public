package za.org.ecg.springmvc.service;

import za.org.ecg.dao.entity.Country;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/18.
 */
public interface CountryService {

    List<Country> findAllCountries() throws EcgDAOException;

    Country findCountryById(Integer id) throws EcgDAOException;

    Country findByCountryCode(String countryCode) throws EcgDAOException;

    void createCountry(Country country) throws EcgDAOException;

    void updateCountry(Country newValue) throws EcgDAOException;

    void removeCountry(Country country) throws EcgDAOException;

    long countCountries() throws EcgDAOException;

    List<Country> findRange(int start, int end) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query) throws EcgDAOException;

    Country mergeCountry(Country country) throws EcgDAOException;

    Integer saveCountry(Country country) throws EcgDAOException;
}
