package za.org.ecg.springmvc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import za.org.ecg.dao.PreferenceDao;
import za.org.ecg.dao.entity.Preference;
import za.org.ecg.dao.exception.EcgDAOException;
import za.org.ecg.springmvc.service.PreferenceService;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/16.
 */
@Service("preferenceService")
public class PreferenceServiceImpl  implements PreferenceService{

    @Autowired
    private PreferenceDao preferenceDao;

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<Preference> findAllPreferences() throws EcgDAOException {
        return preferenceDao.findAll();
    }

    @Override
    public List<Preference> findAllPreferences(Integer[] Ids) throws EcgDAOException {
        return null;
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public Preference findPreferenceById(Integer id) throws EcgDAOException {
        return preferenceDao.findById(id);
    }

    @Override
    @Transactional
    public void createPreference(Preference preference) throws EcgDAOException {
        preferenceDao.create(preference);
    }

    @Override
    @Transactional
    public void updatePreference(Preference newValue) throws EcgDAOException {
        preferenceDao.update(newValue);
    }

    @Override
    @Transactional
    public void removePreference(Preference preference) throws EcgDAOException {
        preferenceDao.remove(preference);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public long countPreferences() throws EcgDAOException {
        return preferenceDao.count();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<Preference> findRange(int start, int end) throws EcgDAOException {
        return preferenceDao.findRange(start,end);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException {
        return preferenceDao.executeSQLQuery(query,params);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query) throws EcgDAOException {
        return preferenceDao.executeSQLQuery(query);
    }

    @Override
    @Transactional
    public Preference mergePreference(Preference preference) throws EcgDAOException {
        return preferenceDao.merge(preference);
    }

    @Override
    @Transactional
    public Integer savePreference(Preference preference) throws EcgDAOException {
        return preferenceDao.save(preference);
    }
}
