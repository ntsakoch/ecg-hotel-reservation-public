package za.org.ecg.springmvc.service;

import za.org.ecg.dao.entity.Attachment;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2016/12/28.
 */
public interface AttachmentService {
    List<Attachment> findAllAttachments() throws EcgDAOException;

    Attachment findAttachmentById(Integer id) throws EcgDAOException;

    void createAttachment(Attachment attachment) throws EcgDAOException;

    void updateAttachment(Attachment newValue) throws EcgDAOException;

    void removeAttachment(Attachment attachment) throws EcgDAOException;

    long countAttachments() throws EcgDAOException;

    List<Attachment> findRange(int start, int end) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query) throws EcgDAOException;

    Attachment mergeAttachment(Attachment attachment) throws EcgDAOException;

    Integer saveAttachment(Attachment attachment) throws EcgDAOException;
}
