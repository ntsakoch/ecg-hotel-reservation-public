package za.org.ecg.springmvc.service.audit;

import java.lang.annotation.*;

/**
 * Created by Ntsako on 2017/01/24.
 */
@Target(ElementType.TYPE)
@Inherited
@Retention(RetentionPolicy.RUNTIME)
public @interface EnableAuditing {
    Class<?> implClass();
}
