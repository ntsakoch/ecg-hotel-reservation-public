package za.org.ecg.springmvc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import za.org.ecg.dao.CustomerDiscountDao;
import za.org.ecg.dao.entity.CustomerDiscount;
import za.org.ecg.dao.exception.EcgDAOException;
import za.org.ecg.springmvc.service.CustomerDiscountService;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/15.
 */
@Service("customerDiscountService")
public class CustomerDiscountServiceImpl implements CustomerDiscountService {

    @Autowired
    private CustomerDiscountDao customerDiscountDao;

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<CustomerDiscount> findAllCustomerDiscounts() throws EcgDAOException {
        return customerDiscountDao.findAll();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public CustomerDiscount findCustomerDiscountById(Integer id) throws EcgDAOException {
        return customerDiscountDao.findById(id);
    }

    @Override
    @Transactional
    public void createCustomerDiscount(CustomerDiscount discount) throws EcgDAOException {
        customerDiscountDao.create(discount);
    }

    @Override
    @Transactional
    public void updateCustomerDiscount(CustomerDiscount newValue) throws EcgDAOException {
        customerDiscountDao.update(newValue);
    }

    @Override
    @Transactional
    public void removeCustomerDiscount(CustomerDiscount discount) throws EcgDAOException {
        customerDiscountDao.remove(discount);
    }

    @Override
    @Transactional
    public long countCustomerDiscounts() throws EcgDAOException {
        return customerDiscountDao.count();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<CustomerDiscount> findRange(int start, int end) throws EcgDAOException {
        return customerDiscountDao.findRange(start,end);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException {
        return customerDiscountDao.executeSQLQuery(query,params);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Object[]> executeSQLQuery(String query) throws EcgDAOException {
        return customerDiscountDao.executeSQLQuery(query);
    }

    @Override
    @Transactional
    public CustomerDiscount mergeCustomerDiscount(CustomerDiscount discount) throws EcgDAOException {
        return customerDiscountDao.merge(discount);
    }

    @Override
    public Integer saveCustomerDiscount(CustomerDiscount discount) throws EcgDAOException {
        return customerDiscountDao.save(discount);
    }
}
