package za.org.ecg.springmvc.service;

import za.org.ecg.dao.entity.Preference;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/16.
 */
public interface PreferenceService {

    List<Preference> findAllPreferences() throws EcgDAOException;

    List<Preference> findAllPreferences(Integer[] Ids) throws EcgDAOException;

    Preference findPreferenceById(Integer id) throws EcgDAOException;

    void createPreference(Preference preference) throws EcgDAOException;

    void updatePreference(Preference newValue) throws EcgDAOException;

    void removePreference(Preference preference) throws EcgDAOException;

    long countPreferences() throws EcgDAOException;

    List<Preference> findRange(int start, int end) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query) throws EcgDAOException;

    Preference mergePreference(Preference preference) throws EcgDAOException;

    Integer savePreference(Preference preference) throws EcgDAOException;
}
