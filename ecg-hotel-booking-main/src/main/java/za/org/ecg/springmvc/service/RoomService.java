package za.org.ecg.springmvc.service;

import za.org.ecg.dao.entity.Room;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/16.
 */
public interface RoomService {

    List<Room> findAllRoomFacilities() throws EcgDAOException;

    List<Room> findTopListedRooms() throws EcgDAOException;

    List<Room> findRoomsOnSpecial() throws EcgDAOException;

    Room findRoomById(Integer id) throws EcgDAOException;

    void createRoom(Room room) throws EcgDAOException;

    void updateRoom(Room newValue) throws EcgDAOException;

    void removeRoom(Room room) throws EcgDAOException;

    long countRoomFacilities() throws EcgDAOException;

    List<Room> findRange(int start, int end) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query) throws EcgDAOException;

    Room mergeRoom(Room room) throws EcgDAOException;

    Integer saveRoom(Room room) throws EcgDAOException;

    List<Room> getAllAvailableRooms(Date checkinDate, Date checkoutDate) throws EcgDAOException;

    List<Room> getAllAvailableRooms(Date checkinDate, Date checkoutDate,int numberOfAdults, int numberOfChildren) throws EcgDAOException ;

}
