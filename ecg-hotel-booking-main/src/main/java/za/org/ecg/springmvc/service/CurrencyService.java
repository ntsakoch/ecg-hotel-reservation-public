package za.org.ecg.springmvc.service;

import za.org.ecg.dao.entity.Currency;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/18.
 */
public interface CurrencyService {

    List<Currency> findAllCurrencies() throws EcgDAOException;

    Currency findCurrencyById(Integer id) throws EcgDAOException;

    void createCurrency(Currency currency) throws EcgDAOException;

    void updateCurrency(Currency newValue) throws EcgDAOException;

    void removeCurrency(Currency currency) throws EcgDAOException;

    long countCurrencies() throws EcgDAOException;

    List<Currency> findRange(int start, int end) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query) throws EcgDAOException;

    Currency mergeCurrency(Currency currency) throws EcgDAOException;

    Integer saveCurrency(Currency currency) throws EcgDAOException;
}
