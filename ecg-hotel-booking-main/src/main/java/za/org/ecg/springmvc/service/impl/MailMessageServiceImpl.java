package za.org.ecg.springmvc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import za.org.ecg.dao.MailMessageDao;
import za.org.ecg.dao.entity.MailMessage;
import za.org.ecg.dao.exception.EcgDAOException;
import za.org.ecg.springmvc.service.MailMessageService;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/16.
 */
@Service("mailMessageService")
public class MailMessageServiceImpl implements MailMessageService {

    @Autowired
    private MailMessageDao mailMessageDao;

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<MailMessage> findAllMailMessages() throws EcgDAOException {
        return mailMessageDao.findAll();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public MailMessage findMailMessageById(Integer id) throws EcgDAOException {
        return mailMessageDao.findById(id);
    }

    @Override
    @Transactional
    public void createMailMessage(MailMessage mailMessage) throws EcgDAOException {
        mailMessageDao.create(mailMessage);
    }

    @Override
    @Transactional
    public void updateMailMessage(MailMessage newValue) throws EcgDAOException {
        mailMessageDao.update(newValue);
    }

    @Override
    @Transactional
    public void removeMailMessage(MailMessage mailMessage) throws EcgDAOException {
        mailMessageDao.remove(mailMessage);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public long countMailMessages() throws EcgDAOException {
        return mailMessageDao.count();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<MailMessage> findRange(int start, int end) throws EcgDAOException {
        return mailMessageDao.findRange(start,end);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException {
        return mailMessageDao.executeSQLQuery(query,params);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query) throws EcgDAOException {
        return mailMessageDao.executeSQLQuery(query);
    }

    @Override
    @Transactional
    public MailMessage mergeMailMessage(MailMessage mailMessage) throws EcgDAOException {
        return mailMessageDao.merge(mailMessage);
    }

    @Override
    @Transactional
    public Integer saveMailMessage(MailMessage mailMessage) throws EcgDAOException {
        return mailMessageDao.save(mailMessage);
    }
}
