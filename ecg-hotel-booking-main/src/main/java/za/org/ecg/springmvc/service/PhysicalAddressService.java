package za.org.ecg.springmvc.service;

import za.org.ecg.dao.entity.PhysicalAddress;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/16.
 */
public interface PhysicalAddressService {

    List<PhysicalAddress> findAllPhysicalAddresses() throws EcgDAOException;

    PhysicalAddress findPhysicalAddressById(Integer id) throws EcgDAOException;

    void createPhysicalAddress(PhysicalAddress physicalAddress) throws EcgDAOException;

    void updatePhysicalAddress(PhysicalAddress newValue) throws EcgDAOException;

    void removePhysicalAddress(PhysicalAddress physicalAddress) throws EcgDAOException;

    long countPhysicalAddresses() throws EcgDAOException;

    List<PhysicalAddress> findRange(int start, int end) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query) throws EcgDAOException;

    PhysicalAddress mergePhysicalAddress(PhysicalAddress physicalAddress) throws EcgDAOException;

    Integer savePhysicalAddress(PhysicalAddress physicalAddress) throws EcgDAOException;
}
