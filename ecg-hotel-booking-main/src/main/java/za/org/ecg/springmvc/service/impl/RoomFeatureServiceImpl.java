package za.org.ecg.springmvc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import za.org.ecg.dao.RoomFeatureDao;
import za.org.ecg.dao.entity.RoomFeature;
import za.org.ecg.dao.exception.EcgDAOException;
import za.org.ecg.springmvc.service.RoomFeatureService;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/16.
 */
@Service("roomFeatureService")
public class RoomFeatureServiceImpl implements RoomFeatureService{

    @Autowired
    private RoomFeatureDao roomFeatureDao;

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<RoomFeature> findAllRoomFacilities() throws EcgDAOException {
        return roomFeatureDao.findAll();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public RoomFeature findRoomFeatureById(Integer id) throws EcgDAOException {
        return roomFeatureDao.findById(id);
    }

    @Override
    @Transactional
    public void createRoomFeature(RoomFeature feature) throws EcgDAOException {
        roomFeatureDao.create(feature);
    }

    @Override
    @Transactional
    public void updateRoomFeature(RoomFeature newValue) throws EcgDAOException {
        roomFeatureDao.update(newValue);
    }

    @Override
    @Transactional
    public void removeRoomFeature(RoomFeature feature) throws EcgDAOException {
        roomFeatureDao.remove(feature);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public long countRoomFacilities() throws EcgDAOException {
        return roomFeatureDao.count();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<RoomFeature> findRange(int start, int end) throws EcgDAOException {
        return roomFeatureDao.findRange(start,end);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException {
        return roomFeatureDao.executeSQLQuery(query,params);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query) throws EcgDAOException {
        return roomFeatureDao.executeSQLQuery(query);
    }

    @Override
    @Transactional
    public RoomFeature mergeRoomFeature(RoomFeature feature) throws EcgDAOException {
        return roomFeatureDao.merge(feature);
    }

    @Override
    @Transactional
    public Integer saveRoomFeature(RoomFeature feature) throws EcgDAOException {
        return roomFeatureDao.save(feature);
    }
}
