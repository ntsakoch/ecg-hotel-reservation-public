package za.org.ecg.springmvc.service;

import za.org.ecg.dao.entity.Discount;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/16.
 */
public interface DiscountService {

    List<Discount> findAllDiscounts() throws EcgDAOException;

    Discount findDiscountById(Integer id) throws EcgDAOException;

    void createDiscount(Discount discount) throws EcgDAOException;

    void updateDiscount(Discount newValue) throws EcgDAOException;

    void removeDiscount(Discount discount) throws EcgDAOException;

    long countDiscounts() throws EcgDAOException;

    List<Discount> findRange(int start, int end) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query) throws EcgDAOException;

    Discount mergeDiscount(Discount discount) throws EcgDAOException;

    Integer saveDiscount(Discount discount) throws EcgDAOException;
}
