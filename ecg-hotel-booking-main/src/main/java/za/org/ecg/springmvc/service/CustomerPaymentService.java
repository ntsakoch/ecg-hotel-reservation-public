package za.org.ecg.springmvc.service;

import za.org.ecg.dao.entity.CustomerPayment;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/16.
 */
public interface CustomerPaymentService {
    
    List<CustomerPayment> findAllCustomerPayments() throws EcgDAOException;

    CustomerPayment findCustomerPaymentById(Integer id) throws EcgDAOException;

    void createCustomerPayment(CustomerPayment payment) throws EcgDAOException;

    void updateCustomerPayment(CustomerPayment newValue) throws EcgDAOException;

    void removeCustomerPayment(CustomerPayment payment) throws EcgDAOException;

    long countCustomerPayments() throws EcgDAOException;

    List<CustomerPayment> findRange(int start, int end) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query) throws EcgDAOException;

    CustomerPayment mergeCustomerPayment(CustomerPayment payment) throws EcgDAOException;

    Integer saveCustomerPayment(CustomerPayment payment) throws EcgDAOException;

    public List<CustomerPayment> findByCustomerIdNumber(String customerIdNumber) throws EcgDAOException;
}
