package za.org.ecg.springmvc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import za.org.ecg.dao.ContactUsMessageResponseDao;
import za.org.ecg.dao.entity.ContactUsMessageResponse;
import za.org.ecg.dao.exception.EcgDAOException;
import za.org.ecg.springmvc.service.ContactUsMessageResponseService;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2016/12/28.
 */
@Service("contactUsMessageResponseService")
public class ContactUsMessageResponseImpl implements ContactUsMessageResponseService {

    @Autowired
    private ContactUsMessageResponseDao contactUsMessageResponseDao;

    @Override
    @Transactional
    public List<ContactUsMessageResponse> findAllContactUsMessageResponses() throws EcgDAOException {
        return contactUsMessageResponseDao.findAll() ;
    }

    @Override
    @Transactional
    public ContactUsMessageResponse findContactUsMessageResponseById(Integer id) throws EcgDAOException {
        return contactUsMessageResponseDao.findById(id);
    }

    @Override
    @Transactional
    public void createContactUsMessageResponse(ContactUsMessageResponse messageResponse) throws EcgDAOException {
        contactUsMessageResponseDao.create(messageResponse);
    }

    @Override
    @Transactional
    public void updateContactUsMessageResponse(ContactUsMessageResponse newValue) throws EcgDAOException {
        contactUsMessageResponseDao.update(newValue);
    }

    @Override
    @Transactional
    public void removeContactUsMessageResponse(ContactUsMessageResponse messageResponse) throws EcgDAOException {
        contactUsMessageResponseDao.remove(messageResponse);
    }

    @Override
    @Transactional
    public long countContactUsMessageResponses() throws EcgDAOException {
        return contactUsMessageResponseDao.count();
    }

    @Override
    @Transactional
    public List<ContactUsMessageResponse> findRange(int start, int end) throws EcgDAOException {
        return contactUsMessageResponseDao.findRange(start,end);
    }

    @Override
    @Transactional
    public List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException {
        return contactUsMessageResponseDao.executeSQLQuery(query,params);
    }

    @Override
    @Transactional
    public List<Object[]> executeSQLQuery(String query) throws EcgDAOException {
        return contactUsMessageResponseDao.executeSQLQuery(query);
    }

    @Override
    @Transactional
    public ContactUsMessageResponse mergeContactUsMessageResponse(ContactUsMessageResponse messageResponse) throws EcgDAOException {
        return contactUsMessageResponseDao.merge(messageResponse);
    }

    @Override
    @Transactional
    public Integer saveContactUsMessageResponse(ContactUsMessageResponse messageResponse) throws EcgDAOException {
        return contactUsMessageResponseDao.save(messageResponse);
    }

    @Override
    public List<ContactUsMessageResponse> findByOriginalContactUsMessageId(int originalMessageId) throws EcgDAOException {
        return contactUsMessageResponseDao.findByOriginalContactUsMessageId(originalMessageId);
    }
}
