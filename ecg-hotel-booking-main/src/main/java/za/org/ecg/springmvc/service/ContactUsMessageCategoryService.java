package za.org.ecg.springmvc.service;

import za.org.ecg.dao.entity.ContactUsMessageCategory;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2016/12/28.
 */
public interface  ContactUsMessageCategoryService {
    List<ContactUsMessageCategory> findAllContactUsMessageCategorys() throws EcgDAOException;

    ContactUsMessageCategory findContactUsMessageCategoryById(Integer id) throws EcgDAOException;

    void createContactUsMessageCategory(ContactUsMessageCategory messageCategory) throws EcgDAOException;

    void updateContactUsMessageCategory(ContactUsMessageCategory newValue) throws EcgDAOException;

    void removeContactUsMessageCategory(ContactUsMessageCategory messageCategory) throws EcgDAOException;

    long countContactUsMessageCategories() throws EcgDAOException;

    List<ContactUsMessageCategory> findRange(int start, int end) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query) throws EcgDAOException;

    ContactUsMessageCategory mergeContactUsMessageCategory(ContactUsMessageCategory messageCategory) throws EcgDAOException;

    Integer saveContactUsMessageCategory(ContactUsMessageCategory messageCategory) throws EcgDAOException;
}
