package za.org.ecg.springmvc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import za.org.ecg.dao.AttachmentDao;
import za.org.ecg.dao.entity.Attachment;
import za.org.ecg.dao.exception.EcgDAOException;
import za.org.ecg.springmvc.service.AttachmentService;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2016/12/28.
 */
@Service("attachmentService")
public class AttachmentServiceImpl implements AttachmentService {

    @Autowired
    private AttachmentDao attachmentDao;

    @Override
    @Transactional
    public List<Attachment> findAllAttachments() throws EcgDAOException {
        return attachmentDao.findAll() ;
    }

    @Override
    @Transactional
    public Attachment findAttachmentById(Integer id) throws EcgDAOException {
        return attachmentDao.findById(id);
    }

    @Override
    @Transactional
    public void createAttachment(Attachment applicationProperty) throws EcgDAOException {
        attachmentDao.create(applicationProperty);
    }

    @Override
    @Transactional
    public void updateAttachment(Attachment newValue) throws EcgDAOException {
        attachmentDao.update(newValue);
    }

    @Override
    @Transactional
    public void removeAttachment(Attachment applicationProperty) throws EcgDAOException {
        attachmentDao.remove(applicationProperty);
    }

    @Override
    @Transactional
    public long countAttachments() throws EcgDAOException {
        return attachmentDao.count();
    }

    @Override
    @Transactional
    public List<Attachment> findRange(int start, int end) throws EcgDAOException {
        return attachmentDao.findRange(start,end);
    }

    @Override
    @Transactional
    public List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException {
        return attachmentDao.executeSQLQuery(query,params);
    }

    @Override
    @Transactional
    public List<Object[]> executeSQLQuery(String query) throws EcgDAOException {
        return attachmentDao.executeSQLQuery(query);
    }

    @Override
    @Transactional
    public Attachment mergeAttachment(Attachment applicationProperty) throws EcgDAOException {
        return attachmentDao.merge(applicationProperty);
    }

    @Override
    @Transactional
    public Integer saveAttachment(Attachment applicationProperty) throws EcgDAOException {
        return attachmentDao.save(applicationProperty);
    }
}
