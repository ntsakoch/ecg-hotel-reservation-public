package za.org.ecg.springmvc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import za.org.ecg.dao.CountryDao;
import za.org.ecg.dao.entity.Country;
import za.org.ecg.dao.exception.EcgDAOException;
import za.org.ecg.springmvc.service.CountryService;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/18.
 */
@Service("countryService")
public class CountryServiceImpl implements CountryService{

    @Autowired
    private CountryDao countryDao;

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED,propagation = Propagation.SUPPORTS)
    public List<Country> findAllCountries() throws EcgDAOException {
        return countryDao.findAll();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED,propagation = Propagation.SUPPORTS)
    public Country findCountryById(Integer id) throws EcgDAOException {
        return countryDao.findById(id);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public Country findByCountryCode(String countryCode) throws EcgDAOException {
        return countryDao.findByCountryCode(countryCode);
    }

    @Override
    @Transactional
    public void createCountry(Country country) throws EcgDAOException {
        countryDao.create(country);
    }

    @Override
    @Transactional
    public void updateCountry(Country newValue) throws EcgDAOException {
        countryDao.update(newValue);
    }

    @Override
    @Transactional
    public void removeCountry(Country country) throws EcgDAOException {
        countryDao.remove(country);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public long countCountries() throws EcgDAOException {
        return countryDao.count();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<Country> findRange(int start, int end) throws EcgDAOException {
        return countryDao.findRange(start,end);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException {
        return countryDao.executeSQLQuery(query,params);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query) throws EcgDAOException {
        return countryDao.executeSQLQuery(query);
    }

    @Override
    @Transactional
    public Country mergeCountry(Country country) throws EcgDAOException {
        return countryDao.merge(country);
    }

    @Override
    @Transactional
    public Integer saveCountry(Country country) throws EcgDAOException {
        return countryDao.save(country);
    }
}
