package za.org.ecg.springmvc.service;

import za.org.ecg.dao.entity.EntityBean;
import za.org.ecg.dao.exception.EcgDAOException;
import za.org.ecg.dao.impl.AbstractDao;

import java.util.List;

/**
 * Created by Ntsako on 2017/01/24.
 */
public interface EntityCacheDaoService {
    List<? extends EntityBean> findAll(AbstractDao abstractDao) throws EcgDAOException;
}
