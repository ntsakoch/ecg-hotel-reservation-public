package za.org.ecg.springmvc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import za.org.ecg.dao.CostDao;
import za.org.ecg.dao.entity.Cost;
import za.org.ecg.dao.exception.EcgDAOException;
import za.org.ecg.springmvc.service.CostService;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/18.
 */
@Service("costService")
public class CostServiceImpl implements CostService{

    @Autowired
    private CostDao costDao;

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<Cost> findAllCosts() throws EcgDAOException {
        return costDao.findAll();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public Cost findCostById(Integer id) throws EcgDAOException {
        return costDao.findById(id);
    }

    @Override
    @Transactional
    public void createCost(Cost cost) throws EcgDAOException {
        costDao.create(cost);
    }

    @Override
    @Transactional
    public void updateCost(Cost newValue) throws EcgDAOException {
        costDao.update(newValue);
    }

    @Override
    @Transactional
    public void removeCost(Cost cost) throws EcgDAOException {
        costDao.remove(cost);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public long countCosts() throws EcgDAOException {
        return costDao.count();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<Cost> findRange(int start, int end) throws EcgDAOException {
        return costDao.findRange(start,end);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException {
        return costDao.executeSQLQuery(query,params);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query) throws EcgDAOException {
        return costDao.executeSQLQuery(query);
    }

    @Override
    @Transactional
    public Cost mergeCost(Cost cost) throws EcgDAOException {
        return costDao.merge(cost);
    }

    @Override
    @Transactional
    public Integer saveCost(Cost cost) throws EcgDAOException {
        return costDao.save(cost);
    }
}
