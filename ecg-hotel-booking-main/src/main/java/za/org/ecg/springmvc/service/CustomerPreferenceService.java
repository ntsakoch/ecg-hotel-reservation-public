package za.org.ecg.springmvc.service;

import za.org.ecg.dao.entity.CustomerPreference;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/16.
 */
public interface CustomerPreferenceService {

    List<CustomerPreference> findAllCustomerPreferences() throws EcgDAOException;

    List<CustomerPreference> findAllCustomerPreferences(Integer[] preferenceIds) throws EcgDAOException;

    CustomerPreference findCustomerPreferenceById(Integer id) throws EcgDAOException;

    void createCustomerPreference(CustomerPreference preference) throws EcgDAOException;

    void updateCustomerPreference(CustomerPreference newValue) throws EcgDAOException;

    void removeCustomerPreference(CustomerPreference preference) throws EcgDAOException;

    long countCustomerPreferences() throws EcgDAOException;

    List<CustomerPreference> findRange(int start, int end) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query) throws EcgDAOException;

    CustomerPreference mergeCustomerPreference(CustomerPreference preference) throws EcgDAOException;

    Integer saveCustomerPreference(CustomerPreference preference) throws EcgDAOException;
}
