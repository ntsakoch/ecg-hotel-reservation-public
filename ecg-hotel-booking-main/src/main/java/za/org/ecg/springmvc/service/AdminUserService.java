package za.org.ecg.springmvc.service;

import za.org.ecg.dao.entity.AdminUser;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2016/12/28.
 */
public interface AdminUserService  {

    List<AdminUser> findAllAdminUsers() throws EcgDAOException;

    AdminUser findAdminUserById(Integer id) throws EcgDAOException;

    void createAdminUser(AdminUser user) throws EcgDAOException;

    void updateAdminUser(AdminUser newValue) throws EcgDAOException;

    void removeAdminUser(AdminUser user) throws EcgDAOException;

    long countAdminUsers() throws EcgDAOException;

    List<AdminUser> findRange(int start, int end) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query) throws EcgDAOException;

    AdminUser mergeAdminUser(AdminUser user) throws EcgDAOException;

    Integer saveAdminUser(AdminUser user) throws EcgDAOException;
}
