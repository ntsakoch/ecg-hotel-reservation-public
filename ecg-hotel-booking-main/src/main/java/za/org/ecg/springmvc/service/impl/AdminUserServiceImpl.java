package za.org.ecg.springmvc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import za.org.ecg.dao.AdminUserDao;
import za.org.ecg.dao.entity.AdminUser;
import za.org.ecg.dao.exception.EcgDAOException;
import za.org.ecg.springmvc.service.AdminUserService;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2016/12/28.
 */
@Service("adminUserService")
public class AdminUserServiceImpl implements AdminUserService {

    @Autowired
    private AdminUserDao adminUserDao;

    @Override
    @Transactional
    public List<AdminUser> findAllAdminUsers() throws EcgDAOException {
        return adminUserDao.findAll();
    }

    @Override
    @Transactional
    public AdminUser findAdminUserById(Integer id) throws EcgDAOException {
        return adminUserDao.findById(id);
    }

    @Override
    @Transactional
    public void createAdminUser(AdminUser user) throws EcgDAOException {
        adminUserDao.create(user);
    }

    @Override
    @Transactional
    public void updateAdminUser(AdminUser newValue) throws EcgDAOException {
        adminUserDao.update(newValue);
    }

    @Override
    @Transactional
    public void removeAdminUser(AdminUser user) throws EcgDAOException {
        adminUserDao.remove(user);
    }

    @Override
    @Transactional
    public long countAdminUsers() throws EcgDAOException {
        return adminUserDao.count();
    }

    @Override
    @Transactional
    public List<AdminUser> findRange(int start, int end) throws EcgDAOException {
        return adminUserDao.findRange(start,end);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException {
        return adminUserDao.executeSQLQuery(query,params);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Object[]> executeSQLQuery(String query) throws EcgDAOException {
        return adminUserDao.executeSQLQuery(query);
    }

    @Override
    public AdminUser mergeAdminUser(AdminUser user) throws EcgDAOException {
        return adminUserDao.merge(user);
    }

    @Override
    @Transactional
    public Integer saveAdminUser(AdminUser user) throws EcgDAOException {
        return adminUserDao.save(user);
    }
}
