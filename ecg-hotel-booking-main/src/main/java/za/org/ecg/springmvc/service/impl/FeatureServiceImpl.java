package za.org.ecg.springmvc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import za.org.ecg.dao.FeatureDao;
import za.org.ecg.dao.entity.Feature;
import za.org.ecg.dao.exception.EcgDAOException;
import za.org.ecg.springmvc.service.FeatureService;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/16.
 */
@Service("featureService")
public class FeatureServiceImpl implements FeatureService{

    @Autowired
    private FeatureDao featureDao;

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<Feature> findAllFeatures() throws EcgDAOException {
        return featureDao.findAll();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public Feature findFeatureById(Integer id) throws EcgDAOException {
        return featureDao.findById(id);
    }

    @Override
    @Transactional
    public void createFeature(Feature feature) throws EcgDAOException {
        featureDao.create(feature);
    }

    @Override
    @Transactional
    public void updateFeature(Feature newValue) throws EcgDAOException {
        featureDao.update(newValue);
    }

    @Override
    @Transactional
    public void removeFeature(Feature feature) throws EcgDAOException {
        featureDao.remove(feature);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public long countFeatures() throws EcgDAOException {
        return featureDao.count();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<Feature> findRange(int start, int end) throws EcgDAOException {
        return featureDao.findRange(start,end);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException {
        return featureDao.executeSQLQuery(query,params);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query) throws EcgDAOException {
        return featureDao.executeSQLQuery(query);
    }

    @Override
    @Transactional
    public Feature mergeFeature(Feature feature) throws EcgDAOException {
        return featureDao.merge(feature);
    }

    @Override
    @Transactional
    public Integer saveFeature(Feature feature) throws EcgDAOException {
        return featureDao.save(feature);
    }
}
