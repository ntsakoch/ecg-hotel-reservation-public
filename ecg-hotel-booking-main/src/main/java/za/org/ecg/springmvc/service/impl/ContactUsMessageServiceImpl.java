package za.org.ecg.springmvc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import za.org.ecg.dao.ContactUsMessageDao;
import za.org.ecg.dao.entity.ContactUsMessage;
import za.org.ecg.dao.exception.EcgDAOException;
import za.org.ecg.springmvc.service.ContactUsMessageService;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2016/12/28.
 */
@Service("contactUsMessageService")
public class ContactUsMessageServiceImpl implements ContactUsMessageService{

    @Autowired
    private ContactUsMessageDao contactUsMessageDao;

    @Override
    @Transactional
    public List<ContactUsMessage> findAllContactUsMessages() throws EcgDAOException {
        return contactUsMessageDao.findAll();
    }

    @Override
    @Transactional
    public ContactUsMessage findContactUsMessageById(Integer id) throws EcgDAOException {
        return contactUsMessageDao.findById(id);
    }

    @Override
    @Transactional
    public void createContactUsMessage(ContactUsMessage message) throws EcgDAOException {
        contactUsMessageDao.create(message);
    }

    @Override
    @Transactional
    public void updateContactUsMessage(ContactUsMessage newValue) throws EcgDAOException {
        contactUsMessageDao.update(newValue);
    }

    @Override
    @Transactional
    public void removeContactUsMessage(ContactUsMessage message) throws EcgDAOException {
        contactUsMessageDao.remove(message);
    }

    @Override
    @Transactional
    public long countContactUsMessages() throws EcgDAOException {
        return contactUsMessageDao.count();
    }

    @Override
    @Transactional
    public List<ContactUsMessage> findRange(int start, int end) throws EcgDAOException {
        return contactUsMessageDao.findRange(start,end);
    }

    @Override
    @Transactional
    public List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException {
        return contactUsMessageDao.executeSQLQuery(query,params);
    }

    @Override
    @Transactional
    public List<Object[]> executeSQLQuery(String query) throws EcgDAOException {
        return contactUsMessageDao.executeSQLQuery(query);
    }

    @Override
    @Transactional
    public ContactUsMessage mergeContactUsMessage(ContactUsMessage message) throws EcgDAOException {
        return contactUsMessageDao.merge(message);
    }

    @Override
    @Transactional
    public Integer saveContactUsMessage(ContactUsMessage message) throws EcgDAOException {
        return contactUsMessageDao.save(message);
    }

    @Override
    @Transactional
    public List<ContactUsMessage> findContactUsMessageByUserId(int userId) throws EcgDAOException {
        return contactUsMessageDao.findByUserId(userId);
    }

    @Override
    @Transactional
    public List<ContactUsMessage> findContactUsMessageByStatus(String status) throws EcgDAOException {
        return contactUsMessageDao.findByStatus(status);
    }

    @Override
    @Transactional
    public List<ContactUsMessage> findContactUsMessageBySenderFullName(String fullName) throws EcgDAOException {
        return contactUsMessageDao.findByFullName(fullName);
    }

    @Override
    @Transactional
    public List<ContactUsMessage> findContactUsMessageByDateRange(Date fromDate, Date toDate) throws EcgDAOException {
        return contactUsMessageDao.findByDateRange(fromDate,toDate);
    }
}
