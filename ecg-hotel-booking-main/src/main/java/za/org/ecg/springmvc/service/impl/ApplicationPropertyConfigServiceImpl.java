package za.org.ecg.springmvc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import za.org.ecg.dao.ApplicationPropertyConfigDao;
import za.org.ecg.dao.entity.ApplicationPropertyConfig;
import za.org.ecg.dao.exception.EcgDAOException;
import za.org.ecg.springmvc.service.ApplicationPropertyConfigService;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2016/12/28.
 */
@Service("applicationPropertyConfigService")
public class ApplicationPropertyConfigServiceImpl implements ApplicationPropertyConfigService {

    @Autowired
    private ApplicationPropertyConfigDao applicationPropertyConfigDao;

    @Override
    @Transactional
    public List<ApplicationPropertyConfig> findAllApplicationPropertyConfigs() throws EcgDAOException {
        return applicationPropertyConfigDao.findAll() ;
    }

    @Override
    @Transactional
    public ApplicationPropertyConfig findApplicationPropertyConfigById(Integer id) throws EcgDAOException {
        return applicationPropertyConfigDao.findById(id);
    }

    @Override
    @Transactional
    public void createApplicationPropertyConfig(ApplicationPropertyConfig applicationProperty) throws EcgDAOException {
        applicationPropertyConfigDao.create(applicationProperty);
    }

    @Override
    @Transactional
    public void updateApplicationPropertyConfig(ApplicationPropertyConfig newValue) throws EcgDAOException {
        applicationPropertyConfigDao.update(newValue);
    }

    @Override
    @Transactional
    public void removeApplicationPropertyConfig(ApplicationPropertyConfig applicationProperty) throws EcgDAOException {
        applicationPropertyConfigDao.remove(applicationProperty);
    }

    @Override
    @Transactional
    public long countApplicationPropertyConfigs() throws EcgDAOException {
        return applicationPropertyConfigDao.count();
    }

    @Override
    @Transactional
    public List<ApplicationPropertyConfig> findRange(int start, int end) throws EcgDAOException {
        return applicationPropertyConfigDao.findRange(start,end);
    }

    @Override
    @Transactional
    public List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException {
        return applicationPropertyConfigDao.executeSQLQuery(query,params);
    }

    @Override
    @Transactional
    public List<Object[]> executeSQLQuery(String query) throws EcgDAOException {
        return applicationPropertyConfigDao.executeSQLQuery(query);
    }

    @Override
    @Transactional
    public ApplicationPropertyConfig mergeApplicationPropertyConfig(ApplicationPropertyConfig applicationProperty) throws EcgDAOException {
        return applicationPropertyConfigDao.merge(applicationProperty);
    }

    @Override
    @Transactional
    public Integer saveApplicationPropertyConfig(ApplicationPropertyConfig applicationProperty) throws EcgDAOException {
        return applicationPropertyConfigDao.save(applicationProperty);
    }
}
