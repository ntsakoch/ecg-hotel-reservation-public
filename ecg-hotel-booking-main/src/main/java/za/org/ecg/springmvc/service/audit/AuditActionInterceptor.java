package za.org.ecg.springmvc.service.audit;



import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import za.org.ecg.dao.entity.ActionAudit;
import za.org.ecg.springmvc.service.ActionAuditService;
import za.org.ecg.springmvc.service.impl.UserServiceImpl;

import java.lang.reflect.Method;
import java.util.Date;

/**
 * Created by Ntsako on 2017/01/21.
 */
public class AuditActionInterceptor implements MethodInterceptor {

    private static final Logger LOGGER=Logger.getLogger(AuditActionInterceptor.class);

    @Autowired
    private ActionAuditService actionAuditService;

    @Override
    public Object invoke(MethodInvocation methodInvocation) throws Throwable {
        String methodName=methodInvocation.getMethod().getName();
        LOGGER.debug("Intercepting action: "+methodName);
        ActionAudit auditInfo=new ActionAudit();
        try
        {
            Object result=null;
            Method declaringMethod=methodInvocation.getMethod();
            boolean isAuditingEnabled=declaringMethod.isAnnotationPresent(Audit.class);
            Audit auditMetaData =null;
            if(isAuditingEnabled)
            {
                auditMetaData=declaringMethod.getDeclaredAnnotation(Audit.class);;
            }
            else if(declaringMethod.getDeclaringClass().isInterface()&&!isAuditingEnabled)
            {
                if(declaringMethod.getDeclaringClass().isAnnotationPresent(EnableAuditing.class))
                {
                    EnableAuditing enableAuditingMetaData=declaringMethod.getDeclaringClass().getDeclaredAnnotation(EnableAuditing.class);
                    Class implClass=enableAuditingMetaData.implClass();
                    Method overridenMethod=implClass.getMethod(methodName,declaringMethod.getParameterTypes());
                    if(overridenMethod.isAnnotationPresent(Audit.class))
                    {
                       auditMetaData=overridenMethod.getAnnotation(Audit.class);
                        isAuditingEnabled=true;
                    }
                }
            }

           if( isAuditingEnabled)
           {
               ActionAuditType actionType= auditMetaData.actionType();
               String actionDescription= auditMetaData.actionDescription();
               LOGGER.info("Audit method...actionType="+actionType+", actionDescription="+actionDescription);
               auditInfo.setUserId(AuditUserIdTracker.getAuditUserId());
               LOGGER.info("Logged In user: "+auditInfo.getUserId());
               auditInfo.setCompleted(false);
               auditInfo.setStartTime(new Date());
               auditInfo.setEndTime(new Date());
               auditInfo.setActionType(actionType.name());
               auditInfo.setActionDescription(actionDescription);
               auditInfo.setSystemProcessName(methodName);
               int id=actionAuditService.saveActionAudit(auditInfo);
               auditInfo.setActionAuditId(id);
               LOGGER.info("Auditing user: "+auditInfo.getUserId());
              result= methodInvocation.proceed();
              if(result!=null) {
                  if (!(result instanceof Void)&&auditMetaData.isActionResult()) {
                      auditInfo.setActionResult(result.toString());
                  }
              }
             auditInfo.setEndTime(new Date());
             auditInfo.setCompleted(true);
             actionAuditService.updateActionAudit(auditInfo);
           }
           return result==null?methodInvocation.proceed():result;
        }
        catch (Exception exc)
        {
            try {
                auditInfo.setErrorDescription(exc.getMessage());
                auditInfo.setEndTime(new Date());
                auditInfo.setCompleted(true);
                LOGGER.error("Error intercepting method: " + methodInvocation.getMethod().getName(), exc);
                actionAuditService.updateActionAudit(auditInfo);
            }
            finally {
                return methodInvocation.proceed();
            }
        }
    }
}
