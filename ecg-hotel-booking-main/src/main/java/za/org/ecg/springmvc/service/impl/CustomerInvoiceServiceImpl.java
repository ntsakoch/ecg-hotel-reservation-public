package za.org.ecg.springmvc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import za.org.ecg.dao.CustomerInvoiceDao;
import za.org.ecg.dao.entity.CustomerInvoice;
import za.org.ecg.dao.exception.EcgDAOException;
import za.org.ecg.springmvc.service.CustomerInvoiceService;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/15.
 */
@Service("customerInvoiceService")
public class CustomerInvoiceServiceImpl implements CustomerInvoiceService{

    @Autowired
    private CustomerInvoiceDao customerInvoiceDao;

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<CustomerInvoice> findAllCustomerInvoices() throws EcgDAOException {
        return customerInvoiceDao.findAll();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public CustomerInvoice findCustomerInvoiceById(Integer id) throws EcgDAOException {
        return customerInvoiceDao.findById(id);
    }

    @Override
    @Transactional
    public void createCustomerInvoice(CustomerInvoice invoice) throws EcgDAOException {
        customerInvoiceDao.create(invoice);
    }

    @Override
    @Transactional
    public void updateCustomerInvoice(CustomerInvoice newValue) throws EcgDAOException {
        customerInvoiceDao.update(newValue);
    }

    @Override
    @Transactional
    public void removeCustomerInvoice(CustomerInvoice invoice) throws EcgDAOException {
        customerInvoiceDao.remove(invoice);
    }

    @Override
    @Transactional
    public long countCustomerInvoices() throws EcgDAOException {
        return customerInvoiceDao.count();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<CustomerInvoice> findRange(int start, int end) throws EcgDAOException {
        return customerInvoiceDao.findRange(start,start);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException {
        return customerInvoiceDao.executeSQLQuery(query,params);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query) throws EcgDAOException {
        return customerInvoiceDao.executeSQLQuery(query);
    }

    @Override
    @Transactional
    public CustomerInvoice mergeCustomerInvoice(CustomerInvoice invoice) throws EcgDAOException {
        return customerInvoiceDao.merge(invoice);
    }

    @Override
    @Transactional
    public Integer saveCustomerInvoice(CustomerInvoice invoice) throws EcgDAOException {
        return customerInvoiceDao.save(invoice);
    }
}
