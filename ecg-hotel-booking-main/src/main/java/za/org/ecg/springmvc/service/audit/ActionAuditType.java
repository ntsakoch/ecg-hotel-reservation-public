package za.org.ecg.springmvc.service.audit;


/**
 * Created by Ntsako on 2017/01/22.
 */
public enum ActionAuditType {
    USER_RETRIEVE,USER_ADD,USER_UPDATE,USER_DELETE,SYS_RETREIVE,SYS_ADD,SYS_UPDATE,SYS_DELETE,UN_SPECIFIED;
}
