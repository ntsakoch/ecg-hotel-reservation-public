package za.org.ecg.springmvc.service.audit;

import java.util.Date;

/**
 * Created by Ntsako on 2017/01/21.
 */
public class AuditInfo {

    private int actionAuditId;
    private int userId;
    private String actionType;
    private Date startTime;
    private Date endTime;
    private String sessionId;
    private boolean completed;
    private String actionResult;
    private String errorDescription;
    private String actionDescription;
    private String systemProcessName;

    public int getActionAuditId() {
        return actionAuditId;
    }

    public void setActionAuditId(int actionAuditId) {
        this.actionAuditId = actionAuditId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public String getActionResult() {
        return actionResult;
    }

    public void setActionResult(String actionResult) {
        this.actionResult = actionResult;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public String getActionDescription() {
        return actionDescription;
    }

    public void setActionDescription(String actionDescription) {
        this.actionDescription = actionDescription;
    }

    public String getSystemProcessName() {
        return systemProcessName;
    }

    public void setSystemProcessName(String systemProcessName) {
        this.systemProcessName = systemProcessName;
    }
}
