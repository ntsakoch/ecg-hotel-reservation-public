package za.org.ecg.springmvc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import za.org.ecg.dao.PreferenceCategoryDao;
import za.org.ecg.dao.entity.PreferenceCategory;
import za.org.ecg.dao.exception.EcgDAOException;
import za.org.ecg.springmvc.service.PreferenceCategoryService;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/16.
 */
@Service("preferenceCategoryService")
public class PreferenceCategoryServiceImpl implements PreferenceCategoryService{

    @Autowired
    private PreferenceCategoryDao preferenceCategoryDao;

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<PreferenceCategory> findAllPreferenceCategories() throws EcgDAOException {
        return preferenceCategoryDao.findAll();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public PreferenceCategory findPreferenceCategoryById(Integer id) throws EcgDAOException {
        return preferenceCategoryDao.findById(id);
    }

    @Override
    @Transactional
    public void createPreferenceCategory(PreferenceCategory preferenceCategory) throws EcgDAOException {
        preferenceCategoryDao.create(preferenceCategory);
    }

    @Override
    @Transactional
    public void updatePreferenceCategory(PreferenceCategory newValue) throws EcgDAOException {
        preferenceCategoryDao.update(newValue);
    }

    @Override
    @Transactional
    public void removePreferenceCategory(PreferenceCategory preferenceCategory) throws EcgDAOException {
        preferenceCategoryDao.remove(preferenceCategory);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public long countPreferenceCategories() throws EcgDAOException {
        return preferenceCategoryDao.count();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<PreferenceCategory> findRange(int start, int end) throws EcgDAOException {
        return preferenceCategoryDao.findRange(start,end);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException {
        return preferenceCategoryDao.executeSQLQuery(query,params);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query) throws EcgDAOException {
        return preferenceCategoryDao.executeSQLQuery(query);
    }

    @Override
    @Transactional
    public PreferenceCategory mergePreferenceCategory(PreferenceCategory preferenceCategory) throws EcgDAOException {
        return preferenceCategoryDao.merge(preferenceCategory);
    }

    @Override
    @Transactional
    public Integer savePreferenceCategory(PreferenceCategory preferenceCategory) throws EcgDAOException {
        return preferenceCategoryDao.save(preferenceCategory);
    }
}
