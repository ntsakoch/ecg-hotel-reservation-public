package za.org.ecg.springmvc.service;

import za.org.ecg.dao.entity.Customer;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/12.
 */
public interface CustomerService {

    List<Customer> findAllCustomers() throws EcgDAOException;

    Customer findCustomerById(Integer id) throws EcgDAOException;

    Customer findByCustomerIdNumber(String idNo) throws EcgDAOException;

    void createCustomer(Customer customer) throws EcgDAOException;

    void updateCustomer(Customer newValue) throws EcgDAOException;

    void removeCustomer(Customer customer) throws EcgDAOException;

    long countCustomers() throws EcgDAOException;

    List<Customer> findRange(int start, int end) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query) throws EcgDAOException;

    Customer mergeCustomer(Customer customer) throws EcgDAOException;

    Integer saveCustomer(Customer customer) throws EcgDAOException;
}
