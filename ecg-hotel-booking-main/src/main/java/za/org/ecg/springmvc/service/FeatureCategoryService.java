package za.org.ecg.springmvc.service;

import za.org.ecg.dao.entity.FeatureCategory;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/16.
 */
public interface FeatureCategoryService {

    List<FeatureCategory> findAllFeatureCategories() throws EcgDAOException;

    FeatureCategory findFeatureCategoryById(Integer id) throws EcgDAOException;

    void createFeatureCategory(FeatureCategory featureCategory) throws EcgDAOException;

    void updateFeatureCategory(FeatureCategory newValue) throws EcgDAOException;

    void removeFeatureCategory(FeatureCategory featureCategory) throws EcgDAOException;

    long countFeatureCategories() throws EcgDAOException;

    List<FeatureCategory> findRange(int start, int end) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query) throws EcgDAOException;

    FeatureCategory mergeFeatureCategory(FeatureCategory featureCategory) throws EcgDAOException;

    Integer saveFeatureCategory(FeatureCategory featureCategory) throws EcgDAOException;
}
