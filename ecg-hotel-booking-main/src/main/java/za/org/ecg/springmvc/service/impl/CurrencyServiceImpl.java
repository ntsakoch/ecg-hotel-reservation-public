package za.org.ecg.springmvc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import za.org.ecg.dao.CurrencyDao;
import za.org.ecg.dao.entity.Currency;
import za.org.ecg.dao.exception.EcgDAOException;
import za.org.ecg.springmvc.service.CurrencyService;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/18.
 */
@Service("currencyService")
public class CurrencyServiceImpl implements CurrencyService{

    @Autowired
    private CurrencyDao currencyDao;

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<Currency> findAllCurrencies() throws EcgDAOException {
        return currencyDao.findAll();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public Currency findCurrencyById(Integer id) throws EcgDAOException {
        return currencyDao.findById(id);
    }

    @Override
    @Transactional
    public void createCurrency(Currency currency) throws EcgDAOException {
        currencyDao.create(currency);
    }

    @Override
    @Transactional
    public void updateCurrency(Currency newValue) throws EcgDAOException {
        currencyDao.update(newValue);
    }

    @Override
    @Transactional
    public void removeCurrency(Currency currency) throws EcgDAOException {
        currencyDao.remove(currency);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public long countCurrencies() throws EcgDAOException {
        return currencyDao.count();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<Currency> findRange(int start, int end) throws EcgDAOException {
        return currencyDao.findRange(start,end);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException {
        return currencyDao.executeSQLQuery(query,params);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query) throws EcgDAOException {
        return currencyDao.executeSQLQuery(query);
    }

    @Override
    @Transactional
    public Currency mergeCurrency(Currency currency) throws EcgDAOException {
        return currencyDao.merge(currency);
    }

    @Override
    @Transactional
    public Integer saveCurrency(Currency currency) throws EcgDAOException {
        return currencyDao.save(currency);
    }
}
