package za.org.ecg.springmvc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import za.org.ecg.dao.RoomGalleryDao;
import za.org.ecg.dao.entity.RoomGallery;
import za.org.ecg.dao.exception.EcgDAOException;
import za.org.ecg.springmvc.service.RoomGalleryService;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/16.
 */
@Service("roomGalleryService")
public class RoomGalleryServiceImpl implements RoomGalleryService {

    @Autowired
    private RoomGalleryDao roomGalleryDao;

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<RoomGallery> findAllRoomFacilitiesGallery() throws EcgDAOException {
        return roomGalleryDao.findAll();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public RoomGallery findRoomGalleryById(Integer id) throws EcgDAOException {
        return roomGalleryDao.findById(id);
    }

    @Override
    @Transactional
    public void createRoomGallery(RoomGallery gallery) throws EcgDAOException {
        roomGalleryDao.create(gallery);
    }

    @Override
    @Transactional
    public void updateRoomGallery(RoomGallery newValue) throws EcgDAOException {
        roomGalleryDao.update(newValue);
    }

    @Override
    @Transactional
    public void removeRoomGallery(RoomGallery gallery) throws EcgDAOException {
        roomGalleryDao.remove(gallery);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public long countRoomFacilitiesGallery() throws EcgDAOException {
        return roomGalleryDao.count();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<RoomGallery> findRange(int start, int end) throws EcgDAOException {
        return roomGalleryDao.findRange(start,end);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException {
        return roomGalleryDao.executeSQLQuery(query,params);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query) throws EcgDAOException {
        return roomGalleryDao.executeSQLQuery(query);
    }

    @Override
    @Transactional
    public RoomGallery mergeRoomGallery(RoomGallery gallery) throws EcgDAOException {
        return roomGalleryDao.merge(gallery);
    }

    @Override
    @Transactional
    public Integer saveRoomGallery(RoomGallery gallery) throws EcgDAOException {
        return roomGalleryDao.save(gallery);
    }
}
