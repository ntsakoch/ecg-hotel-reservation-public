package za.org.ecg.springmvc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import za.org.ecg.dao.ActionAuditDao;
import za.org.ecg.dao.entity.ActionAudit;
import za.org.ecg.dao.exception.EcgDAOException;
import za.org.ecg.springmvc.service.ActionAuditService;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/21.
 */
@Service("actionAuditService")
public class ActionAuditServiceImpl  implements ActionAuditService{

    @Autowired
    private ActionAuditDao actionAuditDao;

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<ActionAudit> findAllActionAudits() throws EcgDAOException {
        return actionAuditDao.findAll();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public ActionAudit findActionAuditById(Integer id) throws EcgDAOException {
        return actionAuditDao.findById(id);
    }

    @Override
    @Transactional()
    public void createActionAudit(ActionAudit actionAudit) throws EcgDAOException {
        actionAuditDao.create(actionAudit);
    }

    @Override
    @Transactional
    public void updateActionAudit(ActionAudit newValue) throws EcgDAOException {
        actionAuditDao.update(newValue);
    }

    @Override
    @Transactional
    public void removeActionAudit(ActionAudit actionAudit) throws EcgDAOException {
        actionAuditDao.remove(actionAudit);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public long countActionAudits() throws EcgDAOException {
        return actionAuditDao.count();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<ActionAudit> findRange(int start, int end) throws EcgDAOException {
        return actionAuditDao.findRange(start,end);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException {
        return actionAuditDao.executeSQLQuery(query,params);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query) throws EcgDAOException {
        return actionAuditDao.executeSQLQuery(query);
    }

    @Override
    @Transactional
    public ActionAudit mergeActionAudit(ActionAudit actionAudit) throws EcgDAOException {
        return actionAuditDao.merge(actionAudit);
    }

    @Override
    @Transactional
    public Integer saveActionAudit(ActionAudit actionAudit) throws EcgDAOException {
        return actionAuditDao.save(actionAudit);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<ActionAudit> findActionsByUser(int userId) throws EcgDAOException {
        return actionAuditDao.findActionsByUser(userId);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<ActionAudit> findActionsByTime(Date startTime, Date endTime) throws EcgDAOException {
        return actionAuditDao.findActionsByTime(startTime,endTime);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<ActionAudit> findActionsByType(String type) throws EcgDAOException {
        return actionAuditDao.findActionsByType(type);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<ActionAudit> findActionsBySystemProcessName(String processName) throws EcgDAOException {
        return actionAuditDao.findActionsBySystemProcessName(processName);
    }

}
