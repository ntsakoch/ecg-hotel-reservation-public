package za.org.ecg.springmvc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import za.org.ecg.dao.ApplicationPermissionDao;
import za.org.ecg.dao.entity.ApplicationPermission;
import za.org.ecg.dao.exception.EcgDAOException;
import za.org.ecg.springmvc.service.ApplicationPermissionService;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2016/12/28.
 */
@Service("applicationPermissionService")
public class ApplicationPermissionserviceImpl implements ApplicationPermissionService {

    @Autowired
    private ApplicationPermissionDao applicationPermissionDao;

    @Override
    @Transactional
    public List<ApplicationPermission> findAllApplicationPermissions() throws EcgDAOException {
        return applicationPermissionDao.findAll() ;
    }

    @Override
    @Transactional
    public ApplicationPermission findApplicationPermissionById(Integer id) throws EcgDAOException {
        return applicationPermissionDao.findById(id);
    }

    @Override
    @Transactional
    public void createApplicationPermission(ApplicationPermission appPermission) throws EcgDAOException {
        applicationPermissionDao.create(appPermission);
    }

    @Override
    @Transactional
    public void updateApplicationPermission(ApplicationPermission newValue) throws EcgDAOException {
        applicationPermissionDao.update(newValue);
    }

    @Override
    @Transactional
    public void removeApplicationPermission(ApplicationPermission appPermission) throws EcgDAOException {
        applicationPermissionDao.remove(appPermission);
    }

    @Override
    @Transactional
    public long countApplicationPermissions() throws EcgDAOException {
        return applicationPermissionDao.count();
    }

    @Override
    @Transactional
    public List<ApplicationPermission> findRange(int start, int end) throws EcgDAOException {
        return applicationPermissionDao.findRange(start,end);
    }

    @Override
    @Transactional
    public List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException {
        return applicationPermissionDao.executeSQLQuery(query,params);
    }

    @Override
    @Transactional
    public List<Object[]> executeSQLQuery(String query) throws EcgDAOException {
        return applicationPermissionDao.executeSQLQuery(query);
    }

    @Override
    @Transactional
    public ApplicationPermission mergeApplicationPermission(ApplicationPermission appPermission) throws EcgDAOException {
        return applicationPermissionDao.merge(appPermission);
    }

    @Override
    @Transactional
    public Integer saveApplicationPermission(ApplicationPermission appPermission) throws EcgDAOException {
        return applicationPermissionDao.save(appPermission);
    }
}
