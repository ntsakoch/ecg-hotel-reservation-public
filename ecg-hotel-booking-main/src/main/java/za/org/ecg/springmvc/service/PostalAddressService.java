package za.org.ecg.springmvc.service;

import za.org.ecg.dao.entity.PostalAddress;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/16.
 */
public interface PostalAddressService {

    List<PostalAddress> findAllPostalAddresses() throws EcgDAOException;

    PostalAddress findPostalAddressById(Integer id) throws EcgDAOException;

    void createPostalAddress(PostalAddress postalAddress) throws EcgDAOException;

    void updatePostalAddress(PostalAddress newValue) throws EcgDAOException;

    void removePostalAddress(PostalAddress postalAddress) throws EcgDAOException;

    long countPostalAddresses() throws EcgDAOException;

    List<PostalAddress> findRange(int start, int end) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query) throws EcgDAOException;

    PostalAddress mergePostalAddress(PostalAddress postalAddress) throws EcgDAOException;

    Integer savePostalAddress(PostalAddress postalAddress) throws EcgDAOException;
}
