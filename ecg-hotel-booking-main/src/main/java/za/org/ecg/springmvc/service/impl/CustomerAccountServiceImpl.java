package za.org.ecg.springmvc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import za.org.ecg.dao.CustomerAccountDao;
import za.org.ecg.dao.entity.CustomerAccount;
import za.org.ecg.dao.exception.EcgDAOException;
import za.org.ecg.springmvc.service.CustomerAccountService;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/12.
 */
@Service("customerAccountService")
public class CustomerAccountServiceImpl implements CustomerAccountService {

    @Autowired
    private CustomerAccountDao customerAccountDao;

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<CustomerAccount> findAllCustomerAccounts() throws EcgDAOException {
        return customerAccountDao.findAll();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public CustomerAccount findCustomerAccountById(Integer id) throws EcgDAOException {
        return customerAccountDao.findById(id);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<CustomerAccount> findByMinimumOutstanding(double minimumBalance) throws EcgDAOException {
        return customerAccountDao.findByMinimumOutstanding(minimumBalance);
    }

    @Override
    @Transactional
    public void createCustomerAccount(CustomerAccount account) throws EcgDAOException {
        customerAccountDao.create(account);
    }

    @Override
    @Transactional
    public void updateCustomerAccount(CustomerAccount newValue) throws EcgDAOException {
        customerAccountDao.update(newValue);
    }

    @Override
    @Transactional
    public void removeCustomerAccount(CustomerAccount account) throws EcgDAOException {
        customerAccountDao.remove(account);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<CustomerAccount> findRange(int start, int end) throws EcgDAOException {
        return customerAccountDao.findRange(start,end);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException {
        return customerAccountDao.executeSQLQuery(query,params);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query) throws EcgDAOException {
        return customerAccountDao.executeSQLQuery(query);
    }

    @Override
    @Transactional
    public CustomerAccount mergeCustomerAccount(CustomerAccount account) throws EcgDAOException {
        return customerAccountDao.merge(account);
    }

    @Override
    @Transactional
    public Integer saveCustomerAccount(CustomerAccount account) throws EcgDAOException {
        return customerAccountDao.save(account);
    }
}
