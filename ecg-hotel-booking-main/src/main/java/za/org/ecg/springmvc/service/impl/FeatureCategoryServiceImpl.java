package za.org.ecg.springmvc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import za.org.ecg.dao.FeatureCategoryDao;
import za.org.ecg.dao.entity.FeatureCategory;
import za.org.ecg.dao.exception.EcgDAOException;
import za.org.ecg.springmvc.service.FeatureCategoryService;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/16.
 */
@Service("featureCategoryService")
public class FeatureCategoryServiceImpl implements FeatureCategoryService{

    @Autowired
    private FeatureCategoryDao featureCategoryDao;

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<FeatureCategory> findAllFeatureCategories() throws EcgDAOException {
        return featureCategoryDao.findAll();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public FeatureCategory findFeatureCategoryById(Integer id) throws EcgDAOException {
        return featureCategoryDao.findById(id);
    }

    @Override
    @Transactional
    public void createFeatureCategory(FeatureCategory featureCategory) throws EcgDAOException {
        featureCategoryDao.create(featureCategory);
    }

    @Override
    @Transactional
    public void updateFeatureCategory(FeatureCategory newValue) throws EcgDAOException {
        featureCategoryDao.update(newValue);
    }

    @Override
    @Transactional
    public void removeFeatureCategory(FeatureCategory featureCategory) throws EcgDAOException {
        featureCategoryDao.remove(featureCategory);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public long countFeatureCategories() throws EcgDAOException {
        return featureCategoryDao.count();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<FeatureCategory> findRange(int start, int end) throws EcgDAOException {
        return featureCategoryDao.findRange(start,end);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException {
        return featureCategoryDao.executeSQLQuery(query,params);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query) throws EcgDAOException {
        return featureCategoryDao.executeSQLQuery(query);
    }

    @Override
    @Transactional
    public FeatureCategory mergeFeatureCategory(FeatureCategory featureCategory) throws EcgDAOException {
        return featureCategoryDao.merge(featureCategory);
    }

    @Override
    @Transactional
    public Integer saveFeatureCategory(FeatureCategory featureCategory) throws EcgDAOException {
        return featureCategoryDao.save(featureCategory);
    }
}
