package za.org.ecg.springmvc.controller.web;

import org.apache.log4j.Logger;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import za.org.ecg.utils.http.HttpClientError;
import za.org.ecg.utils.http.HttpUtils;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Enumeration;
import java.util.Locale;

/**
 * Created by Ntsako on 2017/01/25.
 */
public class HttpRequestController extends HttpServlet{
    private static final Logger LOGGER=Logger.getLogger(HttpRequestController.class);


    public HttpServletRequest getRequest()
    {
        return  ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    }
    public String getParameter(String paramName)
    {
        return getRequest().getParameter(paramName);
    }
    public HttpServletResponse getResponse()
    {
        return  ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();
    }
    public String getSessionId()
    {
        return getRequest().getSession().getId();
    }

    public void setSessionValue(String key, Object value)
    {
        getRequest().getSession().setAttribute(key,value);
    }

    public void addRequestAttribute(String attrName,Object attrValue)
    {
        getRequest().setAttribute(attrName,attrValue);
    }
    public void setClientError(String errorMsg,String operationName)
    {
        errorMsg=errorMsg==null||errorMsg.isEmpty()?HttpUtils.getDefaultClientErrorRequestMsg(getSessionId()):errorMsg;
        setSessionValue("clientError",new HttpClientError(errorMsg,operationName));
    }
    public Object getSessionValue(String key)
    {
        return getRequest().getSession().getAttribute(key);
    }

    public Locale getRequestLocale()
    {
        return getRequest().getLocale();
    }

    public String getClientCountryCode(){
        HttpServletRequest request = getRequest();
        if(request.getHeaderNames() != null) {
            for (Enumeration e = request.getHeaderNames(); e.hasMoreElements() ;) {
                String sName = e.nextElement().toString();
                LOGGER.debug("sName: " + sName);
                if("geo_country".equalsIgnoreCase(sName)){
                    String sValue = request.getHeader(sName);
                    LOGGER.debug("geo_country sValue: " + sValue);
                    return sValue;
                }
            }
        }
        return "ZA";
    }
}
