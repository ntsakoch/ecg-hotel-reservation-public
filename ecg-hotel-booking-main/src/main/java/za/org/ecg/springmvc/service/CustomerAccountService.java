package za.org.ecg.springmvc.service;

import za.org.ecg.dao.entity.CustomerAccount;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/12.
 */
public interface CustomerAccountService {
    List<CustomerAccount> findAllCustomerAccounts() throws EcgDAOException;

    CustomerAccount findCustomerAccountById(Integer id) throws EcgDAOException;

    List<CustomerAccount> findByMinimumOutstanding(double minimumBalance) throws EcgDAOException;

    void createCustomerAccount(CustomerAccount account) throws EcgDAOException;

    void updateCustomerAccount(CustomerAccount newValue) throws EcgDAOException;

    void removeCustomerAccount(CustomerAccount account) throws EcgDAOException;

    List<CustomerAccount> findRange(int start, int end) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query) throws EcgDAOException;

    CustomerAccount mergeCustomerAccount(CustomerAccount account) throws EcgDAOException;

    Integer saveCustomerAccount(CustomerAccount account) throws EcgDAOException;
}
