package za.org.ecg.springmvc.service;

import za.org.ecg.dao.entity.Language;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/18.
 */
public interface LanguageService {

    List<Language> findAllLanguages() throws EcgDAOException;

    Language findLanguageById(Integer id) throws EcgDAOException;

    void createLanguage(Language language) throws EcgDAOException;

    void updateLanguage(Language newValue) throws EcgDAOException;

    void removeLanguage(Language language) throws EcgDAOException;

    long countLanguages() throws EcgDAOException;

    List<Language> findRange(int start, int end) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query) throws EcgDAOException;

    Language mergeLanguage(Language language) throws EcgDAOException;

    Integer saveLanguage(Language language) throws EcgDAOException;
}
