package za.org.ecg.springmvc.service;

import za.org.ecg.dao.entity.ContactUsMessage;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2016/12/28.
 */
public interface ContactUsMessageService {

    List<ContactUsMessage> findAllContactUsMessages() throws EcgDAOException;

    ContactUsMessage findContactUsMessageById(Integer id) throws EcgDAOException;

    void createContactUsMessage(ContactUsMessage message) throws EcgDAOException;

    void updateContactUsMessage(ContactUsMessage newValue) throws EcgDAOException;

    void removeContactUsMessage(ContactUsMessage message) throws EcgDAOException;

    long countContactUsMessages() throws EcgDAOException;

    List<ContactUsMessage> findRange(int start, int end) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query) throws EcgDAOException;

    ContactUsMessage mergeContactUsMessage(ContactUsMessage message) throws EcgDAOException;

    Integer saveContactUsMessage(ContactUsMessage message) throws EcgDAOException;

    List<ContactUsMessage> findContactUsMessageByUserId(int userId) throws EcgDAOException;

    List<ContactUsMessage> findContactUsMessageByStatus(String status) throws EcgDAOException;

    List<ContactUsMessage> findContactUsMessageBySenderFullName(String fullName) throws EcgDAOException;

    List<ContactUsMessage> findContactUsMessageByDateRange(Date fromDate, Date toDate) throws EcgDAOException;
}
