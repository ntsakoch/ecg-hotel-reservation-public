package za.org.ecg.springmvc.service;

import za.org.ecg.dao.entity.ApplicationPermission;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2016/12/28.
 */
public interface ApplicationPermissionService {
    List<ApplicationPermission> findAllApplicationPermissions() throws EcgDAOException;

    ApplicationPermission findApplicationPermissionById(Integer id) throws EcgDAOException;

    void createApplicationPermission(ApplicationPermission appPermission) throws EcgDAOException;

    void updateApplicationPermission(ApplicationPermission newValue) throws EcgDAOException;

    void removeApplicationPermission(ApplicationPermission appPermission) throws EcgDAOException;

    long countApplicationPermissions() throws EcgDAOException;

    List<ApplicationPermission> findRange(int start, int end) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query) throws EcgDAOException;

    ApplicationPermission mergeApplicationPermission(ApplicationPermission appPermission) throws EcgDAOException;

    Integer saveApplicationPermission(ApplicationPermission appPermission) throws EcgDAOException;
}
