package za.org.ecg.springmvc.service.audit;

/**
 * Created by Ntsako on 2017/01/21.
 */
public class AuditUserIdTracker {

    private  static ThreadLocal<Integer> auditUserIds=new ThreadLocal<>();

    public static int getAuditUserId()
    {
        try {
            return auditUserIds.get();
        }
        catch (NullPointerException exc)
        {
            return -1;
        }
    }

    public static void  setAuditUserId(int userId)
    {
        auditUserIds.set(userId);
    }
}
