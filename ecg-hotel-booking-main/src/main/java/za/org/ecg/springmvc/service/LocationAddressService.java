package za.org.ecg.springmvc.service;

import za.org.ecg.dao.entity.LocationAddress;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/02/21.
 */
public interface LocationAddressService {
    
    List<LocationAddress> findAllLocationAddresses() throws EcgDAOException;

    List<LocationAddress> findAllLocationAddressesBySuburbName() throws  EcgDAOException;

    LocationAddress findLocationAddressById(Integer id) throws EcgDAOException;

    void createLocationAddress(LocationAddress locationAddress) throws EcgDAOException;

    void updateLocationAddress(LocationAddress newValue) throws EcgDAOException;

    void removeLocationAddress(LocationAddress locationAddress) throws EcgDAOException;

    long countLocationAddress() throws EcgDAOException;

    List<LocationAddress> findRange(int start, int end) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query) throws EcgDAOException;

    LocationAddress mergeLocationAddress(LocationAddress locationAddress) throws EcgDAOException;

    Integer saveLocationAddress(LocationAddress locationAddress) throws EcgDAOException;
}
