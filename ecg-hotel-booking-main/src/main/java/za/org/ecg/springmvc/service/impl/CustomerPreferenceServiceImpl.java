package za.org.ecg.springmvc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import za.org.ecg.dao.CustomerPreferenceDao;
import za.org.ecg.dao.entity.CustomerPreference;
import za.org.ecg.dao.exception.EcgDAOException;
import za.org.ecg.springmvc.service.CustomerPreferenceService;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/16.
 */
@Service("customerPreferenceService")
public class CustomerPreferenceServiceImpl implements CustomerPreferenceService {

    @Autowired
    private CustomerPreferenceDao customerPreferenceDao;

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<CustomerPreference> findAllCustomerPreferences() throws EcgDAOException {
        return customerPreferenceDao.findAll();
    }

    @Override
    public List<CustomerPreference> findAllCustomerPreferences(Integer[] preferenceIds) throws EcgDAOException {
        return null;
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public CustomerPreference findCustomerPreferenceById(Integer id) throws EcgDAOException {
        return customerPreferenceDao.findById(id);
    }

    @Override
    @Transactional
    public void createCustomerPreference(CustomerPreference preference) throws EcgDAOException {
        customerPreferenceDao.create(preference);
    }

    @Override
    @Transactional
    public void updateCustomerPreference(CustomerPreference newValue) throws EcgDAOException {
        customerPreferenceDao.update(newValue);
    }

    @Override
    @Transactional
    public void removeCustomerPreference(CustomerPreference preference) throws EcgDAOException {
        customerPreferenceDao.remove(preference);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public long countCustomerPreferences() throws EcgDAOException {
        return customerPreferenceDao.count();
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<CustomerPreference> findRange(int start, int end) throws EcgDAOException {
        return customerPreferenceDao.findRange(start,end);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException {
        return customerPreferenceDao.executeSQLQuery(query);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query) throws EcgDAOException {
        return customerPreferenceDao.executeSQLQuery(query);
    }

    @Override
    @Transactional
    public CustomerPreference mergeCustomerPreference(CustomerPreference preference) throws EcgDAOException {
        return customerPreferenceDao.merge(preference);
    }

    @Override
    @Transactional
    public Integer saveCustomerPreference(CustomerPreference preference) throws EcgDAOException {
        return customerPreferenceDao.save(preference);
    }
}
