package za.org.ecg.springmvc.service;

import za.org.ecg.dao.entity.BookingCheckin;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/12.
 */
public interface BookingCheckinService {

    List<BookingCheckin> findAllBookingCheckins() throws EcgDAOException;

    BookingCheckin findBookingCheckinById(Integer id) throws EcgDAOException;

    void createBookingCheckin(BookingCheckin checkin) throws EcgDAOException;

    void updateBookingCheckin(BookingCheckin newValue) throws EcgDAOException;

    void removeBookingCheckin(BookingCheckin checkin) throws EcgDAOException;

    long countBookingCheckins() throws EcgDAOException;

    List<BookingCheckin> findRange(int start, int end) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query) throws EcgDAOException;

    BookingCheckin mergeBookingCheckin(BookingCheckin checkin) throws EcgDAOException;

    Integer saveBookingCheckin(BookingCheckin checkin) throws EcgDAOException;
}
