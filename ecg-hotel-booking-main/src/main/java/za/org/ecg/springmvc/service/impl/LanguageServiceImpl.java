package za.org.ecg.springmvc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import za.org.ecg.dao.LanguageDao;
import za.org.ecg.dao.entity.Language;
import za.org.ecg.dao.exception.EcgDAOException;
import za.org.ecg.springmvc.service.LanguageService;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/18.
 */
@Service("languageService")
public class LanguageServiceImpl implements LanguageService {

    @Autowired
    private LanguageDao languageDao;

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED,propagation = Propagation.NEVER)
    public List<Language> findAllLanguages() throws EcgDAOException {
        return languageDao.findAll();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED,propagation = Propagation.NEVER)
    public Language findLanguageById(Integer id) throws EcgDAOException {
        return languageDao.findById(id);
    }

    @Override
    @Transactional
    public void createLanguage(Language language) throws EcgDAOException {
        languageDao.create(language);
    }

    @Override
    @Transactional
    public void updateLanguage(Language newValue) throws EcgDAOException {
        languageDao.update(newValue);
    }

    @Override
    @Transactional
    public void removeLanguage(Language language) throws EcgDAOException {
        languageDao.remove(language);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public long countLanguages() throws EcgDAOException {
        return languageDao.count();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<Language> findRange(int start, int end) throws EcgDAOException {
        return languageDao.findRange(start,end);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException {
        return languageDao.executeSQLQuery(query,params);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query) throws EcgDAOException {
        return languageDao.executeSQLQuery(query);
    }

    @Override
    @Transactional
    public Language mergeLanguage(Language language) throws EcgDAOException {
        return languageDao.merge(language);
    }

    @Override
    @Transactional
    public Integer saveLanguage(Language language) throws EcgDAOException {
        return languageDao.save(language);
    }
}
