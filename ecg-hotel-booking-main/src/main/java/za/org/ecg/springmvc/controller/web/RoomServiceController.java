package za.org.ecg.springmvc.controller.web;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import za.org.ecg.utils.CommonUtils;
import za.org.ecg.bookings.BookingSearchInfo;
import za.org.ecg.bookings.BookingUtils;
import za.org.ecg.bookings.InitialBookingCost;
import za.org.ecg.dao.entity.Room;
import za.org.ecg.springmvc.service.RoomService;
import za.org.ecg.utils.DateUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Ntsako on 2017/02/15.
 */
@Controller
public class RoomServiceController extends HttpRequestController {

    private static final Logger LOGGER = Logger.getLogger(RoomServiceController.class);
    private static final String SEARCH_ROOMS_RESULTS_PAGE = "search-rooms-results.jsp";
    private static final String TOP_LISTED_PAGE = "top-listed.jsp";
    private static final String SEARCH_ROOMS_RESULTS = "index";
    private static final String SEARCH_ROOMS_RESULTS_ERROR = "rooms-search-results-error";

    private static final String BOOK_ROOM_RESULTS = "book-room";


    @Autowired
    private RoomService roomService;


    @RequestMapping(path = "/search-rooms", method = RequestMethod.POST)
    public ModelAndView searchAvailableRooms(Model model) {
        LOGGER.info("About to searchAvailableRooms");
        ModelAndView modelAndView = new ModelAndView(SEARCH_ROOMS_RESULTS);
        try {
            List results = doSearchAvailableRooms();
            setSessionValue("searchRoomsResults", results);
            modelAndView.setStatus(HttpStatus.OK);

            if (!results.isEmpty()) {
                setSessionValue("currentPage", SEARCH_ROOMS_RESULTS_PAGE);
            } else {
                setSessionValue("currentPage", TOP_LISTED_PAGE);
            }
        } catch (Exception exc) {
            modelAndView.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
            setClientError("", "searchAvailableRooms");
            LOGGER.error("An error occurred on searchAvailableRooms: ", exc);
        }

        LOGGER.info("Done searchAvailableRooms.....Returning: " + modelAndView);

        return modelAndView;
    }

    @RequestMapping(path = "/book-room-preview", method = RequestMethod.POST)
    public ModelAndView bookRoomPreview(Model model) {
        ModelAndView modelAndView = new ModelAndView(BOOK_ROOM_RESULTS );
        try
        {
            LOGGER.info("bookRoomPreview");
            int id = Integer.parseInt(getParameter("roomId"));
            Room toBook = roomService.findRoomById(id);
            setSessionValue("roomToBook",toBook);
            InitialBookingCost initialBookingCost= BookingUtils.calculateInitialBookingCosts((BookingSearchInfo)getSessionValue("bookingSearchInfo"),toBook);
            setSessionValue("initialBookingCost",initialBookingCost);
        } catch (Exception exc) {
            setSessionValue("currentPage", SEARCH_ROOMS_RESULTS_PAGE);
            modelAndView.setViewName(SEARCH_ROOMS_RESULTS);
            setClientError("", "bookRoomPreview");
            LOGGER.error("An error has occurred on bookRoomPreview: ", exc);
        }
        return modelAndView;
    }

    private List<Room> doSearchAvailableRooms() throws Exception {
        List<Room> availableRoomsSearchResults = new ArrayList<>();
        String checkinDateStr = getParameter("checkInDate");
        String checkoutDateStr = getParameter("checkOutDate");
        String numChildren = getParameter("selectNumberOfChildren");
        String numAdults = getParameter("selectNumberOfAdults");

        Date checkinDate = DateUtils.getInstance().parse(checkinDateStr, CommonUtils.DEFAULT_DATE_FORMAT);
        Date checkoutDate = DateUtils.getInstance().parse(checkoutDateStr, CommonUtils.DEFAULT_DATE_FORMAT);
        LOGGER.debug("Searching available rooms: checkinDate=" + checkinDate + ", checkoutDate=" + checkoutDate + ", numAdults=" + numAdults + ", numChildren=" + numChildren);
        availableRoomsSearchResults = roomService.getAllAvailableRooms(checkinDate, checkoutDate, Integer.parseInt(numAdults), Integer.parseInt(numChildren));
        LOGGER.debug("Done searching available rooms...search returned  " + availableRoomsSearchResults.size() + " available rooms based on the search parameters");

        BookingSearchInfo searchInfo=new BookingSearchInfo(Integer.parseInt(numAdults),Integer.parseInt(numChildren),checkinDateStr,checkoutDateStr);
        setSessionValue("bookingSearchInfo",searchInfo);
        return availableRoomsSearchResults;
    }
}
