package za.org.ecg.springmvc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import za.org.ecg.dao.PhysicalAddressDao;
import za.org.ecg.dao.entity.PhysicalAddress;
import za.org.ecg.dao.exception.EcgDAOException;
import za.org.ecg.springmvc.service.PhysicalAddressService;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/16.
 */
@Service("physicalAddressService")
public class PhysicalAddressServiceImpl implements PhysicalAddressService {

    @Autowired
    private PhysicalAddressDao physicalAddressDao;

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<PhysicalAddress> findAllPhysicalAddresses() throws EcgDAOException {
        return physicalAddressDao.findAll();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public PhysicalAddress findPhysicalAddressById(Integer id) throws EcgDAOException {
        return physicalAddressDao.findById(id);
    }

    @Override
    @Transactional
    public void createPhysicalAddress(PhysicalAddress physicalAddress) throws EcgDAOException {
        physicalAddressDao.create(physicalAddress);
    }

    @Override
    @Transactional
    public void updatePhysicalAddress(PhysicalAddress newValue) throws EcgDAOException {
        physicalAddressDao.update(newValue);
    }

    @Override
    @Transactional
    public void removePhysicalAddress(PhysicalAddress mailMessage) throws EcgDAOException {
        physicalAddressDao.remove(mailMessage);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public long countPhysicalAddresses() throws EcgDAOException {
        return physicalAddressDao.count();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<PhysicalAddress> findRange(int start, int end) throws EcgDAOException {
        return physicalAddressDao.findRange(start,end);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException {
        return physicalAddressDao.executeSQLQuery(query,params);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query) throws EcgDAOException {
        return physicalAddressDao.executeSQLQuery(query);
    }

    @Override
    @Transactional
    public PhysicalAddress mergePhysicalAddress(PhysicalAddress physicalAddress) throws EcgDAOException {
        return physicalAddressDao.merge(physicalAddress);
    }

    @Override
    @Transactional
    public Integer savePhysicalAddress(PhysicalAddress physicalAddress) throws EcgDAOException {
        return physicalAddressDao.save(physicalAddress);
    }
}
