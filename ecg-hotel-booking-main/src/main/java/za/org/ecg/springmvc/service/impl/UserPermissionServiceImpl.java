package za.org.ecg.springmvc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import za.org.ecg.dao.UserPermissionDao;
import za.org.ecg.dao.entity.UserPermission;
import za.org.ecg.dao.exception.EcgDAOException;
import za.org.ecg.springmvc.service.UserPermissionService;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/16.
 */
@Service("userPermissionService")
public class UserPermissionServiceImpl implements UserPermissionService {

    @Autowired
    private UserPermissionDao userPermissionDao;

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<UserPermission> findAllRoomUsersPermission() throws EcgDAOException {
        return userPermissionDao.findAll();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public UserPermission findUserPermissionById(Integer id) throws EcgDAOException {
        return userPermissionDao.findById(id);
    }

    @Override
    @Transactional
    public void createUserPermission(UserPermission userPermission) throws EcgDAOException {
        userPermissionDao.create(userPermission);
    }

    @Override
    @Transactional
    public void updateUserPermission(UserPermission newValue) throws EcgDAOException {
        userPermissionDao.update(newValue);
    }

    @Override
    @Transactional
    public void removeUserPermission(UserPermission userPermission) throws EcgDAOException {
        userPermissionDao.remove(userPermission);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public long countRoomUsersPermission() throws EcgDAOException {
        return userPermissionDao.count();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<UserPermission> findRange(int start, int end) throws EcgDAOException {
        return userPermissionDao.findRange(start,end);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException {
        return userPermissionDao.executeSQLQuery(query,params);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query) throws EcgDAOException {
        return userPermissionDao.executeSQLQuery(query);
    }

    @Override
    @Transactional
    public UserPermission mergeUserPermission(UserPermission userPermission) throws EcgDAOException {
        return userPermissionDao.merge(userPermission);
    }

    @Override
    @Transactional
    public Integer saveUserPermission(UserPermission userPermission) throws EcgDAOException {
        return userPermissionDao.save(userPermission);
    }
}
