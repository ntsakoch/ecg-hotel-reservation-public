package za.org.ecg.springmvc.service;

import za.org.ecg.dao.entity.Cost;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/18.
 */
public interface CostService {

    List<Cost> findAllCosts() throws EcgDAOException;

    Cost findCostById(Integer id) throws EcgDAOException;

    void createCost(Cost cost) throws EcgDAOException;

    void updateCost(Cost newValue) throws EcgDAOException;

    void removeCost(Cost cost) throws EcgDAOException;

    long countCosts() throws EcgDAOException;

    List<Cost> findRange(int start, int end) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query) throws EcgDAOException;

    Cost mergeCost(Cost cost) throws EcgDAOException;

    Integer saveCost(Cost cost) throws EcgDAOException;
}
