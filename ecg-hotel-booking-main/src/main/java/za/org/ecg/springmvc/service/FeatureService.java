package za.org.ecg.springmvc.service;

import za.org.ecg.dao.entity.Feature;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/16.
 */
public interface FeatureService {

    List<Feature> findAllFeatures() throws EcgDAOException;

    Feature findFeatureById(Integer id) throws EcgDAOException;

    void createFeature(Feature feature) throws EcgDAOException;

    void updateFeature(Feature newValue) throws EcgDAOException;

    void removeFeature(Feature feature) throws EcgDAOException;

    long countFeatures() throws EcgDAOException;

    List<Feature> findRange(int start, int end) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query) throws EcgDAOException;

    Feature mergeFeature(Feature feature) throws EcgDAOException;

    Integer saveFeature(Feature feature) throws EcgDAOException;
}
