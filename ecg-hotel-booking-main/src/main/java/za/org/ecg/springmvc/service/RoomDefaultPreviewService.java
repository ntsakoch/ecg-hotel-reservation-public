package za.org.ecg.springmvc.service;

import za.org.ecg.dao.entity.RoomDefaultPreview;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/16.
 */
public interface RoomDefaultPreviewService {

    List<RoomDefaultPreview> findAllRoomFacilitiesDefaultPreview() throws EcgDAOException;

    RoomDefaultPreview findRoomDefaultPreviewById(Integer id) throws EcgDAOException;

    void createRoomDefaultPreview(RoomDefaultPreview defaultPreview) throws EcgDAOException;

    void updateRoomDefaultPreview(RoomDefaultPreview newValue) throws EcgDAOException;

    void removeRoomDefaultPreview(RoomDefaultPreview defaultPreview) throws EcgDAOException;

    long countRoomFacilitiesDefaultPreview() throws EcgDAOException;

    List<RoomDefaultPreview> findRange(int start, int end) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query) throws EcgDAOException;

    RoomDefaultPreview mergeRoomDefaultPreview(RoomDefaultPreview defaultPreview) throws EcgDAOException;

    Integer saveRoomDefaultPreview(RoomDefaultPreview defaultPreview) throws EcgDAOException;
}
