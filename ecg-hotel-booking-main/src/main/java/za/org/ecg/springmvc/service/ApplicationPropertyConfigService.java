package za.org.ecg.springmvc.service;

import za.org.ecg.dao.entity.ApplicationPropertyConfig;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2016/12/28.
 */
public interface ApplicationPropertyConfigService {
    List<ApplicationPropertyConfig> findAllApplicationPropertyConfigs() throws EcgDAOException;

    ApplicationPropertyConfig findApplicationPropertyConfigById(Integer id) throws EcgDAOException;

    void createApplicationPropertyConfig(ApplicationPropertyConfig appPermission) throws EcgDAOException;

    void updateApplicationPropertyConfig(ApplicationPropertyConfig newValue) throws EcgDAOException;

    void removeApplicationPropertyConfig(ApplicationPropertyConfig appPermission) throws EcgDAOException;

    long countApplicationPropertyConfigs() throws EcgDAOException;

    List<ApplicationPropertyConfig> findRange(int start, int end) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query) throws EcgDAOException;

    ApplicationPropertyConfig mergeApplicationPropertyConfig(ApplicationPropertyConfig appPermission) throws EcgDAOException;

    Integer saveApplicationPropertyConfig(ApplicationPropertyConfig appPermission) throws EcgDAOException;
}
