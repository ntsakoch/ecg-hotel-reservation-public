package za.org.ecg.springmvc.service;

import za.org.ecg.dao.entity.MailMessage;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/16.
 */

public interface MailMessageService {
    
    List<MailMessage> findAllMailMessages() throws EcgDAOException;

    MailMessage findMailMessageById(Integer id) throws EcgDAOException;

    void createMailMessage(MailMessage mailMessage) throws EcgDAOException;

    void updateMailMessage(MailMessage newValue) throws EcgDAOException;

    void removeMailMessage(MailMessage mailMessage) throws EcgDAOException;

    long countMailMessages() throws EcgDAOException;

    List<MailMessage> findRange(int start, int end) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query) throws EcgDAOException;

    MailMessage mergeMailMessage(MailMessage mailMessage) throws EcgDAOException;

    Integer saveMailMessage(MailMessage mailMessage) throws EcgDAOException;
}
