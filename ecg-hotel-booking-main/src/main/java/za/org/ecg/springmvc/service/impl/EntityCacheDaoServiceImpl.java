package za.org.ecg.springmvc.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import za.org.ecg.dao.entity.EntityBean;
import za.org.ecg.dao.exception.EcgDAOException;
import za.org.ecg.dao.impl.AbstractDao;
import za.org.ecg.springmvc.service.EntityCacheDaoService;

import java.util.List;

/**
 * Created by Ntsako on 2017/01/24.
 */
@Service("entityCacheDaoService")
public class EntityCacheDaoServiceImpl implements EntityCacheDaoService {

    @Override
    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    public List<? extends EntityBean> findAll(AbstractDao abstractDao) throws EcgDAOException {
        return abstractDao.findAll();
    }
}
