package za.org.ecg.springmvc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import za.org.ecg.dao.CustomerBookingDao;
import za.org.ecg.dao.entity.CustomerBooking;
import za.org.ecg.dao.exception.EcgDAOException;
import za.org.ecg.springmvc.service.CustomerBookingService;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/12.
 */
@Service("customerBookingService")
public class CustomerBookingServiceImpl  implements CustomerBookingService{

    @Autowired
    private CustomerBookingDao customerBookingDao;

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<CustomerBooking> findAllCustomerBookings() throws EcgDAOException {
        return customerBookingDao.findAll();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public CustomerBooking findCustomerBookingById(Integer id) throws EcgDAOException {
        return  customerBookingDao.findById(id);
    }

    @Override
    @Transactional
    public void createCustomerBooking(CustomerBooking booking) throws EcgDAOException {
        customerBookingDao.create(booking);
    }

    @Override
    @Transactional
    public void updateCustomerBooking(CustomerBooking newValue) throws EcgDAOException {
        customerBookingDao.update(newValue);
    }

    @Override
    @Transactional
    public void removeCustomerBooking(CustomerBooking booking) throws EcgDAOException {
        customerBookingDao.remove(booking);
    }

    @Override
    @Transactional
    public long countCustomerBookings() throws EcgDAOException {
        return customerBookingDao.count();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<CustomerBooking> findRange(int start, int end) throws EcgDAOException {
        return customerBookingDao.findRange(start,end);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException {
        return customerBookingDao.executeSQLQuery(query,params);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Object[]> executeSQLQuery(String query) throws EcgDAOException {
        return customerBookingDao.executeSQLQuery(query);
    }

    @Override
    @Transactional
    public CustomerBooking mergeCustomerBooking(CustomerBooking booking) throws EcgDAOException {
        return customerBookingDao.merge(booking);
    }

    @Override
    @Transactional
    public Integer saveCustomerBooking(CustomerBooking booking) throws EcgDAOException {
        return customerBookingDao.save(booking);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<CustomerBooking> findByStatus(String status) throws EcgDAOException {
        return customerBookingDao.findByStatus(status);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<CustomerBooking> findByCheckinDate(Date date) throws EcgDAOException {
        return customerBookingDao.findByCheckinDate(date);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<CustomerBooking> findByCheckoutDate(Date date) throws EcgDAOException {
        return customerBookingDao.findByCheckoutDate(date);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<CustomerBooking> findByCheckoutDateRange(Date from, Date to) throws EcgDAOException {
        return customerBookingDao.findByCheckoutDateRange(from,to);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<CustomerBooking> findByCheckinDateRange(Date from, Date to) throws EcgDAOException {
        return customerBookingDao.findByCheckinDateRange(from,to);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<CustomerBooking> findByDateCreationRange(Date from, Date to) throws EcgDAOException {
        return customerBookingDao.findByDateCreationRange(from,to);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<CustomerBooking> findByCustomerIdNumber(String customerIdNumber) throws EcgDAOException {
        return customerBookingDao.findByCustomerIdNumber(customerIdNumber);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<CustomerBooking> findAllOutstandingBookings(double minimumAmount) throws EcgDAOException {
        return customerBookingDao.findAllOutstandingBookings(minimumAmount);
    }
}
