package za.org.ecg.springmvc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import za.org.ecg.dao.UserCredentialDao;
import za.org.ecg.dao.entity.UserCredential;
import za.org.ecg.dao.exception.EcgDAOException;
import za.org.ecg.springmvc.service.UserCredentialService;

import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/16.
 */
@Service("userCredentialService")
public class UserCredentialServiceImpl implements UserCredentialService {

    @Autowired
    private UserCredentialDao userCredentialDao;

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<UserCredential> findAllRoomUsersCredential() throws EcgDAOException {
        return userCredentialDao.findAll();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public UserCredential findUserCredentialById(Integer id) throws EcgDAOException {
        return userCredentialDao.findById(id);
    }

    @Override
    @Transactional
    public void createUserCredential(UserCredential userCredential) throws EcgDAOException {
        userCredentialDao.create(userCredential);
    }

    @Override
    @Transactional
    public void updateUserCredential(UserCredential newValue) throws EcgDAOException {
        userCredentialDao.update(newValue);
    }

    @Override
    @Transactional
    public void removeUserCredential(UserCredential userCredential) throws EcgDAOException {
        userCredentialDao.remove(userCredential);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public long countRoomUsersCredential() throws EcgDAOException {
        return userCredentialDao.count();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<UserCredential> findRange(int start, int end) throws EcgDAOException {
        return userCredentialDao.findRange(start,end);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException {
        return userCredentialDao.executeSQLQuery(query,params);
    }

    @Override
    @Transactional(readOnly = true,isolation = Isolation.READ_COMMITTED)
    public List<Object[]> executeSQLQuery(String query) throws EcgDAOException {
        return userCredentialDao.executeSQLQuery(query);
    }

    @Override
    @Transactional
    public UserCredential mergeUserCredential(UserCredential userCredential) throws EcgDAOException {
        return userCredentialDao.merge(userCredential);
    }

    @Override
    @Transactional
    public Integer saveUserCredential(UserCredential userCredential) throws EcgDAOException {
        return userCredentialDao.save(userCredential);
    }
}
