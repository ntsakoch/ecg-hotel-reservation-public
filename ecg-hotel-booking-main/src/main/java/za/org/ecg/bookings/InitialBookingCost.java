package za.org.ecg.bookings;

/**
 * Created by Ntsako on 2017/02/24.
 */
public class InitialBookingCost {

    private String stayDuration;
    private double roomCost;
    private double selectedFeaturesCosts;
    private String adultData;
    private String childrenData;
    private double adultCost;
    private double childrenCost;
    private double vatAmount;
    private double totalCost;
    private String currencyCode;
    private int vatRate;

    public String getStayDuration() {
        return stayDuration;
    }

    public void setStayDuration(String stayDuration) {
        this.stayDuration = stayDuration;
    }

    public double getRoomCost() {
        return roomCost;
    }

    public void setRoomCost(double roomCost) {
        this.roomCost = roomCost;
    }

    public double getSelectedFeaturesCosts() {
        return selectedFeaturesCosts;
    }

    public void setSelectedFeaturesCosts(double selectedFeaturesCosts) {
        this.selectedFeaturesCosts = selectedFeaturesCosts;
    }

    public String getAdultData() {
        return adultData;
    }

    public void setAdultData(String adultData) {
        this.adultData = adultData;
    }

    public String getChildrenData() {
        return childrenData;
    }

    public void setChildrenData(String childrenData) {
        this.childrenData = childrenData;
    }

    public double getAdultCost() {
        return adultCost;
    }

    public void setAdultCost(double adultCost) {
        this.adultCost = adultCost;
    }

    public double getChildrenCost() {
        return childrenCost;
    }

    public void setChildrenCost(double childrenCost) {
        this.childrenCost = childrenCost;
    }

    public double getVatAmount() {
        return vatAmount;
    }

    public void setVatAmount(double vatAmount) {
        this.vatAmount = vatAmount;
    }

    public double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(double totalCost) {
        this.totalCost = totalCost;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public int getVatRate() {
        return vatRate;
    }

    public void setVatRate(int vatRate) {
        this.vatRate = vatRate;
    }

    @Override
    public String toString() {
        return "InitialBookingCost{" +
                "stayDuration='" + stayDuration + '\'' +
                ", roomCost=" + roomCost +
                ", selectedFeaturesCosts=" + selectedFeaturesCosts +
                ", adultData='" + adultData + '\'' +
                ", childrenData='" + childrenData + '\'' +
                ", adultCost=" + adultCost +
                ", childrenCost=" + childrenCost +
                ", vatAmount=" + vatAmount +
                ", totalCost=" + totalCost +
                ", currencyCode='" + currencyCode + '\'' +
                ", vatRate=" + vatRate +
                '}';
    }
}
