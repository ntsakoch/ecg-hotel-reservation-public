package za.org.ecg.bookings;

/**
 * Created by root on 2016/10/08.
 */
public enum BookingStatus {
    UNCONFIRMED("Unconfirmed"),CANCELED("Cancelled"),CONFIRMED("Confirmed"),CHECKED_OUT("checked_out");

    private String description;

    BookingStatus(String description)
    {
        this.description=description;
    }

    @Override
    public String toString() {
        return description;
    }
}
