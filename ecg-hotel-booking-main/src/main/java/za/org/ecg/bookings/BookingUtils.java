package za.org.ecg.bookings;

import org.apache.log4j.Logger;
import za.org.ecg.dao.entity.Room;
import za.org.ecg.utils.CommonUtils;
import za.org.ecg.utils.DateUtils;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Date;

import static za.org.ecg.utils.CommonUtils.DECIMAL_FORMAT_CONFIG;

/**
 * Created by root on 2016/10/29.
 */
public class BookingUtils {

    private static final Logger LOGGER = Logger.getLogger(BookingUtils.class);

    private static DecimalFormat decimalFormat = new DecimalFormat(System.getProperty(DECIMAL_FORMAT_CONFIG));
    private static String adultData = "Adults X";
    private static String childrenData = "Children X";
    private static  final long DAYS_IN_MILLIS=1000*60*60*24;

    public static InitialBookingCost calculateInitialBookingCosts(BookingSearchInfo bookingInfo, Room roomToBook) throws Exception {
        LOGGER.info("About to calculate initial costs for booking\n" + bookingInfo + "\n" + roomToBook);
        try {
            int stayDuration=calculateNumberOfDays(bookingInfo.getCheckinDAte(),bookingInfo.getCheckoutDAte());
            InitialBookingCost initialBookingCost = new InitialBookingCost();

            initialBookingCost.setRoomCost(Double.parseDouble(decimalFormat.format(roomToBook.getRoomCost().getAmount())));
            initialBookingCost.setSelectedFeaturesCosts(Double.parseDouble(decimalFormat.format(0)));
            initialBookingCost.setStayDuration(stayDuration+" Days");
            initialBookingCost.setAdultData(adultData+" "+bookingInfo.getNumAdults());
            initialBookingCost.setChildrenData(childrenData+" "+bookingInfo.getNumChildren());
            initialBookingCost.setVatRate(Integer.parseInt(System.getProperty(CommonUtils.VAT_RATE_CONFIG)));
            initialBookingCost.setAdultCost(calculateAdultCost(roomToBook,bookingInfo.getNumAdults(),stayDuration));
            initialBookingCost.setChildrenCost(calculateChildrenCost(roomToBook,bookingInfo.getNumChildren(),stayDuration));
            double totalCost=calculateTotalInitialBookingCosts(initialBookingCost);
            initialBookingCost.setTotalCost(totalCost);
            initialBookingCost.setVatAmount(calculateVatAmount(totalCost));
            initialBookingCost.setCurrencyCode(roomToBook.getRoomCost().getCurrency().getCurrencyCode());
            LOGGER.info("Done calculating initial booking costs:\n"+initialBookingCost);
            return initialBookingCost;
        } catch (Exception exc) {
            LOGGER.error("An error occurred on calculateInitialBookingCost: ", exc);
            throw new Exception(exc);
        }
    }

    public static int calculateNumberOfDays(String startDateStr,String endDateStr) throws ParseException {
        Date startDate= DateUtils.getInstance().parse(startDateStr,CommonUtils.DEFAULT_DATE_FORMAT);
        Date endDate=DateUtils.getInstance().parse(endDateStr,CommonUtils.DEFAULT_DATE_FORMAT);
        return (int)((endDate.getTime()-startDate.getTime())/DAYS_IN_MILLIS);
    }

    private static double calculateAdultCost(Room room, int numAdults, int numDays) throws Exception
    {
        double individualCosts=room.getAdultCost().getAmount()*numDays;
        double total=individualCosts*numAdults;
         return Double.parseDouble(decimalFormat.format(total));
    }

    private static double calculateChildrenCost(Room room, int numChildren, int numDays) throws Exception
    {
        double individualCosts=room.getChildrenCost().getAmount()*numDays;
        double total=individualCosts*numChildren;
        return Double.parseDouble(decimalFormat.format(total));
    }
    private static double calculateVatAmount(double totalCost) throws Exception
    {
        double vatRate=Double.parseDouble(System.getProperty(CommonUtils.VAT_RATE_CONFIG))/100;
        return  Double.parseDouble(decimalFormat.format(totalCost*vatRate));
    }

    private static double calculateTotalInitialBookingCosts(InitialBookingCost bookingCost)
    {
        return Double.parseDouble(decimalFormat.format(bookingCost.getRoomCost()+bookingCost.getChildrenCost()+bookingCost.getAdultCost()));
    }
}

