package za.org.ecg.bookings;

/**
 * Created by Ntsako on 2017/02/24.
 */
public class BookingSearchInfo {
    private int numAdults;
    private int numChildren;
    private String checkinDAte;
    private String checkoutDAte;

    public BookingSearchInfo() {
    }

    public BookingSearchInfo(int numAdults, int numChildren, String checkinDAte, String checkoutDAte) {
        this.numAdults = numAdults;
        this.numChildren = numChildren;
        this.checkinDAte = checkinDAte;
        this.checkoutDAte = checkoutDAte;
    }

    public int getNumAdults() {
        return numAdults;
    }

    public void setNumAdults(int numAdults) {
        this.numAdults = numAdults;
    }

    public int getNumChildren() {
        return numChildren;
    }

    public void setNumChildren(int numChildren) {
        this.numChildren = numChildren;
    }

    public String getCheckinDAte() {
        return checkinDAte;
    }

    public void setCheckinDAte(String checkinDAte) {
        this.checkinDAte = checkinDAte;
    }

    public String getCheckoutDAte() {
        return checkoutDAte;
    }

    public void setCheckoutDAte(String checkoutDAte) {
        this.checkoutDAte = checkoutDAte;
    }

    @Override
    public String toString() {
        return "BookingSearchInfo{" +
                "numAdults=" + numAdults +
                ", numChildren=" + numChildren +
                ", checkinDAte='" + checkinDAte + '\'' +
                ", checkoutDAte='" + checkoutDAte + '\'' +
                '}';
    }
}
