package za.org.ecg.exception;

/**
 * Created by Ntsako on 2016/11/08.
 */
public class ECGApplicationException extends Exception {
    private String errorCode;
    private String errorDescription;

    public ECGApplicationException(String message)
    {
        super((message));
    }
    public  ECGApplicationException(String message,Throwable cause)
    {
        super(message,cause);
    }
    public  ECGApplicationException(String message,Throwable cause,AppExceptionCode exceptionCode)
    {
        super(message,cause);
        this.errorCode=exceptionCode.getCode();
        this.errorDescription=exceptionCode.getDescription();
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorDesscription() {
        return errorDescription;
    }

}
