package za.org.ecg.exception;

/**
 * Created by Ntsako on 2016/11/08.
 */
public enum AppExceptionCode {
    DBEX("dbexc","SQL  syntax database exception");

    private String code;
    private String description;

    AppExceptionCode(String code,String description)
    {
        this.code=code;
        this.description=description;
    }
    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

}
