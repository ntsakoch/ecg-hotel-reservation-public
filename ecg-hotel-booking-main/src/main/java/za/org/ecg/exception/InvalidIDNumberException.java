package za.org.ecg.exception;

/**
 * Created by root on 2016/10/29.
 */
public class InvalidIDNumberException extends Exception{
    public InvalidIDNumberException(String message)
    {
        super((message));
    }
    public InvalidIDNumberException(String message,Throwable cause)
    {
        super(message,cause);
    }
}
