package za.org.ecg.dao.cache;


import za.org.ecg.dao.entity.EntityBean;

import java.util.List;

/**
 * Created by Ntsako on 2016/11/11.
 */
public interface EntityCacheManager<T> {
    void init() throws EntityCacheException;
    default void updateCache(T entityBean,boolean removeFromCache) throws EntityCacheException{}
    default void refreshCache() throws EntityCacheException{}
    void refreshCachedEntitiesForType(Class<? extends EntityBean> entityClass) throws  EntityCacheException;
    List<T>  findAll(Class<? extends EntityBean> entityClass) throws  EntityCacheException;
    T findById(Class<? extends EntityBean> entityClass, Integer id) throws EntityCacheException;
    boolean isCacheInitialized();
}
