package za.org.ecg.dao.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import za.org.ecg.dao.cache.EnableEntityCaching;
import za.org.ecg.dao.impl.CurrencyDaoImpl;

import javax.persistence.*;

import static za.org.ecg.dao.entity.EntityBean.CATALOG;

/**
 * Created by Ntsako on 2017/01/18.
 */
@Entity
@Table(name = "currencies", catalog = CATALOG)
@EnableEntityCaching(daoImplementer = CurrencyDaoImpl.class)
public class Currency implements EntityBean{

    private int currencyId;
    private int countryId;
    private String currencyCode;
    private String countryName;
    private String currencyName;
    private String currencySymbol;
    private String thousandSeparator;
    private String decimalSeparator;
    private Country country;

    public Currency(){}

    public Currency(int currencyId) {
        this.currencyId = currencyId;
    }

    public Currency(int currencyId, int countryId, String currencyCode) {
        this.currencyId = currencyId;
        this.countryId = countryId;
        this.currencyCode = currencyCode;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "currency_id")
    public int getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(int currencyId) {
        this.currencyId = currencyId;
    }

    @Column(name = "country_id",updatable = false,insertable = false)
    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    @Column(name = "currency_code")
    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    @Column(name = "country_name")
    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    @Column(name = "currency_name")
    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    @Column(name = "symbol")
    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }

    @Column(name = "thousand_separator")
    public String getThousandSeparator() {
        return thousandSeparator;
    }

    public void setThousandSeparator(String thousandSeparator) {
        this.thousandSeparator = thousandSeparator;
    }

    @Column(name = "decimal_separator")
    public String getDecimalSeparator() {
        return decimalSeparator;
    }

    public void setDecimalSeparator(String decimalSeparator) {
        this.decimalSeparator = decimalSeparator;
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "country_id")
    @NotFound(action = NotFoundAction.IGNORE)
    @JsonIgnore
    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return "Currency{" +
                "currencyId=" + currencyId +
                ", countryId=" + countryId +
                ", currencyCode='" + currencyCode + '\'' +
                ", countryName='" + countryName + '\'' +
                ", currencyName='" + currencyName + '\'' +
                ", currencySymbol='" + currencySymbol + '\'' +
                ", thousandSeparator='" + thousandSeparator + '\'' +
                ", decimalSeparator='" + decimalSeparator +
                '}';
    }

    @Override
    @Transient
    public Integer getId() {
        return countryId;
    }
}
