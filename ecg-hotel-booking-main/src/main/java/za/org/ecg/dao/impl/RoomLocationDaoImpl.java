package za.org.ecg.dao.impl;

import org.springframework.stereotype.Repository;
import za.org.ecg.dao.RoomLocationDao;
import za.org.ecg.dao.entity.RoomLocation;

/**
 * Created by Ntsako on 2017/02/21.
 */
@Repository("roomLocationDao")
public class RoomLocationDaoImpl  extends  AbstractDao<RoomLocation> implements RoomLocationDao{
}
