package za.org.ecg.dao.impl;

import org.springframework.stereotype.Repository;
import za.org.ecg.dao.AttachmentDao;
import za.org.ecg.dao.entity.Attachment;

/**
 * Created by F4817273 on 2016/09/22.
 */
@Repository("attachmentDao")
public class AttachmentDaoImpl extends AbstractDao<Attachment> implements AttachmentDao {
    public AttachmentDaoImpl() {
        super(Attachment.class);
    }
}
