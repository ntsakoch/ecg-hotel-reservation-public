package za.org.ecg.dao;

import za.org.ecg.dao.entity.Cost;

/**
 * Created by Ntsako on 2017/01/18.
 */
public interface CostDao extends GenericDao<Cost> {
}
