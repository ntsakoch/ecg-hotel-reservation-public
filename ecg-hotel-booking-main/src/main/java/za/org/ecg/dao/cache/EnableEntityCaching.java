package za.org.ecg.dao.cache;



import za.org.ecg.dao.impl.AbstractDao;

import java.lang.annotation.*;

/**
 * Created by Ntsako on 2017/01/23.
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface EnableEntityCaching {
    Class<? extends AbstractDao> daoImplementer();
}
