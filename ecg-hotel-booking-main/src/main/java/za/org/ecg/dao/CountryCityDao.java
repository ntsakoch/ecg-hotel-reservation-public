package za.org.ecg.dao;

import za.org.ecg.dao.GenericDao;
import za.org.ecg.dao.entity.CountryCity;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.List;

/**
 * Created by Ntsako on 2017/02/21.
 */
public interface CountryCityDao  extends GenericDao<CountryCity> {

    List<CountryCity> findByCityName(String cityName) throws EcgDAOException;
}
