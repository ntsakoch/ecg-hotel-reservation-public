package za.org.ecg.dao.impl;

import org.springframework.stereotype.Repository;
import za.org.ecg.dao.CurrencyDao;
import za.org.ecg.dao.entity.Currency;

/**
 * Created by Ntsako on 2017/01/18.
 */
@Repository("currencyDao")
public class CurrencyDaoImpl extends AbstractDao<Currency> implements CurrencyDao{
}
