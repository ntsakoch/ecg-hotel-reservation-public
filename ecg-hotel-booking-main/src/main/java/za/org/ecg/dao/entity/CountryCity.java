package za.org.ecg.dao.entity;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import za.org.ecg.dao.cache.EnableEntityCaching;
import za.org.ecg.dao.impl.CountryCityDaoImpl;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import java.util.Set;

import static za.org.ecg.dao.entity.EntityBean.CATALOG;

/**
 * Created by Ntsako on 2017/02/21.
 */
@Entity
@Table(name="country_cities"
        ,catalog=CATALOG
)
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "CountryCity.findByCityName", query = "SELECT cc FROM CountryCity cc WHERE cc.cityName=:cityName")})
@EnableEntityCaching(daoImplementer = CountryCityDaoImpl.class)
public class CountryCity implements EntityBean{
    private int countryCityId;
    private int countryId;
    private String cityName;
    private String cityCode;
    private Country country;
    private Set<LocationAddress> cityAddresses;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "country_city_id")
    public int getCountryCityId() {
        return countryCityId;
    }

    public void setCountryCityId(int countryCityId) {
        this.countryCityId = countryCityId;
    }

    @Column(name = "country_id",insertable = false,updatable = false)
    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    @Column(name = "city_name")
    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    @Column(name = "city_code")
    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "country_id")
    @NotFound(action = NotFoundAction.IGNORE)
    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "country_city_id")
    @Fetch(value = FetchMode.SUBSELECT)
    public Set<LocationAddress> getCityAddresses() {
        return cityAddresses;
    }

    public void setCityAddresses(Set<LocationAddress> cityAddresses) {
        this.cityAddresses = cityAddresses;
    }
    @Override
    @Transient
    public Integer getId() {
        return countryCityId;
    }

    @Override
    public String toString() {
        return "CountryCity{" +
                "countryCityId=" + countryCityId +
                ", countryId=" + countryId +
                ", cityName='" + cityName + '\'' +
                ", cityCode='" + cityCode + '\'' +
                '}';
    }
}
