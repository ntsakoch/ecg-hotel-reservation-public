package za.org.ecg.dao;

import za.org.ecg.dao.entity.ActionAudit;
import za.org.ecg.dao.entity.User;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.List;

/**
 * Created by f4817273 on 2016/09/05.
 */
public interface UserDao extends GenericDao<User> {
    User findByIdNumber(String id) throws EcgDAOException;
    User authenticateUser(String username, String password) throws EcgDAOException;
    User findByUsername(String username) throws EcgDAOException;
    List<ActionAudit> findAllUsersActions(int userId) throws EcgDAOException;
}
