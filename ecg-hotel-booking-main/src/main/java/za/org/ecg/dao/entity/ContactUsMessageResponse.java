package za.org.ecg.dao.entity;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;
import static za.org.ecg.dao.entity.EntityBean.CATALOG;

/**
 *
 * @author F4817273
 */

@Entity
@Table(name="contact_us_message_responses"
    ,catalog=CATALOG
)
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "ContactUsMessageResponse.findAll", query = "SELECT c FROM ContactUsMessageResponse c"),
        @NamedQuery(name = "ContactUsMessageResponse.findByContactUsMessageResponseId", query = "SELECT c FROM ContactUsMessageResponse c WHERE c.contactUsMessageResponseId = :contactUsMessageResponseId"),
        @NamedQuery(name = "ContactUsMessageResponse.findByContactUsMessageId", query = "SELECT c FROM ContactUsMessageResponse c WHERE c.contactUsMessageId = :contactUsMessageId"),
        @NamedQuery(name = "ContactUsMessageResponse.findByAdminUserId", query = "SELECT c FROM ContactUsMessageResponse c WHERE c.adminUserId = :adminUserId"),
        @NamedQuery(name = "ContactUsMessageResponse.findByResolution", query = "SELECT c FROM ContactUsMessageResponse c WHERE c.resolution = :resolution"),
        @NamedQuery(name = "ContactUsMessageResponse.findByMessage", query = "SELECT c FROM ContactUsMessageResponse c WHERE c.message = :message"),
        @NamedQuery(name = "ContactUsMessageResponse.findByReplyDate", query = "SELECT c FROM ContactUsMessageResponse c WHERE c.responseDate = :responseDate"),
        @NamedQuery(name = "ContactUsMessageResponse.findByAttachmentId", query = "SELECT c FROM ContactUsMessageResponse c WHERE c.attachmentId = :attachmentId"),
        @NamedQuery(name = "ContactUsMessageResponse.findByRemarks", query = "SELECT c FROM ContactUsMessageResponse c WHERE c.remarks = :remarks")})

public class ContactUsMessageResponse implements EntityBean {

     private Integer contactUsMessageResponseId;
     private int contactUsMessageId;
     private int adminUserId;
     private String resolution;
     private String message;
     private Date responseDate;
     private int attachmentId;
     private String remarks;
     private ContactUsMessage originalMessage;
    private Attachment attachment;

    public ContactUsMessageResponse() {
    }

    public ContactUsMessageResponse(Integer id) {
        this.contactUsMessageResponseId=id;
    }
    public ContactUsMessageResponse(int contactUsMessageId, int adminUserId, String resolution, String message,Date replyDate, int attachmentId, String remarks) {
       this.contactUsMessageId = contactUsMessageId;
       this.adminUserId = adminUserId;
       this.resolution = resolution;
       this.message = message;
       this.responseDate = replyDate;
       this.attachmentId = attachmentId;
       this.remarks = remarks;
    }
   
    @Id @GeneratedValue(strategy=IDENTITY)
    @Column(name="contact_us_message_reply_id", unique=true, nullable=false)
    public Integer getContactUsMessageResponseId() {
        return this.contactUsMessageResponseId;
    }
    
    public void setContactUsMessageResponseId(Integer contactUsMessageResponseId) {
        this.contactUsMessageResponseId = contactUsMessageResponseId;
    }

    
    @Column(name="contact_us_message_id", nullable=false,updatable = false,insertable = false)
    public int getContactUsMessageId() {
        return this.contactUsMessageId;
    }
    
    public void setContactUsMessageId(int contactUsMessageId) {
        this.contactUsMessageId = contactUsMessageId;
    }

    
    @Column(name="admin_user_id", nullable=false)
    public int getAdminUserId() {
        return this.adminUserId;
    }
    
    public void setAdminUserId(int adminUserId) {
        this.adminUserId = adminUserId;
    }

    
    @Column(name="resolution", nullable=false)
    public String getResolution() {
        return this.resolution;
    }
    
    public void setResolution(String resolution) {
        this.resolution = resolution;
    }

    
    @Column(name="message", nullable=false, length=1000)
    public String getMessage() {
        return this.message;
    }
    
    public void setMessage(String message) {
        this.message = message;
    }

    
    @Column(name="response_date", nullable=false)
    public Date getResponseDate() {
        return this.responseDate;
    }
    
    public void setResponseDate(Date responseDate) {
        this.responseDate = responseDate;
    }

    
    @Column(name="attachment_id", nullable=false,updatable = false,insertable = false)
    public int getAttachmentId() {
        return this.attachmentId;
    }
    
    public void setAttachmentId(int attachmentId) {
        this.attachmentId = attachmentId;
    }

    
    @Column(name="remarks", nullable=false, length=1000)
    public String getRemarks() {
        return this.remarks;
    }
    
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "contact_us_message_id")
    @NotFound(action = NotFoundAction.EXCEPTION)
    public ContactUsMessage getOriginalMessage() {
        return originalMessage;
    }

    public void setOriginalMessage(ContactUsMessage originalMessage) {
        this.originalMessage = originalMessage;
    }

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL,orphanRemoval = true)
    @JoinColumn(name = "attachment_id")
    @NotFound(action = NotFoundAction.IGNORE)
    @Fetch(value = FetchMode.JOIN)
    public Attachment getAttachment() {
        return attachment;
    }

    public void setAttachment(Attachment attachment) {
        this.attachment = attachment;
    }

    @Override
    public String toString() {
        return "ContactUsMessageResponse{" +
                "contactUsMessageResponseId=" + contactUsMessageResponseId +
                ", contactUsMessageId=" + contactUsMessageId +
                ", adminUserId=" + adminUserId +
                ", resolution='" + resolution + '\'' +
                ", message='" + message + '\'' +
                ", responseDate=" + responseDate +
                ", attachmentId=" + attachmentId +
                ", remarks='" + remarks + '\'' +
                '}';
    }
}


