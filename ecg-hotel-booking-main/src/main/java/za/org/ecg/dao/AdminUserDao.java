package za.org.ecg.dao;

import za.org.ecg.dao.entity.AdminUser;

/**
 * Created by f4817273 on 2016/09/05.
 */
public interface AdminUserDao extends GenericDao<AdminUser> {

}
