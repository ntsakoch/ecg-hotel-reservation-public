package za.org.ecg.dao;

import za.org.ecg.dao.entity.CustomerAccount;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.List;

/**
 * Created by F4817273 on 2016/09/22.
 */
public interface CustomerAccountDao extends GenericDao<CustomerAccount> {
    List<CustomerAccount> findByMinimumOutstanding(double minimumBalance) throws EcgDAOException;
}
