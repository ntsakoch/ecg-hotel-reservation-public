package za.org.ecg.dao.exception;

/**
 * Created by root on 2016/10/02.
 */
public class EcgDAOException  extends Exception{
    public EcgDAOException(String message)
    {
        super(message);
    }
    public EcgDAOException(String message,Throwable cause)
    {
        super(message,cause);
    }
}
