package za.org.ecg.dao.impl;

import org.springframework.stereotype.Repository;
import za.org.ecg.dao.LocationAddressDao;
import za.org.ecg.dao.entity.LocationAddress;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.List;

/**
 * Created by Ntsako on 2017/02/21.
 */
@Repository("locationAddressDao")
public class LocationAddressDaoImpl  extends AbstractDao<LocationAddress> implements LocationAddressDao{
    private final static String FIND_BY_SUBURB_NAME_QUERY="SELECT location_address_id FROM location_addresses WHERE suburb_name LIKE %s";
    @Override
    public List<LocationAddress> findBySuburbName(String suburbName) throws EcgDAOException {
        List<Object[]> result=executeSQLQuery(String.format(FIND_BY_SUBURB_NAME_QUERY,"%'"+suburbName+"%'"));
        return findBySQLQueryResult(result,0);
    }
}
