package za.org.ecg.dao.impl;

import org.springframework.stereotype.Repository;
import za.org.ecg.dao.LanguageDao;
import za.org.ecg.dao.entity.Language;

/**
 * Created by Ntsako on 2017/01/18.
 */
@Repository("languageDao")
public class LanguageDaoImpl extends AbstractDao<Language> implements LanguageDao{
}
