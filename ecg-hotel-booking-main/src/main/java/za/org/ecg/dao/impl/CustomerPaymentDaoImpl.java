package za.org.ecg.dao.impl;

import org.springframework.stereotype.Repository;
import za.org.ecg.dao.CustomerPaymentDao;
import za.org.ecg.dao.entity.CustomerPayment;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by root on 2016/09/28.
 */
@Repository("customerPaymentDao")
public class CustomerPaymentDaoImpl extends AbstractDao<CustomerPayment> implements CustomerPaymentDao {

    private  static final String FIND_CUSTOMER_ID_NUMBER_QUERY="SELECT d.identity_no,'id' FROM customer_payments a\n" +
            "INNER JOIN customer_accounts AS b ON a.customer_account_id=b.customer_account_id\n" +
            "INNER JOIN customers AS c ON b.customer_id=c.customer_id\n" +
            "INNER JOIN users AS d ON c.user_id=d.user_id\n" +
            "WHERE d.identity_no=:idNumber";

    public CustomerPaymentDaoImpl() {
        super(CustomerPayment.class);
    }

    @Override
    public List<CustomerPayment> findByCustomerIdNumber(String customerIdNumber) throws EcgDAOException {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("idNumber", customerIdNumber);
            List<Object[]> result = executeSQLQuery(FIND_CUSTOMER_ID_NUMBER_QUERY, params);
            return findBySQLQueryResult(result, 0);
    }

}
