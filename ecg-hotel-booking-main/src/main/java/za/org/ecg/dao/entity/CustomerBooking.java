package za.org.ecg.dao.entity;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import static javax.persistence.GenerationType.IDENTITY;
import static za.org.ecg.dao.entity.EntityBean.CATALOG;

import java.util.Date;
import java.util.Set;

/**
 *
 * @author F4817273
 */

@Entity
@Table(name="customer_bookings"
    ,catalog=CATALOG
)
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "CustomerBooking.findAll", query = "SELECT c FROM CustomerBooking c"),
        @NamedQuery(name = "CustomerBooking.findByCustomerBookingId", query = "SELECT c FROM CustomerBooking c WHERE c.customerBookingId = :customerBookingId"),
        @NamedQuery(name = "CustomerBooking.findByCustomerId", query = "SELECT c FROM CustomerBooking c WHERE c.customerId = :customerId"),
        @NamedQuery(name = "CustomerBooking.findByStatus", query = "SELECT c FROM CustomerBooking c WHERE c.status = :status"),
        @NamedQuery(name = "CustomerBooking.findByCheckinDate", query = "SELECT c FROM CustomerBooking c WHERE c.checkinDate = :checkinDate"),
        @NamedQuery(name = "CustomerBooking.findByCheckoutDate", query = "SELECT c FROM CustomerBooking c WHERE c.checkoutDate = :checkoutDate"),
        @NamedQuery(name = "CustomerBooking.findByCheckoutDateRange", query = "SELECT c FROM CustomerBooking c WHERE c.checkoutDate BETWEEN :fromDate AND :toDate"),
        @NamedQuery(name = "CustomerBooking.findByCheckinDateRange", query = "SELECT c FROM CustomerBooking c WHERE c.checkinDate BETWEEN :fromDate AND :toDate"),
        @NamedQuery(name = "CustomerBooking.findByDateCreationRange", query = "SELECT c FROM CustomerBooking c WHERE c.dateCreated BETWEEN :fromDate AND :toDate"),
        @NamedQuery(name = "CustomerBooking.findByDateCreated", query = "SELECT c FROM CustomerBooking c WHERE c.dateCreated = :dateCreated")})

public class CustomerBooking  implements EntityBean {


     private Integer customerBookingId;
     private int customerId;
     private String status;
     private Date checkinDate;
     private Date checkoutDate;
     private int roomId;
     private Date dateCreated;
     private Room bookedRoom;
     private Customer customer;
     private CustomerInvoice invoice;
     private Set<CustomerPreference> preferences;
     private Set<BookingCheckin> checkIns;

    public CustomerBooking() {
    }

    public CustomerBooking(Integer customerBookingId) {
        this.customerBookingId=customerBookingId;
    }
    public CustomerBooking(int customerId, String status, Date checkeinDate, Date checkoutDate, int roomId, Date dateCreated) {
       this.customerId = customerId;
       this.status = status;
       this.checkinDate = checkeinDate;
       this.checkoutDate = checkoutDate;
       this.roomId = roomId;
       this.dateCreated = dateCreated;
    }
   
    @Id @GeneratedValue(strategy=IDENTITY)
    @Column(name="customer_booking_id", unique=true, nullable=false)
    public Integer getCustomerBookingId() {
        return this.customerBookingId;
    }
    
    public void setCustomerBookingId(Integer customerBookingId) {
        this.customerBookingId = customerBookingId;
    }


    @Column(name="customer_id", nullable=false,insertable = false,updatable = false)
    public int getCustomerId() {
        return this.customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    
    @Column(name="status", nullable=false, length=12)
    public String getStatus() {
        return this.status;
    }
    
    public void setStatus(String status) {
        this.status = status;
    }

    
    @Column(name="checkin_date", nullable=false)
    public Date getCheckinDate() {
        return this.checkinDate;
    }
    
    public void setCheckinDate(Date checkeinDate) {
        this.checkinDate = checkeinDate;
    }

    
    @Column(name="checkout_date", nullable=false)
    public Date getCheckoutDate() {
        return this.checkoutDate;
    }
    
    public void setCheckoutDate(Date checkoutDate) {
        this.checkoutDate = checkoutDate;
    }

    
    @Column(name="room_id", nullable=false,insertable = false,updatable = false)
    public int getRoomId() {
        return this.roomId;
    }
    
    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    
    @Column(name="date_created", nullable=false)
    public Date getDateCreated() {
        return this.dateCreated;
    }
    
    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }


    @OneToOne(fetch = FetchType.EAGER,cascade = CascadeType.ALL,orphanRemoval = true)
    @JoinColumn(name = "customer_booking_id")
    @NotFound(action = NotFoundAction.EXCEPTION)
    @Fetch(value = FetchMode.JOIN)
    public CustomerInvoice getInvoice() {
        return invoice;
    }

    public void setInvoice(CustomerInvoice invoice) {
        this.invoice = invoice;
    }

   @ManyToOne(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @JoinColumn(name = "customer_id")
    @NotFound(action = NotFoundAction.EXCEPTION)
    @Fetch(value = FetchMode.JOIN)
    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @OneToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL,orphanRemoval = true)
    @JoinColumn(name = "customer_booking_id")
    @Fetch(value = FetchMode.SUBSELECT)
    public Set<CustomerPreference> getPreferences() {
        return preferences;
    }

    public void setPreferences(Set<CustomerPreference> preferences) {
        this.preferences = preferences;
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "room_id")
    @NotFound(action = NotFoundAction.EXCEPTION)
    public Room getBookedRoom() {
        return bookedRoom;
    }

    public void setBookedRoom(Room bookedRoom) {
        this.bookedRoom = bookedRoom;
    }

    @OneToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL,orphanRemoval = true)
    @JoinColumn(name = "customer_booking_id")
    @Fetch(value = FetchMode.SUBSELECT)
    public Set<BookingCheckin> getCheckIns() {
        return checkIns;
}

    public void setCheckIns(Set<BookingCheckin> checkIns) {
        this.checkIns = checkIns;
    }

    @Override
    public String toString() {
        return "CustomerBooking{" +
                "customerBookingId=" + customerBookingId +
                ", customerId=" + customerId +
                ", status='" + status + '\'' +
                ", checkinDate=" + checkinDate +
                ", checkoutDate=" + checkoutDate +
                ", roomId=" + roomId +
                ", dateCreated=" + dateCreated +
                '}';
    }
}


