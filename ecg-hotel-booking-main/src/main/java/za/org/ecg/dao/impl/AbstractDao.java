/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.org.ecg.dao.impl;

import org.hibernate.*;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import za.org.ecg.dao.cache.EnableEntityCaching;
import za.org.ecg.dao.cache.EntityCacheManagerProvider;
import za.org.ecg.dao.cache.EntityCacheException;
import za.org.ecg.dao.entity.EntityBean;
import za.org.ecg.dao.exception.EcgDAOException;

import java.lang.reflect.ParameterizedType;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @author F4817273
 */
public abstract class AbstractDao<T> {
    private Class<? extends EntityBean> entityClass;

    @Autowired
    private SessionFactory sessionFactory;

    public AbstractDao() {
        this.entityClass = (Class<EntityBean>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0];
    }

    public AbstractDao(Class<? extends EntityBean> entityClass) {
        this.entityClass = entityClass;
    }

    public List<T> findAll() throws EcgDAOException {
        try {
            if(entityClass.isAnnotationPresent(EnableEntityCaching.class)&&
                    EntityCacheManagerProvider.getInstance().getCacheManager().isCacheInitialized()    ) {
                return EntityCacheManagerProvider.getInstance().getCacheManager().findAll(entityClass);
            }
            return getSession().createCriteria(entityClass).list();
        } catch (HibernateException | EntityCacheException exc) {
            throw new EcgDAOException(exc.getMessage(), exc);
        }
    }

    public T findById(Integer id) throws EcgDAOException {
        try {
            if(entityClass.isAnnotationPresent(EnableEntityCaching.class)&&
                    EntityCacheManagerProvider.getInstance().getCacheManager().isCacheInitialized())
            {
                return (T) EntityCacheManagerProvider.getInstance().getCacheManager().findById(entityClass,id);
            }
            return (T) getSession().get(entityClass, id);
        } catch (HibernateException | EntityCacheException hibernateException) {
            throw new EcgDAOException(hibernateException.getMessage(), hibernateException);
        }
    }

    public void create(T entity) throws EcgDAOException {
        try {
            getSession().persist(entity);
            refreshCachedEntitiesForType();
        } catch (HibernateException | EntityCacheException exc) {
            throw new EcgDAOException(exc.getMessage(), exc);
        }
    }

    public void update(T newValue) throws EcgDAOException {
        try {
            getSession().update(newValue);
            updateEntityCache(newValue,false);
        } catch (HibernateException | EntityCacheException exc) {
            throw new EcgDAOException(exc.getMessage(), exc);
        }
    }

    public T merge(T entity) throws EcgDAOException {
        try {
            T result= (T) getSession().merge(entity);
            if(((EntityBean)entity).getId()==-1)
            {
                refreshCachedEntitiesForType();
            }
            else
            {
                updateEntityCache(entity,false);
            }
            return result;
        }  catch (HibernateException | EntityCacheException exc) {
            throw new EcgDAOException(exc.getMessage(), exc);
        }
    }

    public Integer save(T entity) throws EcgDAOException {
        try {
            int id= (Integer) getSession().save(entity);
            refreshCachedEntitiesForType();
            return id;
        }  catch (HibernateException | EntityCacheException exc) {
            throw new EcgDAOException(exc.getMessage(), exc);
        }
    }

    protected List<T> findByNamedQuery(String namedQuery, Map<String, Object> params) throws EcgDAOException {
        try {
            Query query = getSession().getNamedQuery(namedQuery);
            if (params != null && !params.isEmpty()) {
                for (String param : params.keySet()) {
                    query.setParameter(param, params.get(param));
                }
            }
            return query.list();
        } catch (HibernateException hibernateException) {
            throw new EcgDAOException(hibernateException.getMessage(), hibernateException);
        }
    }

    protected List<T> findBySQLQueryResult(List<Object[]> result, int columnIndex) throws EcgDAOException {
        try {
            List<T> list = new ArrayList<>();
            for (Object[] data : result) {
                Integer id = Integer.parseInt(String.valueOf(data[columnIndex]));
                list.add(findById(id));
            }
            return list;
        } catch (ArrayIndexOutOfBoundsException exc) {
            throw new EcgDAOException("No SQLQuery result at index: " + columnIndex, exc);
        }
    }

    protected List<T> findByDate(Date startDate, String dateField) throws EcgDAOException {
        try {
            Criteria criteria = getSession().createCriteria(entityClass);
            Date maxDate = new Date(startDate.getTime() + TimeUnit.DAYS.toMillis(1));
            Conjunction and = Restrictions.conjunction();
            and.add(Restrictions.ge(dateField, startDate));
            and.add(Restrictions.lt(dateField, maxDate));
            criteria.add(and);
            return criteria.list();
        } catch (HibernateException hibernateException) {
            throw new EcgDAOException(hibernateException.getMessage(), hibernateException);
        }
    }

    public void remove(T entity) throws EcgDAOException {
        try {
            getSession().delete(entity);
            updateEntityCache(entity,true);
        } catch (HibernateException | EntityCacheException exc) {
            throw new EcgDAOException(exc.getMessage(), exc);
        }
    }

    public long count() throws EcgDAOException {
        try {
            return (Long) getSession().createCriteria(entityClass).setProjection(Projections.rowCount()).uniqueResult();
        } catch (HibernateException hibernateException) {
            throw new EcgDAOException(hibernateException.getMessage(), hibernateException);
        }
    }

    public List<Object[]> executeSQLQuery(String query, Map<String, Object> params) throws EcgDAOException {
        try {
            SQLQuery sqlQuery = getSession().createSQLQuery(query);
            if (params != null && !params.isEmpty()) {
                for (String key : params.keySet())
                    sqlQuery.setParameter(key, params.get(key));
            }
            System.out.println("executing query: "+sqlQuery.toString());
            return (List<Object[]>) sqlQuery.list();
        } catch (HibernateException hibernateException) {
            throw new EcgDAOException(hibernateException.getMessage(), hibernateException);
        }
    }

    public List<Object[]> executeSQLQuery(String query) throws EcgDAOException {
        try {
            return (List<Object[]>) getSession().createSQLQuery(query).list();
        } catch (HibernateException hibernateException) {
            throw new EcgDAOException(hibernateException.getMessage(), hibernateException);
        }
    }

    public List<T> findRange(int start, int end) throws EcgDAOException {
        try {
            Criteria criteria = getSession().createCriteria(entityClass);
            start = start != 0 ? start - 1 : start;
            criteria.setFirstResult(start);
            criteria.setMaxResults(end - start);
            return criteria.list();
        } catch (HibernateException hibernateException) {
            throw new EcgDAOException(hibernateException.getMessage(), hibernateException);
        }
    }

    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    private void refreshCachedEntitiesForType() throws EntityCacheException
    {
                 if(entityClass.isAnnotationPresent(EnableEntityCaching.class)&&
                        EntityCacheManagerProvider.getInstance().getCacheManager().isCacheInitialized())
                {
                    EntityCacheManagerProvider.getInstance().getCacheManager().refreshCachedEntitiesForType(entityClass);
                }
    }

    private void updateEntityCache(T newValue,boolean removeFromCache) throws EntityCacheException {
                if(entityClass.isAnnotationPresent(EnableEntityCaching.class)&&
                        EntityCacheManagerProvider.getInstance().getCacheManager().isCacheInitialized())
                    EntityCacheManagerProvider.getInstance().getCacheManager().updateCache(newValue,removeFromCache);

                }

}
