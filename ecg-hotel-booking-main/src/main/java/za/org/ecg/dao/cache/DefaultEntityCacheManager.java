package za.org.ecg.dao.cache;

import org.apache.log4j.Logger;
import org.reflections.Reflections;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import za.org.ecg.dao.entity.EntityBean;
import za.org.ecg.dao.impl.AbstractDao;
import za.org.ecg.springmvc.configuration.DataSourceConfiguration;
import za.org.ecg.springmvc.service.EntityCacheDaoService;
import za.org.ecg.test.spring.configuration.ServiceBeanConfiguration;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Ntsako on 2016/11/11.
 */
public class DefaultEntityCacheManager implements EntityCacheManager<EntityBean> {

    private EntityCacheDaoService entityCacheDaoService;

    private static ApplicationContext applicationContext;
    private boolean  cacheInitialized=false;
    private int numberOfItemsInCache=0;
    private final Object lock=new Object();

    private static final Logger  LOG=Logger.getLogger(DefaultEntityCacheManager.class);

    private static DefaultEntityCacheManager INSTANCE=new DefaultEntityCacheManager();
    private static final Map<Class<? extends EntityBean>,Map<Integer,EntityBean>> entityCache=new ConcurrentHashMap<>();

    private DefaultEntityCacheManager(){}

    public static EntityCacheManager getInstance()
    {
        return INSTANCE;
    }

    @Override
    public void init() throws EntityCacheException
    {
        synchronized (lock) {
            LOG.info("initializing entity cache....");
            try {
                applicationContext = new AnnotationConfigApplicationContext(ServiceBeanConfiguration.class, DataSourceConfiguration.class);
                entityCacheDaoService = (EntityCacheDaoService) applicationContext.getBean("entityCacheDaoService");
                String entityBeansPackage = EntityBean.class.getPackage().getName();
                Set<Class<? extends EntityBean>> entityClasses = new Reflections(entityBeansPackage).getSubTypesOf(EntityBean.class);
                for (Class<? extends EntityBean> entityClass : entityClasses) {
                    if (entityClass.isAnnotationPresent(EnableEntityCaching.class)) {
                        LOG.info("Caching enabled for class: " + entityClass);
                        retrieveAllEntitiesAndAddToCache(getAbstractDao(entityClass));
                    }
                }
                this.cacheInitialized = true;
            } catch (Exception exc) {
                LOG.error("An error occurred initializing entity cache: ", exc);
            }
            LOG.info("Cache Initialized: "+entityCache);
            LOG.info("Done initializing entity cache,  cache size: "+entityCache.size()+"===>"+numberOfItemsInCache);
        }
    }

    private void retrieveAllEntitiesAndAddToCache(AbstractDao abstractDao) throws Exception {
        LOG.info("About to add entity values to cache: "+abstractDao.getClass());
        List<? extends EntityBean> resultList= entityCacheDaoService.findAll(abstractDao);
        for(EntityBean entityBean: resultList)
        {
            addOrUpdateCache(entityBean);
        }
    }

    private void addOrUpdateCache(EntityBean entityBean) throws Exception {
        if(!entityCache.containsKey(entityBean.getClass())) {
            LOG.info("Adding to cache: "+entityBean.getClass()+", entity: "+entityBean);
            Map<Integer,EntityBean> entityBeanMap=new HashMap<>();
            entityBeanMap.put(entityBean.getId(),entityBean);
            entityCache.put(entityBean.getClass(), entityBeanMap);
        }
        else
        {
            Map<Integer,EntityBean> entityBeanMap= entityCache.get(entityBean.getClass());
            entityBeanMap.put(entityBean.getId(),entityBean);
        }
        numberOfItemsInCache++;
    }

    @Override
    public synchronized void updateCache(EntityBean entityBean,boolean removeFromCache) throws EntityCacheException {
        try {
            synchronized (lock) {
                LOG.info("onEntityUpdated: " + entityBean + ", removeFromCache: " + removeFromCache);
                if (removeFromCache) {
                    entityCache.get(entityBean.getClass()).remove(entityBean.getId());
                    numberOfItemsInCache--;
                } else {
                    Map<Integer, EntityBean> entityBeanMap = entityCache.get(entityBean.getClass());
                    entityBeanMap.put(entityBean.getId(), entityBean);
                }
            }
        }
        catch (Exception exc)
        {
            throw new EntityCacheException("An error occurred trying to update cached entityBean",exc);
        }

    }

    @Override
    public synchronized void refreshCache() throws EntityCacheException {
        synchronized (lock) {
            LOG.info("Refreshing the cache....");
            entityCache.clear();
            init();
        }
    }

    @Override
    public void refreshCachedEntitiesForType(Class<? extends EntityBean> entityClass) throws EntityCacheException {
        try {
            synchronized (lock) {
                LOG.info("refreshCachedEntitiesForType: "+entityClass);
                LOG.info("Before refreshing cached entities for type: "+entityClass);
                LOG.info(entityCache.get(entityClass).size()+"===>"+entityCache.get(entityClass));
                List<? extends EntityBean> resultList = entityCacheDaoService.findAll(getAbstractDao(entityClass));
                entityCache.remove(entityClass);
                for (EntityBean entityBean : resultList) {
                    addOrUpdateCache(entityBean);
                }
                LOG.info("After refreshing cached entities for type: "+entityClass);
                LOG.info(entityCache.get(entityClass).size()+"===>"+entityCache.get(entityClass));
                LOG.info("Done refreshing cache!");
            }
        }
        catch (Exception exc)
        {
            throw new EntityCacheException("An error occurred trying to refresh entity cache: "+entityClass,exc);
        }
    }

    private AbstractDao getAbstractDao(Class<? extends EntityBean> entityClass) throws  Exception {
        EnableEntityCaching metaData = entityClass.getAnnotation(EnableEntityCaching.class);
        Class<? extends AbstractDao> daoClassImpl = metaData.daoImplementer();
        return applicationContext.getBean(daoClassImpl);
    }

    public  List<EntityBean> findAll(Class<? extends EntityBean> entityClazz) throws EntityCacheException
    {
        try {
            synchronized (lock) {
                LOG.info("retrieving from cache: " + entityClazz);
                return new ArrayList<>(entityCache.get(entityClazz).values());
            }
        }
        catch (Exception exc)
        {
            throw new EntityCacheException("An error occurred trying to get cached entities: "+entityClazz,exc);
        }
    }

    public EntityBean findById(Class<? extends EntityBean> entityClazz,Integer id) throws EntityCacheException
    {
        try {
            synchronized (lock) {
                LOG.info("retrieving from cache: " + entityClazz + ", id: " + id);
                return entityCache.get(entityClazz).get(id);
            }
        }
          catch (Exception exc)
        {
            throw new EntityCacheException("An error occurred trying to get cached entity: "+id,exc);
        }
    }

    @Override
    public boolean isCacheInitialized() {
        return cacheInitialized;
    }

}
