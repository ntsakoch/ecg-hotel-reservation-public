package za.org.ecg.dao.impl;

import org.springframework.stereotype.Repository;
import za.org.ecg.dao.ApplicationPermissionDao;
import za.org.ecg.dao.entity.ApplicationPermission;

/**
 * Created by F4817273 on 2016/09/15.
 */
@Repository("applicationPermissionDao")
public class ApplicationPermissionDaoImpl extends AbstractDao<ApplicationPermission> implements ApplicationPermissionDao {
    public ApplicationPermissionDaoImpl() {
        super(ApplicationPermission.class);
    }
}
