package za.org.ecg.dao;

import za.org.ecg.dao.entity.CustomerBooking;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.Date;
import java.util.List;

/**
 * Created by F4817273 on 2016/09/22.
 */
public interface CustomerBookingDao extends GenericDao<CustomerBooking> {
    List<CustomerBooking> findByStatus(String status) throws EcgDAOException;
    List<CustomerBooking> findByCheckinDate(Date date) throws EcgDAOException;
    List<CustomerBooking> findByCheckoutDate(Date date) throws EcgDAOException;
    List<CustomerBooking> findByCheckoutDateRange(Date from,Date to) throws EcgDAOException;
    List<CustomerBooking> findByCheckinDateRange(Date from,Date to) throws EcgDAOException;
    List<CustomerBooking> findByDateCreationRange(Date from,Date to) throws EcgDAOException;
    List<CustomerBooking> findByCustomerIdNumber(String customerIdNumber) throws EcgDAOException;
    List<CustomerBooking> findAllOutstandingBookings(double minimumAmount) throws EcgDAOException;
}
