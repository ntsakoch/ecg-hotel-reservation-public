package za.org.ecg.dao;

import za.org.ecg.dao.entity.RoomLocation;

/**
 * Created by Ntsako on 2017/02/21.
 */
public interface RoomLocationDao extends  GenericDao<RoomLocation> {
}
