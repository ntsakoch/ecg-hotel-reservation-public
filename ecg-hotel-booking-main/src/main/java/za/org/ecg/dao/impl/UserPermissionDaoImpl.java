package za.org.ecg.dao.impl;

import org.springframework.stereotype.Repository;
import za.org.ecg.dao.UserPermissionDao;
import za.org.ecg.dao.entity.UserPermission;

/**
 * Created by F4817273 on 2016/09/15.
 */
@Repository("userPermissionDao")
public class UserPermissionDaoImpl extends AbstractDao<UserPermission> implements UserPermissionDao {
    public UserPermissionDaoImpl() {
        super(UserPermission.class);
    }
}
