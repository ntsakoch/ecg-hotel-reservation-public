package za.org.ecg.dao.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import static za.org.ecg.dao.entity.EntityBean.CATALOG;

/**
 * Created by Ntsako on 2016/12/23.
 */
@Entity
@Table(name = "booking_checkins", catalog = CATALOG)
@XmlRootElement
@NamedQueries({})
public class BookingCheckin implements EntityBean{

    private int bookingCheckInId;
    private int bookingId;
    private String fullName;
    private String identityNumber;
    private String checkInStatus;
    private CustomerBooking customerBooking;

    public BookingCheckin() {
    }

    public BookingCheckin(int bookingId) {
        this.bookingId = bookingId;
    }

    public BookingCheckin(int bookingId, String fullName, String identityNumber, String checkInStatus) {
        this.bookingId = bookingId;
        this.fullName = fullName;
        this.identityNumber = identityNumber;
        this.checkInStatus = checkInStatus;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "booking_checkin_id")
    public int getBookingCheckInId() {
        return bookingCheckInId;
    }

    public void setBookingCheckInId(int bookingCheckInId) {
        this.bookingCheckInId = bookingCheckInId;
    }

    @Column(name = "customer_booking_id",insertable = false,updatable = false)
    public int getBookingId() {
        return bookingId;
    }

    public void setBookingId(int bookingId) {
        this.bookingId = bookingId;
    }

    @Column(name = "full_name")
    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @Column(name = "identity_number")
    public String getIdentityNumber() {
        return identityNumber;
    }

    public void setIdentityNumber(String identityNumber) {
        this.identityNumber = identityNumber;
    }

    @Column(name = "status")
    public String getCheckInStatus() {
        return checkInStatus;
    }

    public void setCheckInStatus(String checkInStatus) {
        this.checkInStatus = checkInStatus;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "customer_booking_id")
    @NotFound(action = NotFoundAction.EXCEPTION)
    @Fetch(FetchMode.JOIN)
    @JsonIgnore
    public CustomerBooking getCustomerBooking() {
        return customerBooking;
    }

    public void setCustomerBooking(CustomerBooking customerBooking) {
        this.customerBooking = customerBooking;
    }

    @Override
    public String toString() {
        return "BookingCheckin{" +
                "bookingCheckInId=" + bookingCheckInId +
                ", bookingId=" + bookingId +
                ", fullName='" + fullName + '\'' +
                ", identityNumber='" + identityNumber + '\'' +
                ", checkInStatus='" + checkInStatus + '\'' +
                '}';
    }
}
