package za.org.ecg.dao.entity;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import za.org.ecg.dao.cache.EnableEntityCaching;
import za.org.ecg.dao.impl.RoomLocationDaoImpl;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import static za.org.ecg.dao.entity.EntityBean.CATALOG;

/**
 * Created by Ntsako on 2017/02/21.
 */
@Entity
@Table(name="rooms_locations"
        ,catalog=CATALOG
)
@XmlRootElement
@EnableEntityCaching(daoImplementer = RoomLocationDaoImpl.class)
public class RoomLocation implements EntityBean {

    private int  roomLocationId;
    private int locationAddressId;
    private int roomId;
    private LocationAddress locationAddress;
    private Room room;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "room_location_id")
    public int getRoomLocationId() {
        return roomLocationId;
    }

    public void setRoomLocationId(int roomLocationId) {
        this.roomLocationId = roomLocationId;
    }

    @Column(name = "location_address_id",insertable = false,updatable = false)
    public int getLocationAddressId() {
        return locationAddressId;
    }

    public void setLocationAddressId(int locationAddressId) {
        this.locationAddressId = locationAddressId;
    }

    @Column(name = "room_id",insertable = false,updatable = false)
    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "location_address_id")
    @NotFound(action = NotFoundAction.EXCEPTION)
    public LocationAddress getLocationAddress() {
        return locationAddress;
    }

    public void setLocationAddress(LocationAddress locationAddress) {
        this.locationAddress = locationAddress;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "room_id")
    @NotFound(action = NotFoundAction.EXCEPTION)
    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    @Override
    @Transient
    public Integer getId() {
        return  roomLocationId;
    }

    @Override
    public String toString() {
        return "RoomLocation{" +
                "roomLocationId=" + roomLocationId +
                ", locationAddressId=" + locationAddressId +
                ", roomId=" + roomId +
                '}';
    }
}
