package za.org.ecg.dao.entity;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import static javax.persistence.GenerationType.IDENTITY;
import static za.org.ecg.dao.entity.EntityBean.CATALOG;

import java.io.Serializable;

/**
 *
 * @author F4817273
 */
@Entity
@Table(name="mail_messages"
    ,catalog=CATALOG
)
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "MailMessage.findAll", query = "SELECT m FROM MailMessage m"),
        @NamedQuery(name = "MailMessage.findByMailMessageId", query = "SELECT m FROM MailMessage m WHERE m.mailMessageId = :mailMessageId"),
        @NamedQuery(name = "MailMessage.findByCustomerId", query = "SELECT m FROM MailMessage m WHERE m.customerId = :customerId"),
        @NamedQuery(name = "MailMessage.findBySource", query = "SELECT m FROM MailMessage m WHERE m.source = :source"),
        @NamedQuery(name = "MailMessage.findByDestination", query = "SELECT m FROM MailMessage m WHERE m.destination = :destination"),
        @NamedQuery(name = "MailMessage.findBySubject", query = "SELECT m FROM MailMessage m WHERE m.subject = :subject"),
        @NamedQuery(name = "MailMessage.findByMessage", query = "SELECT m FROM MailMessage m WHERE m.message = :message"),
        @NamedQuery(name = "MailMessage.findByDirection", query = "SELECT m FROM MailMessage m WHERE m.direction = :direction"),
        @NamedQuery(name = "MailMessage.findByDateSent", query = "SELECT m FROM MailMessage m WHERE m.date = :date")})

public class MailMessage  implements EntityBean {

     private Integer mailMessageId;
     private int customerId;
     private String source;
     private String destination;
     private String subject;
     private String message;
     private String direction;
     private String date;
     private int attachmentId;
     private Attachment attachment;

    public MailMessage() {
    }

    public MailMessage(Integer mailMessageId) {
        this.mailMessageId=mailMessageId;
    }
    public MailMessage(int customerId, String source, String destination, String subject, String message, String date) {
       this.customerId = customerId;
       this.source = source;
       this.destination = destination;
       this.subject = subject;
       this.message = message;
       this.date = date;
    }
   
    @Id @GeneratedValue(strategy=IDENTITY)
    @Column(name="mail_message_id", unique=true, nullable=false)
    public Integer getMailMessageId() {
        return this.mailMessageId;
    }
    
    public void setMailMessageId(Integer mailMessageId) {
        this.mailMessageId = mailMessageId;
    }

    
    @Column(name="customer_id", nullable=false)
    public int getCustomerId() {
        return this.customerId;
    }
    
    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    
    @Column(name="source", nullable=false)
    public String getSource() {
        return this.source;
    }
    
    public void setSource(String source) {
        this.source = source;
    }

    
    @Column(name="destination", nullable=false)
    public String getDestination() {
        return this.destination;
    }
    
    public void setDestination(String destination) {
        this.destination = destination;
    }

    
    @Column(name="subject", nullable=false)
    public String getSubject() {
        return this.subject;
    }
    
    public void setSubject(String subject) {
        this.subject = subject;
    }

    
    @Column(name="message", nullable=false, length=3000)
    public String getMessage() {
        return this.message;
    }
    
    public void setMessage(String message) {
        this.message = message;
    }

    
    @Column(name="date", nullable=false, length=50)
    public String getDate() {
        return this.date;
    }
    
    public void setDate(String date) {
        this.date = date;
    }

    @Column(name="direction", nullable=false, length=50)
    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    @Column(name="attachment_id", updatable = false,insertable = false)
    public int getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(int attachmentId) {
        this.attachmentId = attachmentId;
    }

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL,orphanRemoval = true)
    @JoinColumn(name = "attachment_id")
    @NotFound(action = NotFoundAction.IGNORE)
    @Fetch(value = FetchMode.JOIN)
    public Attachment getAttachment() {
        return attachment;
    }

    public void setAttachment(Attachment attachment) {
        this.attachment = attachment;
    }
}


