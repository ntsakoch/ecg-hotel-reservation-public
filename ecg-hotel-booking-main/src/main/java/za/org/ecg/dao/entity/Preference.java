package za.org.ecg.dao.entity;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import za.org.ecg.dao.cache.EnableEntityCaching;
import za.org.ecg.dao.impl.PreferenceDaoImpl;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import static javax.persistence.GenerationType.IDENTITY;
import static za.org.ecg.dao.entity.EntityBean.CATALOG;

import java.io.Serializable;

/**
 *
 * @author F4817273
 */

@Entity
@Table(name="preferences"
    ,catalog=CATALOG
)
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "Preference.findAll", query = "SELECT p FROM Preference p"),
        @NamedQuery(name = "Preference.findByPreferenceId", query = "SELECT p FROM Preference p WHERE p.preferenceId = :preferenceId"),
        @NamedQuery(name = "Preference.findByPreferenceCategoryId", query = "SELECT p FROM Preference p WHERE p.preferenceCategoryId = :preferenceCategoryId"),
        @NamedQuery(name = "Preference.findByPreferenceName", query = "SELECT p FROM Preference p WHERE p.preferenceName = :preferenceName"),
        @NamedQuery(name = "Preference.findByDescription", query = "SELECT p FROM Preference p WHERE p.description = :description")})
@EnableEntityCaching(daoImplementer = PreferenceDaoImpl.class)
public class Preference  implements EntityBean {


     private Integer preferenceId;
     private int preferenceCategoryId;
     private String preferenceName;
     private String description;
     private PreferenceCategory category;
     private int costId;
     private Cost preferenceCost;

    public Preference() {
    }
    public Preference(Integer preferenceId) {
        this.preferenceId=preferenceId;
    }
    public Preference(int preferenceCategoryId, String preferenceName, String description) {
       this.preferenceCategoryId = preferenceCategoryId;
       this.preferenceName = preferenceName;
       this.description = description;
    }
   
    @Id @GeneratedValue(strategy=IDENTITY)
    @Column(name="preference_id", unique=true, nullable=false)
    public Integer getPreferenceId() {
        return this.preferenceId;
    }
    
    public void setPreferenceId(Integer preferenceId) {
        this.preferenceId = preferenceId;
    }
    
    @Column(name="preference_category_id", nullable=false,updatable = false,insertable = false)
    public int getPreferenceCategoryId() {
        return this.preferenceCategoryId;
    }
    
    public void setPreferenceCategoryId(int preferenceCategoryId) {
        this.preferenceCategoryId = preferenceCategoryId;
    }

    @Column(name="preference_name", nullable=false, length=100)
    public String getPreferenceName() {
        return this.preferenceName;
    }
    
    public void setPreferenceName(String preferenceName) {
        this.preferenceName = preferenceName;
    }
    
    @Column(name="description", nullable=false)
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "preference_category_id")
    @NotFound(action = NotFoundAction.EXCEPTION)
    public PreferenceCategory getCategory() {
        return category;
    }

    public void setCategory(PreferenceCategory category) {
        this.category = category;
    }

    @Column(name="cost_id", nullable=false,updatable = false,insertable = false)
    public int getCostId() {
        return costId;
    }

    public void setCostId(int costId) {
        this.costId = costId;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "cost_id")
    @NotFound(action = NotFoundAction.EXCEPTION)
    public Cost getPreferenceCost() {
        return preferenceCost;
    }

    public void setPreferenceCost(Cost preferenceCost) {
        this.preferenceCost = preferenceCost;
    }

    @Override
    public String toString() {
        return "Preference{" +
                "preferenceId=" + preferenceId +
                ", preferenceCategoryId=" + preferenceCategoryId +
                ", preferenceName='" + preferenceName + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    @Override
    @Transient
    public Integer getId() {
        return preferenceId;
    }
}


