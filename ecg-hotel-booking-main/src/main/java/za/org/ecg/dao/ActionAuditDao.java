package za.org.ecg.dao;

import za.org.ecg.dao.entity.ActionAudit;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.Date;
import java.util.List;

/**
 * Created by Ntsako on 2017/01/21.
 */
public interface ActionAuditDao extends GenericDao<ActionAudit> {

    List<ActionAudit> findActionsByUser(int userId) throws EcgDAOException;

    List<ActionAudit> findActionsByTime(Date startTime,Date endTime) throws  EcgDAOException;

    List<ActionAudit> findActionsByType(String type) throws  EcgDAOException;

    List<ActionAudit> findActionsBySystemProcessName(String processName) throws  EcgDAOException;
}
