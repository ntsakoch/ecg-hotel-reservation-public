/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package za.org.ecg.dao.impl;

import org.springframework.stereotype.Repository;
import za.org.ecg.dao.CustomerDao;
import za.org.ecg.dao.entity.Customer;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author F4817273
 */
@Repository("customerDao")
public class CustomerDaoImpl extends AbstractDao<Customer> implements CustomerDao {

    private static final String FIND_BY_IDNUMBER_QUERY="SELECT a.customer_id,'c_id' FROM customer a\n" +
            "INNER JOIN user AS b ON a.user_id=b.user_id\n"+
            "AND b.identity_no=:id_number";

    public CustomerDaoImpl() {
        super(Customer.class);
    }


    @Override
    public Customer findByIdNumber(String idNumber) throws EcgDAOException {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("id_number", idNumber);
        List<Object[]> result = executeSQLQuery(FIND_BY_IDNUMBER_QUERY, params);
        List<Customer> customers=findBySQLQueryResult(result,0);
        return !customers.isEmpty()?customers.get(0):null;
    }
}
