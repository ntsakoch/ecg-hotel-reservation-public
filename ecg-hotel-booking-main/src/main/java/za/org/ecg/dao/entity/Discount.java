package za.org.ecg.dao.entity;

import za.org.ecg.dao.cache.EnableEntityCaching;
import za.org.ecg.dao.impl.DiscountDaoImpl;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import static javax.persistence.GenerationType.IDENTITY;
import static za.org.ecg.dao.entity.EntityBean.CATALOG;

import java.io.Serializable;

/**
 *
 * @author F4817273
 */

@Entity
@Table(name="discounts"
    ,catalog=CATALOG
)
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "Discount.findAll", query = "SELECT d FROM Discount d"),
        @NamedQuery(name = "Discount.findByDiscountId", query = "SELECT d FROM Discount d WHERE d.discountId = :discountId"),
        @NamedQuery(name = "Discount.findByMinimumBookings", query = "SELECT d FROM Discount d WHERE d.minimumBookings = :minimumBookings"),
        @NamedQuery(name = "Discount.findByMaximumBookings", query = "SELECT d FROM Discount d WHERE d.maximumBookings = :maximumBookings"),
        @NamedQuery(name = "Discount.findByPercentToDiscount", query = "SELECT d FROM Discount d WHERE d.percentToDiscount = :percentToDiscount"),
        @NamedQuery(name = "Discount.findByActive", query = "SELECT d FROM Discount d WHERE d.active = :active")})
@EnableEntityCaching(daoImplementer = DiscountDaoImpl.class)
public class Discount  implements EntityBean {

     private Integer discountId;
     private int minimumBookings;
     private int maximumBookings;
     private int percentToDiscount;
     private int active;
     private String remarks;

    public Discount() {
    }
    public Discount(Integer discountId) {
        this.discountId=discountId;
    }
    public Discount(int minimumBookings, int maximumBookings, int percentToDiscount, int active,String remarks) {
       this.minimumBookings = minimumBookings;
       this.maximumBookings = maximumBookings;
       this.percentToDiscount = percentToDiscount;
       this.active = active;
       this.remarks=remarks;
    }
   
    @Id @GeneratedValue(strategy=IDENTITY)
    @Column(name="discount_id", unique=true, nullable=false)
    public Integer getDiscountId() {
        return this.discountId;
    }
    
    public void setDiscountId(Integer discountId) {
        this.discountId = discountId;
    }

    
    @Column(name="minimum_bookings", nullable=false)
    public int getMinimumBookings() {
        return this.minimumBookings;
    }
    
    public void setMinimumBookings(int minimumBookings) {
        this.minimumBookings = minimumBookings;
    }

    
    @Column(name="maximum_bookings", nullable=false)
    public int getMaximumBookings() {
        return this.maximumBookings;
    }
    
    public void setMaximumBookings(int maximumBookings) {
        this.maximumBookings = maximumBookings;
    }

    
    @Column(name="percent_to_discount", nullable=false)
    public int getPercentToDiscount() {
        return this.percentToDiscount;
    }
    
    public void setPercentToDiscount(int percentToDiscount) {
        this.percentToDiscount = percentToDiscount;
    }

    
    @Column(name="active", nullable=false)
    public int getActive() {
        return this.active;
    }
    
    public void setActive(int active) {
        this.active = active;
    }

    @Column(name="remarks")
    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Override
    public String toString() {
        return "Discount{" +
                "discountId=" + discountId +
                ", minimumBookings=" + minimumBookings +
                ", maximumBookings=" + maximumBookings +
                ", percentToDiscount=" + percentToDiscount +
                ", active=" + active +
                ", remarks='" + remarks + '\'' +
                '}';
    }

    @Override
    @Transient
    public Integer getId() {
        return discountId;
    }
}


