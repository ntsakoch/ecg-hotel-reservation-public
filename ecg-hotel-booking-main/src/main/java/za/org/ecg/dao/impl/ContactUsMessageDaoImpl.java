package za.org.ecg.dao.impl;

import org.springframework.stereotype.Repository;
import za.org.ecg.dao.ContactUsMessageDao;
import za.org.ecg.dao.entity.ContactUsMessage;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by F4817273 on 2016/09/22.
 */
@Repository("contactUsMessageDao")
public class ContactUsMessageDaoImpl extends AbstractDao<ContactUsMessage> implements ContactUsMessageDao {

    public ContactUsMessageDaoImpl() {
        super(ContactUsMessage.class);
    }

    @Override
    public List<ContactUsMessage> findByUserId(int userId) throws EcgDAOException {
        Map<String,Object> params=new HashMap<>();
        params.put("userId",userId);
        return  findByNamedQuery("ContactUsMessage.findByUserId",params);
    }

    @Override
    public List<ContactUsMessage> findByStatus(String status) throws EcgDAOException {
        Map<String,Object> params=new HashMap<>();
        params.put("status",status);
        return  findByNamedQuery("ContactUsMessage.findByStatus",params);
    }

    @Override
    public List<ContactUsMessage> findByFullName(String fullName) throws EcgDAOException {
        Map<String,Object> params=new HashMap<>();
        params.put("senderFullname",fullName);
        return  findByNamedQuery("ContactUsMessage.findBySenderFullname",params);
        }

    @Override
    public List<ContactUsMessage> findByDateRange(Date fromDate, Date toDate) throws EcgDAOException {
        Map<String,Object> params=new HashMap<>();
        params.put("fromDate",fromDate);
        params.put("toDate",toDate);
        return  findByNamedQuery("ContactUsMessage.findByDateSentRange",params);
    }
}
