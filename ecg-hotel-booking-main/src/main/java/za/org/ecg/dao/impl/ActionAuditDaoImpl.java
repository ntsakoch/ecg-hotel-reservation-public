package za.org.ecg.dao.impl;

import org.springframework.stereotype.Repository;
import za.org.ecg.dao.ActionAuditDao;
import za.org.ecg.dao.entity.ActionAudit;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/21.
 */
@Repository("actionAuditDao")
public class ActionAuditDaoImpl extends AbstractDao<ActionAudit> implements ActionAuditDao {

    @Override
    public List<ActionAudit> findActionsByUser(int userId) throws EcgDAOException {
        Map<String,Object> params=new HashMap<>();
        params.put("userId",userId);
        return  findByNamedQuery("ActionAudit.findByUserId",params);
    }

    @Override
    public List<ActionAudit> findActionsByTime(Date startTime, Date endTime) throws EcgDAOException {
        Map<String,Object> params=new HashMap<>();
        params.put("startTime",startTime);
        params.put("endTime",endTime);
        return  findByNamedQuery("ActionAudit.findByStartTime",params);
    }

    @Override
    public List<ActionAudit> findActionsByType(String type) throws EcgDAOException {
        Map<String,Object> params=new HashMap<>();
        params.put("actionType",type);
        return  findByNamedQuery("ActionAudit.findByActionType",params);
    }

    @Override
    public List<ActionAudit> findActionsBySystemProcessName(String processName) throws EcgDAOException {
        Map<String,Object> params=new HashMap<>();
        params.put("systemProcessName",processName);
        return  findByNamedQuery("ActionAudit.findBySystemProcessName",params);
    }
}
