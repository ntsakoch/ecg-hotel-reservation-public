/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.org.ecg.dao.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import static za.org.ecg.dao.entity.EntityBean.CATALOG;

/**
 *
 * @author F4817273
 */
@Entity
@Table(name = "postal_addresses", catalog = CATALOG)
public class PostalAddress implements EntityBean {

    private static final long serialVersionUID = 1L;
    private Integer postalAddressId;
    private String address;
    private String city;
    private String postalCode;
    private User userInfo;
    private Integer userId;


    public PostalAddress(Integer postalAddressId) {
        this.postalAddressId = postalAddressId;
    }

    public PostalAddress() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "postal_address_id")
    public Integer getPostalAddressId() {
        return postalAddressId;
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    @NotFound(action = NotFoundAction.EXCEPTION)
    @JsonIgnore
    public User getUserInfo() {
        return userInfo;
    } 
    
    public void setUserInfo(User userInfo) {
        this.userInfo = userInfo;
    }
    
    public void setPostalAddressId(Integer id) {
        this.postalAddressId = id;
    }

    @Column(name = "user_id",insertable = false,updatable = false)
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Column(name = "address")
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Column(name = "city")
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Column(name = "code")
    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (postalAddressId != null ? postalAddressId.hashCode() : super.hashCode());
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PostalAddress)) {
            return false;
        }
        PostalAddress other = (PostalAddress) object;
        if ((this.postalAddressId == null && other.postalAddressId != null) || (this.postalAddressId != null && !this.postalAddressId.equals(other.postalAddressId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "PostalAddress{" +
                "postalAddressId=" + postalAddressId +
                ", address='" + address + '\'' +
                ", city='" + city + '\'' +
                ", postalCode='" + postalCode + '\'' +
                '}';
    }
}
