package za.org.ecg.dao.entity;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import za.org.ecg.dao.cache.EnableEntityCaching;
import za.org.ecg.dao.impl.RoomFeatureDaoImpl;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;
import static za.org.ecg.dao.entity.EntityBean.CATALOG;

/**
 *
 * @author F4817273
 */
@Entity
@Table(name="room_features"
    ,catalog=CATALOG
)
@EnableEntityCaching(daoImplementer = RoomFeatureDaoImpl.class)
public class RoomFeature implements EntityBean {

     private Integer roomFeatureId;
     private int featureId;
     private int roomId;
     private Feature feature;

    public RoomFeature() {
    }
    public RoomFeature(Integer roomFeatureId) {
        this.roomFeatureId = roomFeatureId;
    }
    public RoomFeature(int featureId, int roomId) {
       this.featureId = featureId;
       this.roomId = roomId;
    }
   
    @Id @GeneratedValue(strategy=IDENTITY)
    @Column(name="room_feature_id", unique=true, nullable=false)
    public Integer getRoomFeatureId() {
        return this.roomFeatureId;
    }
    
    public void setRoomFeatureId(Integer roomFeatureId) {
        this.roomFeatureId = roomFeatureId;
    }

    
    @Column(name="feature_id", nullable=false,insertable = false,updatable = false)
    public int getFeatureId() {
        return this.featureId;
    }
    
    public void setFeatureId(int featureId) {
        this.featureId = featureId;
    }

    
    @Column(name="room_id", nullable=false)
    public int getRoomId() {
        return this.roomId;
    }
    
    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "feature_id")
    @NotFound(action = NotFoundAction.EXCEPTION)
    public Feature getFeature() {
        return feature;
    }

    public void setFeature(Feature feature) {
        this.feature = feature;
    }

    @Override
    public String toString() {
        return "RoomFeature{" +
                "roomFeatureId=" + roomFeatureId +
                ", featureId=" + featureId +
                ", roomId=" + roomId +
                '}';
    }

    @Override
    @Transient
    public Integer getId() {
        return roomFeatureId;
    }
}


