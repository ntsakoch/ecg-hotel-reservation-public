package za.org.ecg.dao;

import za.org.ecg.dao.entity.RoomDefaultPreview;

/**
 * Created by F4817273 on 2016/09/22.
 */
public interface RoomDefaultPreviewDao extends GenericDao<RoomDefaultPreview> {
}
