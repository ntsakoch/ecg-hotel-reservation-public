/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package za.org.ecg.dao.impl;

import org.springframework.stereotype.Repository;
import za.org.ecg.dao.PostalAddressDao;
import za.org.ecg.dao.entity.PostalAddress;

/**
 *
 * @author F4817273
 */
@Repository("postalAddressDao")
public class PostalAddressDaoImpl extends AbstractDao<PostalAddress> implements PostalAddressDao {

    public PostalAddressDaoImpl() {
        super(PostalAddress.class);
    }

}
