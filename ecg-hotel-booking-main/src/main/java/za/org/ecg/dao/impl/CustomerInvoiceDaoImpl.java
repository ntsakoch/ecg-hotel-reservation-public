package za.org.ecg.dao.impl;

import org.springframework.stereotype.Repository;
import za.org.ecg.dao.CustomerInvoiceDao;
import za.org.ecg.dao.entity.CustomerInvoice;

/**
 * Created by root on 2016/09/28.
 */
@Repository("customerInvoiceDao")
public class CustomerInvoiceDaoImpl extends AbstractDao<CustomerInvoice> implements CustomerInvoiceDao {

    public CustomerInvoiceDaoImpl() {
        super(CustomerInvoice.class);
    }
}
