package za.org.ecg.dao.entity;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import za.org.ecg.dao.cache.EnableEntityCaching;
import za.org.ecg.dao.impl.RoomDaoImpl;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import static javax.persistence.GenerationType.IDENTITY;
import static za.org.ecg.dao.entity.EntityBean.CATALOG;

import java.util.Set;

/**
 *
 * @author F4817273
 */
@Entity
@Table(name="rooms"
    ,catalog=CATALOG
)
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "Room.findAll", query = "SELECT r FROM Room r"),
        @NamedQuery(name = "Room.findByDimension", query = "SELECT r FROM Room r WHERE r.dimension = :dimension"),
        @NamedQuery(name = "Room.findByCapacity", query = "SELECT r FROM Room r WHERE r.capacity = :capacity"),
        @NamedQuery(name = "Room.findTopListed", query = "SELECT r FROM Room r WHERE r.topListed = 1"),
        @NamedQuery(name = "Room.findOnSpecial", query = "SELECT r FROM Room r WHERE r.onSpecial = 1")})
@EnableEntityCaching(daoImplementer = RoomDaoImpl.class)
public class Room implements EntityBean {

     private Integer roomId;
     private int roomDefaultPreviewId;
     private int costId;
     private int adultCostId;
     private int childrenCostId;
     private int roomNumber;
     private Cost roomCost;
     private Cost adultCost;
     private Cost childrenCost;
     private String dimension;
     private boolean topListed;
     private boolean onSpecial;
     private int capacity;
     private String size;
     private int maxNumberOfChildren;
     private int maxNumberOfAdults;
     private RoomDefaultPreview defaultPreview;
     private Set<RoomFeature> features;
     private Set<RoomGallery> gallery;
     private RoomLocation roomLocation;

    public Room() {
    }

    public Room(Integer roomId) {
        this.roomId = roomId;
    }
    public Room(int roomDefaultPreviewId, String dimension, int capacity) {
       this.roomDefaultPreviewId = roomDefaultPreviewId;
       this.dimension = dimension;
       this.capacity = capacity;
    }
   
    @Id @GeneratedValue(strategy=IDENTITY)
    @Column(name="room_id", unique=true, nullable=false)
    public Integer getRoomId() {
        return this.roomId;
    }
    
    public void setRoomId(Integer roomId) {
        this.roomId = roomId;
    }

    @Column(name = "room_number")
    public int getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(int roomNumber) {
        this.roomNumber = roomNumber;
    }

    @Column(name="room_default_preview_id", nullable=false,insertable = false,updatable = false)
    public int getRoomDefaultPreviewId() {
        return this.roomDefaultPreviewId;
    }
    
    public void setRoomDefaultPreviewId(int roomDefaultPreviewId) {
        this.roomDefaultPreviewId = roomDefaultPreviewId;
    }

    @Column(name="room_cost_id", nullable=false,insertable = false,updatable = false)
    public int getCostId() {
        return costId;
    }

    public void setCostId(int costId) {
        this.costId = costId;
    }

    @Column(name="adult_cost_id", nullable=false,insertable = false,updatable = false)
    public int getAdultCostId() {
        return adultCostId;
    }

    public void setAdultCostId(int adultCostId) {
        this.adultCostId = adultCostId;
    }

    @Column(name="children_cost_id", nullable=false,insertable = false,updatable = false)
    public int getChildrenCostId() {
        return childrenCostId;
    }

    public void setChildrenCostId(int childrenCostId) {
        this.childrenCostId = childrenCostId;
    }

    @Column(name="dimension", nullable=false, length=100)
    public String getDimension() {
        return this.dimension;
    }
    
    public void setDimension(String dimension) {
        this.dimension = dimension;
    }

    
    @Column(name="capacity", nullable=false)
    public int getCapacity() {
        return this.capacity;
    }
    
    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }


    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "room_default_preview_id")
    @NotFound(action = NotFoundAction.IGNORE)
    @Fetch(value = FetchMode.JOIN)
    public RoomDefaultPreview getDefaultPreview() {
        return defaultPreview;
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "room_id")
    @NotFound(action = NotFoundAction.IGNORE)
    @Fetch(value = FetchMode.JOIN)
    public RoomLocation getRoomLocation() {
        return roomLocation;
    }

    public void setRoomLocation(RoomLocation roomLocation) {
        this.roomLocation = roomLocation;
    }

    public void setDefaultPreview(RoomDefaultPreview defaultPreview) {
        this.defaultPreview = defaultPreview;
    }

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "room_id")
    public Set<RoomFeature> getFeatures() {
        return features;
    }

    public void setFeatures(Set<RoomFeature> features) {
        this.features = features;
    }

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "room_id")
    @Fetch(value = FetchMode.SUBSELECT)
    public Set<RoomGallery> getGallery() {
        return gallery;
    }

    public void setGallery(Set<RoomGallery> gallery) {
        this.gallery = gallery;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "room_cost_id")
    @NotFound(action = NotFoundAction.EXCEPTION)
    public Cost getRoomCost() {
        return roomCost;
    }

    public void setRoomCost(Cost roomCost) {
        this.roomCost = roomCost;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "adult_cost_id")
    @NotFound(action = NotFoundAction.EXCEPTION)
    public Cost getAdultCost() {
        return adultCost;
    }

    public void setAdultCost(Cost adultCost) {
        this.adultCost = adultCost;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "children_cost_id")
    @NotFound(action = NotFoundAction.EXCEPTION)
    public Cost getChildrenCost() {
        return childrenCost;
    }

    public void setChildrenCost(Cost childrenCost) {
        this.childrenCost = childrenCost;
    }

    @Column(name = "is_top_listed")
    public boolean isTopListed() {
        return topListed;
    }

    public void setTopListed(boolean topListed) {
        this.topListed = topListed;
    }

    @Column(name = "is_on_special")
    public boolean isOnSpecial() {
        return onSpecial;
    }

    public void setOnSpecial(boolean onSpecial) {
        this.onSpecial = onSpecial;
    }

    @Column(name = "size")
    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    @Column(name = "max_num_children")
    public int getMaxNumberOfChildren() {
        return maxNumberOfChildren;
    }

    public void setMaxNumberOfChildren(int maxNumberOfChildren) {
        this.maxNumberOfChildren = maxNumberOfChildren;
    }

    @Column(name = "max_num_adults")
    public int getMaxNumberOfAdults() {
        return maxNumberOfAdults;
    }

    public void setMaxNumberOfAdults(int maxNumberOfAdults) {
        this.maxNumberOfAdults = maxNumberOfAdults;
    }

    @Override
    public String toString() {
        return "Room{" +
                "roomId=" + roomId +
                ", roomDefaultPreviewId=" + roomDefaultPreviewId +
                ", costId=" + costId +
                ", adultCostId=" + adultCostId +
                ", childrenCostId=" + childrenCostId +
                ", roomNumber=" + roomNumber +
                ", roomCost=" + roomCost +
                ", adultCost=" + adultCost +
                ", childrenCost=" + childrenCost +
                ", dimension='" + dimension + '\'' +
                ", topListed=" + topListed +
                ", onSpecial=" + onSpecial +
                ", capacity=" + capacity +
                ", size='" + size + '\'' +
                ", maxNumberOfChildren=" + maxNumberOfChildren +
                ", maxNumberOfAdults=" + maxNumberOfAdults +
                '}';
    }

    @Override
    @Transient
    public Integer getId() {
        return roomId;
    }
}


