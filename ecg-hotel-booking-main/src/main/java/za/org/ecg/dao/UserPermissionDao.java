package za.org.ecg.dao;

import za.org.ecg.dao.entity.UserPermission;

/**
 * Created by F4817273 on 2016/09/15.
 */
public interface UserPermissionDao extends GenericDao<UserPermission> {
}
