/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.org.ecg.dao.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.*;
import za.org.ecg.dao.cache.EnableEntityCaching;
import za.org.ecg.dao.impl.UserDaoImpl;

import java.util.Date;
import java.util.Set;
import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import static za.org.ecg.dao.entity.EntityBean.CATALOG;

/**
 *
 * @author
 */
@Entity
@Table(name = "users", catalog = CATALOG)
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "User.findAll", query = "SELECT u FROM User u"),
        @NamedQuery(name = "User.findByUserId", query = "SELECT u FROM User u WHERE u.userId = :userId"),
        @NamedQuery(name = "User.findByFirstName", query = "SELECT u FROM User u WHERE u.firstName = :firstName"),
        @NamedQuery(name = "User.findByLastName", query = "SELECT u FROM User u WHERE u.lastName = :lastName"),
        @NamedQuery(name = "User.findByDob", query = "SELECT u FROM User u WHERE u.dateOfBirth = :dateOfBirth"),
        @NamedQuery(name = "User.findByIdentityNo", query = "SELECT u FROM User u WHERE u.identityNumber = :identityNo"),
        @NamedQuery(name = "User.findByIdentityType", query = "SELECT u FROM User u WHERE u.identityType = :identityType"),
        @NamedQuery(name = "User.findByContactNo", query = "SELECT u FROM User u WHERE u.contactNumber = :contactNumber"),
        @NamedQuery(name = "User.findByEmailAddress", query = "SELECT u FROM User u WHERE u.emailAddress = :emailAddress"),
        @NamedQuery(name = "User.findByCitizenship", query = "SELECT u FROM User u WHERE u.citizenship = :citizenship")})
//@EnableEntityCaching(daoImplementer = UserDaoImpl.class)
public class User implements  EntityBean {

    private static final long serialVersionUID = 1L;

    private Integer userId;
    private String firstName;
    private String lastName;
    private Date dateOfBirth;
    private String identityNumber;
    private String identityType;
    private String citizenship;
    private String contactNumber;
    private String emailAddress;
    private PostalAddress postalAddress;
    private PhysicalAddress physicalAddress;
    private UserCredential userCredential;
    private Set<UserPermission> userPermissions;
    

    public User(Integer id, String firstName, String lastName, Date dateOfBirth, String identityNumber, String identityType, String citizenship, String contactNumber, String emailAddress) {
        this.userId = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.identityNumber = identityNumber;
        this.identityType = identityType;
        this.citizenship = citizenship;
        this.contactNumber = contactNumber;
        this.emailAddress = emailAddress;
    }

    public User() {
    }

    public User(Integer userId) {
        this.userId = userId;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_id")
    public Integer getUserId() {
        return userId;
    }


    public void setUserId(Integer id) {
        this.userId = id;
    }

    @Override
    @Transient
    public Integer getId()
    {
        return userId;
    }

    @Column(name = "first_name")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Column(name = "last_name")
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Column(name = "dob")
    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    @Column(name = "identity_no")
    public String getIdentityNumber() {
        return identityNumber;
    }

    public void setIdentityNumber(String identityNumber) {
        this.identityNumber = identityNumber;
    }

    @Column(name = "identity_type")
    public String getIdentityType() {
        return identityType;
    }

    public void setIdentityType(String identityType) {
        this.identityType = identityType;
    }

    @Column(name = "citizenship")
    public String getCitizenship() {
        return citizenship;
    }

    public void setCitizenship(String citizenship) {
        this.citizenship = citizenship;
    }

    @Column(name = "contact_no")
    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    @Column(name = "email_address")
    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    @OneToOne(fetch = FetchType.EAGER, mappedBy = "userInfo", cascade = CascadeType.ALL,orphanRemoval = true)
    @NotFound(action = NotFoundAction.IGNORE)
    @Fetch(value = FetchMode.JOIN)
    public PostalAddress getPostalAddress() {
        return postalAddress;
    }

    public void setPostalAddress(PostalAddress postalAddress) {
        this.postalAddress = postalAddress;
    }
    
    @OneToOne(fetch = FetchType.EAGER, mappedBy = "userInfo", cascade = CascadeType.ALL,orphanRemoval = true)
    @NotFound(action = NotFoundAction.IGNORE)
    @Fetch(value = FetchMode.JOIN)
    public PhysicalAddress getPhysicalAddress() {
        return physicalAddress;
    }

    public void setPhysicalAddress(PhysicalAddress physicalAddress) {
        this.physicalAddress = physicalAddress;
    }

    @OneToMany(fetch = FetchType.EAGER,mappedBy = "userInfo",cascade ={ CascadeType.ALL,CascadeType.REMOVE},orphanRemoval = true)
    @Fetch(value = FetchMode.SUBSELECT)
      public Set<UserPermission> getUserPermissions()
    {
        return this.userPermissions;
    }

    public void setUserPermissions(Set<UserPermission> permissions)
    {
        this.userPermissions=permissions;
    }

    @OneToOne(fetch = FetchType.LAZY,mappedBy = "userInfo",cascade = CascadeType.ALL,orphanRemoval = true)
    @JoinColumn(name = "user_id")
    @NotFound(action = NotFoundAction.IGNORE)
    @Fetch(value = FetchMode.JOIN)
    @JsonIgnore
    public UserCredential getUserCredential() {
        return userCredential;
    }

    public void setUserCredential(UserCredential userCredential) {
        this.userCredential = userCredential;
    }

 /*   byte[] a = new byte[2];
    byte[] b = new byte[3];
    public int hashCode() {
        // assume that both a and b are sorted
        return a[0] + powerOf52(a[1], 1) + powerOf52(b[0], 2) + powerOf52(b[1], 3) + powerOf52(b[2], 4);
    }

    public static int powerOf52(byte b, int power) {
        int result = b;
        for (int i = 0; i < power; i++) {
            result *= 52;
        }
        return result;
    }*/

    @Override
    public String toString() {
        return "User{"
                + "userId='" + userId + '\''
                + ", firstName='" + firstName + '\''
                + ", lastName='" + lastName + '\''
                + ", identityNumber='" + identityNumber + '\''
                + ", identityType='" + identityType + '\''
                + ", citizenship='" + citizenship + '\''
                + ", contactNumber='" + contactNumber + '\''
                + ", emailAddress='" + emailAddress + '\''
                + ", dateOfBirth=" + dateOfBirth
                + '}';
    }

}
