package za.org.ecg.dao.entity;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import java.io.Serializable;

import static javax.persistence.GenerationType.IDENTITY;
import static za.org.ecg.dao.entity.EntityBean.CATALOG;


/**
 *
 * @author F4817273
 */

@Entity
@Table(name="customer_discounts"
    ,catalog=CATALOG
)
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "CustomerDiscount.findByCustomerInvoiceId", query = "SELECT c FROM CustomerDiscount c WHERE c.customerInvoiceId = :customerInvoiceId"),
        @NamedQuery(name = "CustomerDiscount.findByDiscountId", query = "SELECT c FROM CustomerDiscount c WHERE c.discountId = :discountId")})

public class CustomerDiscount  implements EntityBean {

     private Integer customerDiscountId;
     private int customerInvoiceId;
     private int discountId;
     private Discount discountDetails;

    public CustomerDiscount() {
    }
    public CustomerDiscount(Integer customerDiscountId) {
        this.customerDiscountId=customerDiscountId;
    }
    public CustomerDiscount(int customerInvoiceId, int discountId) {
       this.customerInvoiceId = customerInvoiceId;
       this.discountId = discountId;
    }
   
    @Id @GeneratedValue(strategy=IDENTITY)
    @Column(name="customer_discount_id", unique=true, nullable=false)
    public Integer getCustomerDiscountId() {
        return this.customerDiscountId;
    }
    
    public void setCustomerDiscountId(Integer customerDiscountId) {
        this.customerDiscountId = customerDiscountId;
    }

    @Column(name="customer_invoice_id", nullable=false)
    public int getCustomerInvoiceId() {
        return this.customerInvoiceId;
    }
    
    public void setCustomerInvoiceId(int customerInvoiceId) {
        this.customerInvoiceId = customerInvoiceId;
    }

    
    @Column(name="discount_id", nullable=false,insertable = false,updatable = false)
    public int getDiscountId() {
        return this.discountId;
    }
    
    public void setDiscountId(int discountId) {
        this.discountId = discountId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "discount_id")
    @NotFound(action = NotFoundAction.EXCEPTION)
    public Discount getDiscountDetatils() {
        return discountDetails;
    }

    public void setDiscountDetatils(Discount discountDetails) {
        this.discountDetails = discountDetails;
    }
}


