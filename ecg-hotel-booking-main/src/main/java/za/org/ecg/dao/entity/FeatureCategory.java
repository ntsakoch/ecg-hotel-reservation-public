package za.org.ecg.dao.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import static javax.persistence.GenerationType.IDENTITY;
import static za.org.ecg.dao.entity.EntityBean.CATALOG;

/**
 *
 * @author F4817273
 */

@Entity
@Table(name="feature_categories"
    ,catalog=CATALOG
)
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "FeatureCategory.findAll", query = "SELECT f FROM FeatureCategory f"),
        @NamedQuery(name = "FeatureCategory.findByCategoryCode", query = "SELECT f FROM FeatureCategory f WHERE f.categoryCode = :categoryCode"),
        @NamedQuery(name = "FeatureCategory.findByCategoryName", query = "SELECT f FROM FeatureCategory f WHERE f.categoryName = :categoryName")})

public class FeatureCategory implements EntityBean {

     private Integer featureCategoryId;
     private String categoryCode;
     private String categoryName;

    public FeatureCategory() {
    }
    public FeatureCategory(Integer featureCategoryId) {
        this.featureCategoryId = featureCategoryId;
    }

    public FeatureCategory(String categoryCode, String categoryName) {
       this.categoryCode = categoryCode;
       this.categoryName = categoryName;
    }
   
    @Id @GeneratedValue(strategy=IDENTITY)
    @Column(name="feature_category_id", unique=true, nullable=false)
    public Integer getFeatureCategoryId() {
        return this.featureCategoryId;
    }
    
    public void setFeatureCategoryId(Integer featureCategoryId) {
        this.featureCategoryId = featureCategoryId;
    }

    
    @Column(name="category_code", nullable=false, length=100)
    public String getCategoryCode() {
        return this.categoryCode;
    }
    
    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    
    @Column(name="category_name", nullable=false, length=100)
    public String getCategoryName() {
        return this.categoryName;
    }
    
    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    @Override
    public String toString() {
        return "FeatureCategory{" +
                "featureCategoryId=" + featureCategoryId +
                ", categoryCode='" + categoryCode + '\'' +
                ", categoryName='" + categoryName + '\'' +
                '}';
    }

    @Override
    @Transient
    public Integer getId() {
        return featureCategoryId;
    }
}


