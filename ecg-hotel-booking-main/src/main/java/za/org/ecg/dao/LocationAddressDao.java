package za.org.ecg.dao;

import za.org.ecg.dao.entity.LocationAddress;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.List;

/**
 * Created by Ntsako on 2017/02/21.
 */
public interface LocationAddressDao extends  GenericDao<LocationAddress> {

    List<LocationAddress> findBySuburbName(String suburbName) throws EcgDAOException;
}
