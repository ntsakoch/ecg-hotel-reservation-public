package za.org.ecg.dao.impl;

import org.springframework.stereotype.Repository;
import za.org.ecg.dao.CostDao;
import za.org.ecg.dao.entity.Cost;

/**
 * Created by Ntsako on 2017/01/18.
 */
@Repository("costDao")
public class CostDaoImpl extends AbstractDao<Cost> implements CostDao {
}
