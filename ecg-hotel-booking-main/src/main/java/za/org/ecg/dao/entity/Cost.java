package za.org.ecg.dao.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import za.org.ecg.dao.cache.EnableEntityCaching;
import za.org.ecg.dao.impl.CostDaoImpl;

import javax.persistence.*;

import static za.org.ecg.dao.entity.EntityBean.CATALOG;

/**
 * Created by Ntsako on 2017/01/18.
 */
@Entity
@Table(name = "costs", catalog = CATALOG)
@EnableEntityCaching(daoImplementer = CostDaoImpl.class)
public class Cost implements EntityBean{

    private int costId;
    private int currencyId;
    private double amount;
    private Currency currency;
    private String unitCode;

    public Cost() {
    }

    public Cost(int costId) {
        this.costId = costId;
    }

    public Cost(int costId, int currencyId, double amount) {
        this.costId = costId;
        this.currencyId = currencyId;
        this.amount = amount;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "cost_id")
    public int getCostId() {
        return costId;
    }

    public void setCostId(int costId) {
        this.costId = costId;
    }

    @Column(name = "currency_id",updatable = false,insertable = false)
    public int getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(int currencyId) {
        this.currencyId = currencyId;
    }
    @Column(name = "amount")
    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    @Column(name = "unit_code", nullable = false)
    public String getUnitCode() {
        return unitCode;
    }

    public void setUnitCode(String unitCode) {
        this.unitCode = unitCode;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "currency_id")
    @NotFound(action = NotFoundAction.EXCEPTION)
    @JsonIgnore
    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    @Override
    public String toString() {
        return "Cost{" +
                "costId=" + costId +
                ", currencyId=" + currencyId +
                ", amount=" + amount +
                ", unitCode='" + unitCode + '\'' +
                '}';
    }

    @Override
    @Transient
    public Integer getId() {
        return costId;
    }
}
