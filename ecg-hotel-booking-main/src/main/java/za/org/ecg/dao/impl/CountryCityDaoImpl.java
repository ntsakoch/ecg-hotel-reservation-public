package za.org.ecg.dao.impl;

import org.springframework.stereotype.Repository;
import za.org.ecg.dao.CountryCityDao;
import za.org.ecg.dao.entity.CountryCity;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.List;

/**
 * Created by Ntsako on 2017/02/21.
 */
@Repository("countryCityDao")
public class CountryCityDaoImpl extends AbstractDao<CountryCity> implements CountryCityDao {
   private static final String FIND_BY_CITY_NAME_QUERY="SELECT country_city_id,'c_id' FROM country_cities WHERE city_name LIKE %s";
    @Override
    public List<CountryCity> findByCityName(String cityName) throws EcgDAOException {
        List<Object[]> result=executeSQLQuery(String.format(FIND_BY_CITY_NAME_QUERY,"'%"+cityName+"%'"));
        return findBySQLQueryResult(result,0);
    }
}
