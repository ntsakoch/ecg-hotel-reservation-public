package za.org.ecg.dao.impl;

import org.springframework.stereotype.Repository;
import za.org.ecg.dao.ContactUsMessageCategoryDao;
import za.org.ecg.dao.entity.ContactUsMessageCategory;

/**
 * Created by F4817273 on 2016/09/23.
 */
@Repository("contactUsMessageCategoryDao")
public class ContactUsMessageCategoryDaoImpl extends AbstractDao<ContactUsMessageCategory> implements ContactUsMessageCategoryDao {
    public ContactUsMessageCategoryDaoImpl() {
        super(ContactUsMessageCategory.class);
    }
}
