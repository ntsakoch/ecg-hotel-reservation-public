package za.org.ecg.dao.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import java.util.Date;

import static za.org.ecg.dao.entity.EntityBean.CATALOG;

/**
 * Created by Ntsako on 2017/01/21.
 */
@Entity
@Table(name = "action_audit", catalog = CATALOG)
@XmlRootElement
@NamedQueries({@NamedQuery(name = "ActionAudit.findByUserId", query = "SELECT a FROM ActionAudit a WHERE a.userId=:userId"),
                           @NamedQuery(name = "ActionAudit.findByStartTime", query = "SELECT a FROM ActionAudit a WHERE a.startTime BETWEEN :startTime AND :endTime"),
                           @NamedQuery(name = "ActionAudit.findByActionType", query = "SELECT a FROM ActionAudit a WHERE a.actionType=:actionType"),
                           @NamedQuery(name = "ActionAudit.findBySystemProcessName", query = "SELECT a FROM ActionAudit a WHERE a.systemProcessName=:systemProcessName")})
public class ActionAudit  implements EntityBean{

    private int actionAuditId;
    private int userId;
    private String actionType;
    private Date startTime;
    private Date endTime;
    private String sessionId;
    private boolean completed;
    private String actionResult;
    private String errorDescription;
    private String actionDescription;
    private String systemProcessName;

    public ActionAudit() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "action_audit_id")
    public int getActionAuditId() {
        return actionAuditId;
    }

    public void setActionAuditId(int actionAuditId) {
        this.actionAuditId = actionAuditId;
    }

    @Column(name = "user_id")
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Column(name = "action_type")
    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    @Column(name = "start_time")
    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    @Column(name = "end_time")
    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    @Column(name = "session_id")
    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    @Column(name = "completed")
    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    @Column(name = "action_result")
    public String getActionResult() {
        return actionResult;
    }

    public void setActionResult(String actionResult) {
        this.actionResult = actionResult;
    }

    @Column(name = "error_description")
    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    @Column(name = "action_description")
    public String getActionDescription() {
        return actionDescription;
    }

    public void setActionDescription(String actionDescription) {
        this.actionDescription = actionDescription;
    }

    @Column(name = "sys_process_name")
    public String getSystemProcessName() {
        return systemProcessName;
    }

    public void setSystemProcessName(String systemProcessName) {
        this.systemProcessName = systemProcessName;
    }

    @Override
    public String toString() {
        return "ActionAudit{" +
                "actionAuditId=" + actionAuditId +
                ", userId=" + userId +
                ", actionType='" + actionType + '\'' +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", sessionId='" + sessionId + '\'' +
                ", completed=" + completed +
                ", actionResult='" + actionResult + '\'' +
                ", errorDescription='" + errorDescription + '\'' +
                ", actionDescription='" + actionDescription + '\'' +
                ", systemProcessName='" + systemProcessName + '\'' +
                '}';
    }
}
