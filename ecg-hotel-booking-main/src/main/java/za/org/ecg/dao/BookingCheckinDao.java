package za.org.ecg.dao;

import za.org.ecg.dao.entity.BookingCheckin;

/**
 * Created by Ntsako on 2017/01/12.
 */
public interface BookingCheckinDao extends GenericDao<BookingCheckin> {
}
