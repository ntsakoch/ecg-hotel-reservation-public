/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package za.org.ecg.dao.impl;

import org.springframework.stereotype.Repository;
import za.org.ecg.dao.PhysicalAddressDao;
import za.org.ecg.dao.entity.PhysicalAddress;

/**
 *
 * @author F4817273
 */
@Repository("physicalAddressDao")
public class PhysicalAddressDaoImpl extends AbstractDao<PhysicalAddress> implements PhysicalAddressDao {
    public PhysicalAddressDaoImpl() {
        super(PhysicalAddress.class);
    }

}
