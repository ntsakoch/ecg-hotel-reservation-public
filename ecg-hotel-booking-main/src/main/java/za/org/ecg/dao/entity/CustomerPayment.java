package za.org.ecg.dao.entity;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import static javax.persistence.GenerationType.IDENTITY;
import static za.org.ecg.dao.entity.EntityBean.CATALOG;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author F4817273
 */

@Entity
@Table(name="customer_payments"
    ,catalog=CATALOG
)
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "CustomerPayment.findAll", query = "SELECT c FROM CustomerPayment c"),
        @NamedQuery(name = "CustomerPayment.findByPaymentId", query = "SELECT c FROM CustomerPayment c WHERE c.paymentId = :paymentId"),
        @NamedQuery(name = "CustomerPayment.findByCustomerAccountId", query = "SELECT c FROM CustomerPayment c WHERE c.customerAccountId = :customerAccountId"),
        @NamedQuery(name = "CustomerPayment.findByAmountPayed", query = "SELECT c FROM CustomerPayment c WHERE c.amountPayed = :amountPayed"),
        @NamedQuery(name = "CustomerPayment.findByDateReceived", query = "SELECT c FROM CustomerPayment c WHERE c.dateReceived = :dateReceived"),
        @NamedQuery(name = "CustomerPayment.findByDatePayed", query = "SELECT c FROM CustomerPayment c WHERE c.datePayed = :datePayed"),
        @NamedQuery(name = "CustomerPayment.findByAttachmentId", query = "SELECT c FROM CustomerPayment c WHERE c.attachmentId = :attachmentId")})

public class CustomerPayment  implements EntityBean {

     private Integer customerPaymentId;
     private int customerAccountId;
     private int invoiceId;
     private int currencyId;
     private Currency currency;
     private double amountPayed;
     private Date dateReceived;
     private Date datePayed;
     private int attachmentId;
     private CustomerAccount account;
     private Attachment attachment;
     private CustomerInvoice paymentInvoice;
     private String currencyCode;

    public CustomerPayment() {
    }
    public CustomerPayment(Integer paymentId) {
        this.customerPaymentId=paymentId;
    }
    public CustomerPayment(int customerAccountId, double amountPayed, Date dateReceived, Date datePayed, int attachmentId) {
       this.customerAccountId = customerAccountId;
       this.amountPayed = amountPayed;
       this.dateReceived = dateReceived;
       this.datePayed = datePayed;
       this.attachmentId = attachmentId;
    }
   
    @Id @GeneratedValue(strategy=IDENTITY)
    @Column(name="customer_payment_id", unique=true, nullable=false)
    public Integer getPaymentId() {
        return this.customerPaymentId;
    }
    
    public void setPaymentId(Integer paymentId) {
        this.customerPaymentId = paymentId;
    }

    
    @Column(name="customer_account_id", nullable=false,insertable = false,updatable = false)
    public int getCustomerAccountId() {
        return this.customerAccountId;
    }
    
    public void setCustomerAccountId(int customerAccountId) {
        this.customerAccountId = customerAccountId;
    }

    
    @Column(name="amount_payed", nullable=false, precision=8)
    public double getAmountPayed() {
        return this.amountPayed;
    }
    
    public void setAmountPayed(double amountPayed) {
        this.amountPayed = amountPayed;
    }

    
    @Column(name="date_received", nullable=false)
    public Date getDateReceived() {
        return this.dateReceived;
    }
    
    public void setDateReceived(Date dateReceived) {
        this.dateReceived = dateReceived;
    }

    
    @Column(name="date_payed", nullable=false)
    public Date getDatePayed() {
        return this.datePayed;
    }
    
    public void setDatePayed(Date datePayed) {
        this.datePayed = datePayed;
    }

    
    @Column(name="attachment_id", nullable=false,updatable = false,insertable = false)
    public int getAttachmentId() {
        return this.attachmentId;
    }
    
    public void setAttachmentId(int attachmentId) {
        this.attachmentId = attachmentId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customer_account_id")
    @NotFound(action = NotFoundAction.EXCEPTION)
    public CustomerAccount getAccount() {
        return account;
    }

    public void setAccount(CustomerAccount account) {
        this.account = account;
    }

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL,orphanRemoval = true)
    @JoinColumn(name = "attachment_id")
    @NotFound(action = NotFoundAction.IGNORE)
    @Fetch(value = FetchMode.JOIN)
    public Attachment getAttachment() {
        return attachment;
    }

    public void setAttachment(Attachment attachment) {
        this.attachment = attachment;
    }

    @Column(name="invoice_id", nullable=false,updatable = false,insertable = false)
    public int getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(int invoiceId) {
        this.invoiceId = invoiceId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "invoice_id")
    @NotFound(action = NotFoundAction.EXCEPTION)
    public CustomerInvoice getPaymentInvoice() {
        return paymentInvoice;
    }

    public void setPaymentInvoice(CustomerInvoice paymentInvoice) {
        this.paymentInvoice = paymentInvoice;
    }

    @Column(name = "currency_code")
    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    @Column(name="currency_id", nullable=false,updatable = false,insertable = false)
    public int getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(int currencyId) {
        this.currencyId = currencyId;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "currency_id")
    @Fetch(value = FetchMode.JOIN)
    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    @Override
    public String toString() {
        return "CustomerPayment{" +
                "customerPaymentId=" + customerPaymentId +
                ", customerAccountId=" + customerAccountId +
                ", invoiceId=" + invoiceId +
                ", amountPayed=" + amountPayed +
                ", dateReceived=" + dateReceived +
                ", datePayed=" + datePayed +
                ", attachmentId=" + attachmentId + '}';
    }
}


