package za.org.ecg.dao.entity;

import za.org.ecg.dao.ApplicationPropertyConfigDao;
import za.org.ecg.dao.cache.EnableEntityCaching;
import za.org.ecg.dao.impl.ApplicationPropertyCofigDaoImpl;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import static za.org.ecg.dao.entity.EntityBean.CATALOG;

/**
 * Created by Ntsako on 2016/12/23.
 */
@Entity
@Table(name = "application_properties_configs", catalog = CATALOG)
@XmlRootElement
@NamedQueries({})
@EnableEntityCaching(daoImplementer = ApplicationPropertyCofigDaoImpl.class)
public class ApplicationPropertyConfig implements EntityBean{
    private static final long serialVersionUID = 1L;

    private int applicationPropertyConfigId;
    private String configKey;
    private String configValue;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "application_property_config_id")
    public int getApplicationPropertyConfigId() {
        return applicationPropertyConfigId;
    }

    public void setApplicationPropertyConfigId(int applicationPropertyConfigId) {
        this.applicationPropertyConfigId = applicationPropertyConfigId;
    }

    @Column(name = "config_key")
    public String getConfigKey() {
        return configKey;
    }

    public void setConfigKey(String configKey) {
        this.configKey = configKey;
    }

    @Column(name = "config_value")
    public String getConfigValue() {
        return configValue;
    }

    public void setConfigValue(String configValue) {
        this.configValue = configValue;
    }

    @Override
    public String toString() {
        return "ApplicationPropertyConfig{" +
                "applicationPropertyConfigId=" + applicationPropertyConfigId +
                ", configKey='" + configKey + '\'' +
                ", configValue='" + configValue + '\'' +
                '}';
    }

    @Override
    @Transient
    public Integer getId() {
        return applicationPropertyConfigId;
    }
}
