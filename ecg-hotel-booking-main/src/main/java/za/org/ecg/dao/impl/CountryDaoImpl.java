package za.org.ecg.dao.impl;

import org.springframework.stereotype.Repository;
import za.org.ecg.dao.CostDao;
import za.org.ecg.dao.CountryDao;
import za.org.ecg.dao.entity.Country;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Ntsako on 2017/01/18.
 */
@Repository("countryDao")
public class CountryDaoImpl extends AbstractDao<Country> implements CountryDao{

    @Override
    public List<Country> findAll() throws EcgDAOException {
        return findByNamedQuery("Country.findAllActive",null);
    }

    @Override
    public Country findByCountryCode(String countryCode) throws EcgDAOException {
        Map<String,Object> params=new HashMap<>();
        params.put("countryCode",countryCode);
        List<Country> countries=findByNamedQuery("Country.findByCountryCode",params);
        return countries.isEmpty()?null:countries.get(0);
    }
}
