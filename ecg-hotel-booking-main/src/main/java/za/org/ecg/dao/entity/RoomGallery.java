package za.org.ecg.dao.entity;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import za.org.ecg.dao.cache.EnableEntityCaching;
import za.org.ecg.dao.impl.RoomGalleryDaoImpl;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;
import static za.org.ecg.dao.entity.EntityBean.CATALOG;

/**
 *
 * @author F4817273
 */

@Entity
@Table(name="room_galleries"
    ,catalog=CATALOG
)
@EnableEntityCaching(daoImplementer = RoomGalleryDaoImpl.class)
public class RoomGallery implements EntityBean {


     private Integer roomGalleryId;
     private int attachmentId;
     private int roomId;
     private Attachment attachment;

    public RoomGallery() {
    }

    public RoomGallery(Integer roomGalleryId) {
        this.roomGalleryId = roomGalleryId;
    }

    public RoomGallery(int attachementId, int roomId) {
       this.attachmentId = attachementId;
       this.roomId = roomId;
    }
   
    @Id @GeneratedValue(strategy=IDENTITY)
    @Column(name="room_gallery_id", unique=true, nullable=false)
    public Integer getRoomGalleryId() {
        return this.roomGalleryId;
    }
    
    public void setRoomGalleryId(Integer roomGalleryId) {
        this.roomGalleryId = roomGalleryId;
    }

    
    @Column(name="attachement_id", updatable = false,insertable = false)
    public int getAttachmentId() {
        return this.attachmentId;
    }
    
    public void setAttachmentId(int attachmentId) {
        this.attachmentId = attachmentId;
    }

    
    @Column(name="room_id", nullable=false)
    public int getRoomId() {
        return this.roomId;
    }
    
    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL,orphanRemoval = true)
    @JoinColumn(name = "attachement_id")
    @NotFound(action = NotFoundAction.IGNORE)
    @Fetch(value = FetchMode.JOIN)
    public Attachment getAttachment() {
        return attachment;
    }

    public void setAttachment(Attachment attachment) {
        this.attachment = attachment;
    }

    @Override
    public String toString() {
        return "RoomGallery{" +
                "roomGalleryId=" + roomGalleryId +
                ", attachmentId=" + attachmentId +
                ", roomId=" + roomId +
                '}';
    }

    @Override
    @Transient
    public Integer getId() {
        return roomGalleryId;
    }
}


