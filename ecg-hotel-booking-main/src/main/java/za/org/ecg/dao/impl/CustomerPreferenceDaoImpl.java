package za.org.ecg.dao.impl;

import org.springframework.stereotype.Repository;
import za.org.ecg.dao.CustomerPreferenceDao;
import za.org.ecg.dao.entity.CustomerPreference;

/**
 * Created by root on 2016/09/28.
 */
@Repository("customerPreferenceDao")
public class CustomerPreferenceDaoImpl extends AbstractDao<CustomerPreference> implements CustomerPreferenceDao {

    public CustomerPreferenceDaoImpl() {
        super(CustomerPreference.class);
    }
}
