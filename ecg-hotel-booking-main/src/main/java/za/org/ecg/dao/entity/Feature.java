package za.org.ecg.dao.entity;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import za.org.ecg.dao.cache.EnableEntityCaching;
import za.org.ecg.dao.impl.FeatureDaoImpl;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import static javax.persistence.GenerationType.IDENTITY;
import static za.org.ecg.dao.entity.EntityBean.CATALOG;


/**
 *
 * @author F4817273
 */

@Entity
@Table(name="features"
    ,catalog=CATALOG
)
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "Feature.findAll", query = "SELECT f FROM Feature f"),
        @NamedQuery(name = "Feature.findByName", query = "SELECT f FROM Feature f WHERE f.name = :name")})
@EnableEntityCaching(daoImplementer =FeatureDaoImpl.class)
public class Feature implements EntityBean {

     private Integer featureId;
     private int featureCategoryId;
     private int costId;
     private String name;
     private FeatureCategory category;
     private Cost featureCost;

    public Feature() {
    }
    public Feature(Integer featureId) {
        this.featureId = featureId;
    }
    public Feature(int featureCategoryId, String name) {
       this.featureCategoryId = featureCategoryId;
       this.name = name;
    }
   
    @Id @GeneratedValue(strategy=IDENTITY)
    @Column(name="feature_id", unique=true, nullable=false)
    public Integer getFeatureId() {
        return this.featureId;
    }
    
    public void setFeatureId(Integer featureId) {
        this.featureId = featureId;
    }

    
    @Column(name="feature_category_id", nullable=false,updatable = false,insertable = false)
    public int getFeatureCategoryId() {
        return this.featureCategoryId;
    }
    
    public void setFeatureCategoryId(int featureCategoryId) {
        this.featureCategoryId = featureCategoryId;
    }

    
    @Column(name="name", nullable=false, length=100)
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "cost_id",nullable = false,insertable = false,updatable = false)
    public int getCostId() {
        return costId;
    }

    public void setCostId(int costId) {
        this.costId = costId;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "cost_id")
    @NotFound(action = NotFoundAction.EXCEPTION)
    public Cost getFeatureCost() {
        return featureCost;
    }

    public void setFeatureCost(Cost featureCost) {
        this.featureCost = featureCost;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "feature_category_id")
    @NotFound(action = NotFoundAction.IGNORE)
    public FeatureCategory getCategory() {
        return category;
    }

    public void setCategory(FeatureCategory category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "Feature{" +
                "featureId=" + featureId +
                ", featureCategoryId=" + featureCategoryId +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    @Transient
    public Integer getId() {
        return featureId;
    }
}


