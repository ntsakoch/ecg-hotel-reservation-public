package za.org.ecg.dao.cache;

/**
 * Created by Ntsako on 2016/11/11.
 */
public class EntityCacheException extends Exception {
    public EntityCacheException(String message)
    {
        super(message);
    }
    public EntityCacheException(String message,Throwable cause)
    {
        super(message,cause);
    }
}
