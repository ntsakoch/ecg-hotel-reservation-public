package za.org.ecg.dao.impl;

import org.springframework.stereotype.Repository;
import za.org.ecg.dao.RoomDefaultPreviewDao;
import za.org.ecg.dao.entity.RoomDefaultPreview;

/**
 * Created by root on 2016/09/28.
 */
@Repository("roomDefaultPreviewDao")
public class RoomDefaultPreviewDaoImpl extends AbstractDao<RoomDefaultPreview> implements RoomDefaultPreviewDao {

    public RoomDefaultPreviewDaoImpl() {
        super(RoomDefaultPreview.class);
    }
}
