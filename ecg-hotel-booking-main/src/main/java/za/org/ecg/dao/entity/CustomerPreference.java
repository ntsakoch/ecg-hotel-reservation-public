package za.org.ecg.dao.entity;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import static javax.persistence.GenerationType.IDENTITY;
import static za.org.ecg.dao.entity.EntityBean.CATALOG;

import java.io.Serializable;

/**
 *
 * @author F4817273
 */

@Entity
@Table(name="customer_preferences"
    ,catalog=CATALOG
)
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "CustomerPreference.findAll", query = "SELECT c FROM CustomerPreference c"),
        @NamedQuery(name = "CustomerPreference.findByCustomerPreferenceId", query = "SELECT c FROM CustomerPreference c WHERE c.customerPreferenceId = :customerPreferenceId"),
        @NamedQuery(name = "CustomerPreference.findByCustomerBookingId", query = "SELECT c FROM CustomerPreference c WHERE c.customerBookingId = :customerBookingId"),
        @NamedQuery(name = "CustomerPreference.findByPreferenceId", query = "SELECT c FROM CustomerPreference c WHERE c.preferenceId = :preferenceId")})

public class CustomerPreference  implements EntityBean {


     private Integer customerPreferenceId;
     private int customerBookingId;
     private int preferenceId;
     private Preference preferenceDetails;

    public CustomerPreference() {
    }
    public CustomerPreference(Integer customerPreferenceId) {
        this.customerPreferenceId=customerPreferenceId;
    }

    public CustomerPreference(int customerId, int preferenceBookingId) {
       this.customerBookingId = customerBookingId;
       this.preferenceId = preferenceId;
    }
   
    @Id @GeneratedValue(strategy=IDENTITY)
    @Column(name="customer_preference_id", unique=true, nullable=false)
    public Integer getCustomerPreferenceId() {
        return this.customerPreferenceId;
    }
    
    public void setCustomerPreferenceId(Integer customerPreferenceId) {
        this.customerPreferenceId = customerPreferenceId;
    }

    
    @Column(name="customer_booking_id", nullable=false)
    public int getCustomerBookingId() {
        return this.customerBookingId;
    }
    
    public void setCustomerBookingId(int customerId) {
        this.customerBookingId = customerId;
    }

    
    @Column(name="preference_id", nullable=false,insertable = false,updatable = false)
    public int getPreferenceId() {
        return this.preferenceId;
    }
    
    public void setPreferenceId(int preferenceId) {
        this.preferenceId = preferenceId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "preference_id")
    @NotFound(action = NotFoundAction.IGNORE)
    public Preference getPreferenceDetails() {
        return preferenceDetails;
    }

    public void setPreferenceDetails(Preference preferenceDetails) {
        this.preferenceDetails = preferenceDetails;
    }
}


