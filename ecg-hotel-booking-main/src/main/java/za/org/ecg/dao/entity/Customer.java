/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.org.ecg.dao.entity;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import java.util.Set;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import static za.org.ecg.dao.entity.EntityBean.CATALOG;

/**
 *
 * @author F4817273
 */

@Entity
@Table(name = "customers", catalog = CATALOG)
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "Customer.findAll", query = "SELECT c FROM Customer c"),
        @NamedQuery(name = "Customer.findByCustomerId", query = "SELECT c FROM Customer c WHERE c.customerId = :customerId"),
        @NamedQuery(name = "Customer.findByUserId", query = "SELECT c FROM Customer c WHERE c.userId = :userId")})

public class Customer implements  EntityBean {

    private static final long serialVersionUID = 1L;

    private Integer customerId;
    private Integer userId;
    private User customerDetails;
    private CustomerAccount account;
    private Set<CustomerBooking> bookings;

    public Customer(Integer customerId) {
        this.customerId = customerId;
    }

    public Customer() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "customer_id")
    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL,orphanRemoval = true)
    @JoinColumn(name = "user_id")
    @NotFound(action = NotFoundAction.EXCEPTION)
    @Fetch(value = FetchMode.JOIN)
    public User getCustomerDetails() {
        return customerDetails;
    }

    public void setCustomerDetails(User customerDetails) {
        this.customerDetails = customerDetails;
    }

    @Column(name = "user_id", insertable = false, updatable = false)
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }


    @OneToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @JoinColumn(name = "customer_id")
    public Set<CustomerBooking> getBookings() {
        return bookings;
    }

    public void setBookings(Set<CustomerBooking> bookings) {
        this.bookings = bookings;
    }

    @OneToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "customer_id")
    @NotFound(action = NotFoundAction.IGNORE)
    public CustomerAccount getAccount() {
        return account;
    }

    public void setAccount(CustomerAccount account) {
        this.account = account;
    }


    @Override
    public String toString() {
        return "Customer{" +
                "customerId=" + customerId +
                ", customerDetails=" + customerDetails +
                '}';
    }

}
