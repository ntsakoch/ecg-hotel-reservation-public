package za.org.ecg.dao.impl;

import org.springframework.stereotype.Repository;
import za.org.ecg.dao.RoomGalleryDao;
import za.org.ecg.dao.entity.RoomGallery;

/**
 * Created by root on 2016/09/28.
 */
@Repository("roomGalleryDao")
public class RoomGalleryDaoImpl extends AbstractDao<RoomGallery> implements RoomGalleryDao {

    public RoomGalleryDaoImpl() {
        super(RoomGallery.class);
    }
}
