package za.org.ecg.dao;

import za.org.ecg.dao.entity.Language;

/**
 * Created by Ntsako on 2017/01/18.
 */
public interface LanguageDao extends GenericDao<Language> {
}
