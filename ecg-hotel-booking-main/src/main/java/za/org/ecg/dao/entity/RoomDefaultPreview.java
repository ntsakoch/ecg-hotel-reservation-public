package za.org.ecg.dao.entity;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import za.org.ecg.dao.cache.EnableEntityCaching;
import za.org.ecg.dao.impl.RoomDefaultPreviewDaoImpl;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;
import static za.org.ecg.dao.entity.EntityBean.CATALOG;

/**
 *
 * @author F4817273
 */
@Entity
@Table(name="room_default_previews"
    ,catalog=CATALOG
)
@EnableEntityCaching(daoImplementer = RoomDefaultPreviewDaoImpl.class)
public class RoomDefaultPreview implements EntityBean {

     private Integer roomDefaultPreviewId;
     private int attachmentId;
     private Attachment attachment;


    public RoomDefaultPreview() {
    }
    public RoomDefaultPreview(Integer roomDefaultPreviewId) {
        this.roomDefaultPreviewId = roomDefaultPreviewId;
    }
    public RoomDefaultPreview(int attachmentId) {
       this.attachmentId = attachmentId;
    }
   
    @Id @GeneratedValue(strategy=IDENTITY)
    @Column(name="room_default_preview_id", unique=true, nullable=false)
    public Integer getRoomDefaultPreviewId() {
        return this.roomDefaultPreviewId;
    }
    
    public void setRoomDefaultPreviewId(Integer roomDefaultPreviewId) {
        this.roomDefaultPreviewId = roomDefaultPreviewId;
    }

    
    @Column(name="attachment_id", nullable=false,updatable = false,insertable = false)
    public int getAttachmentId() {
        return this.attachmentId;
    }
    
    public void setAttachmentId(int attachmentId) {
        this.attachmentId = attachmentId;
    }

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL,orphanRemoval = true)
    @JoinColumn(name = "attachment_id")
    @NotFound(action = NotFoundAction.IGNORE)
    @Fetch(value = FetchMode.JOIN)
    public Attachment getAttachment() {
        return attachment;
    }

    public void setAttachment(Attachment attachment) {
        this.attachment = attachment;
    }

    @Override
    public String toString() {
        return "RoomDefaultPreview{" +
                "roomDefaultPreviewId=" + roomDefaultPreviewId +
                ", attachmentId=" + attachmentId +
                '}';
    }

    @Override
    @Transient
    public Integer getId() {
        return roomDefaultPreviewId;
    }
}


