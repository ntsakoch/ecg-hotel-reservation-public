package za.org.ecg.dao;

import za.org.ecg.dao.entity.PostalAddress;

/**
 * Created by f4817273 on 2016/09/05.
 */
public interface PostalAddressDao extends GenericDao<PostalAddress> {
}
