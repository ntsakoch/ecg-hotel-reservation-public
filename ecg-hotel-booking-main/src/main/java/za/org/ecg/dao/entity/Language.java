package za.org.ecg.dao.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import za.org.ecg.dao.cache.EnableEntityCaching;
import za.org.ecg.dao.impl.LanguageDaoImpl;

import javax.persistence.*;

import static za.org.ecg.dao.entity.EntityBean.CATALOG;

/**
 * Created by Ntsako on 2017/01/18.
 */
@Entity
@Table(name = "languages", catalog = CATALOG)
@EnableEntityCaching(daoImplementer = LanguageDaoImpl.class)
public class Language implements EntityBean{

    private int languageId;
    private int countryId;
    private String name;
    private String code;
    private Country country;

    public Language() {
    }

    public Language(int languageId) {
        this.languageId = languageId;
    }

    public Language(int languageId, int countryId, String name, String code) {
        this.languageId = languageId;
        this.countryId = countryId;
        this.name = name;
        this.code = code;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "language_id")
    public int getLanguageId() {
        return languageId;
    }

    public void setLanguageId(int languageId) {
        this.languageId = languageId;
    }

    @Column(name = "country_id",updatable = false,insertable = false)
    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "code")
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "country_id")
    @NotFound(action = NotFoundAction.EXCEPTION)
    @JsonIgnore
    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return "Language{" +
                "languageId=" + languageId +
                ", countryId=" + countryId +
                ", name='" + name + '\'' +
                ", code='" + code + '\'' +
                '}';
    }

    @Override
    @Transient
    public Integer getId() {
        return languageId;
    }
}
