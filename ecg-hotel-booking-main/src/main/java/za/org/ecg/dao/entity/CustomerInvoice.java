package za.org.ecg.dao.entity;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import static javax.persistence.GenerationType.IDENTITY;
import static za.org.ecg.dao.entity.EntityBean.CATALOG;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author F4817273
 */

@Entity
@Table(name="customer_invoices"
    ,catalog=CATALOG
)
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "CustomerInvoice.findByAttachmentId", query = "SELECT c FROM CustomerInvoice c WHERE c.attachmentId = :attachmentId"),
        @NamedQuery(name = "CustomerInvoice.findByTotalCost", query = "SELECT c FROM CustomerInvoice c WHERE c.totalCost = :totalCost"),
        @NamedQuery(name = "CustomerInvoice.findByDateCreated", query = "SELECT c FROM CustomerInvoice c WHERE c.dateCreated = :dateCreated"),
        @NamedQuery(name = "CustomerInvoice.findByBookingId", query = "SELECT c FROM CustomerInvoice c WHERE c.bookingId = :bookingId")})

public class CustomerInvoice  implements EntityBean {

     private Integer invoiceId;
     private int attachmentId;
     private int currencyId;
     private Currency currency;
     private double totalCost;
     private Date dateCreated;
     private int bookingId;
     private CustomerDiscount discount;
     private Attachment attachment;

    public CustomerInvoice() {
    }

    public CustomerInvoice(Integer invoiceId) {
        this.invoiceId=invoiceId;
    }
    public CustomerInvoice(int attachmentId, double totalCost, Date dateCreated, int bookingId) {
       this.attachmentId = attachmentId;
       this.totalCost = totalCost;
       this.dateCreated = dateCreated;
       this.bookingId = bookingId;
    }
   
    @Id @GeneratedValue(strategy=IDENTITY)
    @Column(name="invoice_id", unique=true, nullable=false)
    public Integer getInvoiceId() {
        return this.invoiceId;
    }
    
    public void setInvoiceId(Integer invoiceId) {
        this.invoiceId = invoiceId;
    }

    
    @Column(name="attachment_id", nullable=false)
    public int getAttachmentId() {
        return this.attachmentId;
    }
    
    public void setAttachmentId(int attachmentId) {
        this.attachmentId = attachmentId;
    }

    
    @Column(name="total_cost", nullable=false, precision=8)
    public double getTotalCost() {
        return this.totalCost;
    }
    
    public void setTotalCost(double totalCost) {
        this.totalCost = totalCost;
    }

    
    @Column(name="date_created", nullable=false)
    public Date getDateCreated() {
        return this.dateCreated;
    }
    
    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    
    @Column(name="booking_id", nullable=false)
    public int getBookingId() {
        return this.bookingId;
    }
    
    public void setBookingId(int bookingId) {
        this.bookingId = bookingId;
    }

    @OneToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL,orphanRemoval = true)
    @JoinColumn(name = "invoice_id")
    @NotFound(action = NotFoundAction.IGNORE)
    @Fetch(value = FetchMode.JOIN)
    public CustomerDiscount getDiscount() {
        return discount;
    }

    public void setDiscounts(CustomerDiscount discount) {
        this.discount = discount;
    }

    public void setDiscount(CustomerDiscount discount) {
        this.discount = discount;
    }

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL,orphanRemoval = true)
    @JoinColumn(name = "attachment_id",updatable = false,insertable = false)
    @NotFound(action = NotFoundAction.IGNORE)
    @Fetch(value = FetchMode.JOIN)
    public Attachment getAttachment() {
        return attachment;
    }

    public void setAttachment(Attachment attachment) {
        this.attachment = attachment;
    }

    @Column(name="currency_id", nullable=false,updatable = false,insertable = false)
    public int getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(int currencyId) {
        this.currencyId = currencyId;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "currency_id")
    @Fetch(value = FetchMode.JOIN)
    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    @Override
    public String toString() {
        return "CustomerInvoice{" +
                "invoiceId=" + invoiceId +
                ", attachmentId=" + attachmentId +
                ", totalCost=" + totalCost +
                ", dateCreated=" + dateCreated +
                ", bookingId=" + bookingId +
                ", discount=" + discount +
                ", attachment=" + attachment +
                '}';
    }
}


