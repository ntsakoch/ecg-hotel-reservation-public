package za.org.ecg.dao.impl;

import org.springframework.stereotype.Repository;
import za.org.ecg.dao.BookingCheckinDao;
import za.org.ecg.dao.entity.BookingCheckin;

/**
 * Created by Ntsako on 2017/01/12.
 */
@Repository("bookingCheckInDao")
public class BookingCheckinDaoImpl extends AbstractDao<BookingCheckin> implements BookingCheckinDao {
}
