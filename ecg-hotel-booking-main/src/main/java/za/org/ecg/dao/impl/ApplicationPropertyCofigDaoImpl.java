package za.org.ecg.dao.impl;

import org.springframework.stereotype.Repository;
import za.org.ecg.dao.ApplicationPropertyConfigDao;
import za.org.ecg.dao.entity.ApplicationPropertyConfig;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.List;

/**
 * Created by Ntsako on 2016/12/23.
 */
@Repository("applicationPropertyConfigDao")
public class ApplicationPropertyCofigDaoImpl  extends AbstractDao<ApplicationPropertyConfig> implements ApplicationPropertyConfigDao{

    @Override
    public List<ApplicationPropertyConfig> findAll() throws EcgDAOException {
        List<ApplicationPropertyConfig> applicationProperties=super.findAll();
        for(ApplicationPropertyConfig applicationProperty:applicationProperties)
        {
            System.getProperties().setProperty(applicationProperty.getConfigKey(),applicationProperty.getConfigValue());
        }
        return applicationProperties;
    }

    @Override
    public void create(ApplicationPropertyConfig entity) throws EcgDAOException {
        super.create(entity);
        System.getProperties().setProperty(entity.getConfigKey(),entity.getConfigValue());
    }

    @Override
    public void update(ApplicationPropertyConfig newValue) throws EcgDAOException {
        super.update(newValue);
        System.getProperties().setProperty(newValue.getConfigKey(),newValue.getConfigValue());
    }

    @Override
    public ApplicationPropertyConfig merge(ApplicationPropertyConfig entity) throws EcgDAOException {
        ApplicationPropertyConfig applicationPropertyConfig= super.merge(entity);
        System.getProperties().setProperty(entity.getConfigKey(),entity.getConfigValue());
        return applicationPropertyConfig;
    }

    @Override
    public Integer save(ApplicationPropertyConfig entity) throws EcgDAOException {
        Integer id= super.save(entity);
        System.getProperties().setProperty(entity.getConfigKey(),entity.getConfigValue());
        return  id;
    }

    @Override
    public void remove(ApplicationPropertyConfig entity) throws EcgDAOException {
        super.remove(entity);
        System.getProperties().remove(entity.getConfigKey());
    }

}
