/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package za.org.ecg.dao;

import za.org.ecg.dao.exception.EcgDAOException;

import java.util.List;
import java.util.Map;

/**
 *
 * @author F4817273
 */
public interface GenericDao<T>{
    List<T> findAll() throws EcgDAOException;

    T findById(Integer id) throws EcgDAOException;

    void create(T entityBean) throws EcgDAOException;

    void update(T newValue) throws EcgDAOException;

    void remove(T entityBean) throws EcgDAOException;

    long count() throws EcgDAOException;

    List<T> findRange(int start,int end) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query,Map<String,Object> params) throws EcgDAOException;

    List<Object[]> executeSQLQuery(String query) throws EcgDAOException;

    T merge(T entityBean) throws EcgDAOException;

    Integer save(T entityBean) throws EcgDAOException;
}
