package za.org.ecg.dao.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Base64;

import static javax.persistence.GenerationType.IDENTITY;
import static za.org.ecg.dao.entity.EntityBean.CATALOG;

/**
 *
 * @author F4817273
 */

@Entity
@Table(name="attachments"
    ,catalog=CATALOG
)
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "Attachment.findAll", query = "SELECT a FROM Attachment a"),
        @NamedQuery(name = "Attachment.findByAttachmentId", query = "SELECT a FROM Attachment a WHERE a.attachmentId = :attachmentId"),
        @NamedQuery(name = "Attachment.findByFormat", query = "SELECT a FROM Attachment a WHERE a.format = :format"),
        @NamedQuery(name = "Attachment.findByName", query = "SELECT a FROM Attachment a WHERE a.name = :name")})

public class Attachment  implements EntityBean {

     private Integer attachmentId;
     private String format;
     private String name;
     private String url;
     private byte[] content;
     private String systemPath;
     private String context;

    public Attachment() {
    }
    public Attachment(int id) {
        this.attachmentId=id;
    }
    public Attachment(String format, String name, byte[] content) {
       this.format = format;
       this.name = name;
       this.content = content;
    }
   
    @Id @GeneratedValue(strategy=IDENTITY)
    @Column(name="attachment_id", unique=true, nullable=false)
    public Integer getAttachmentId() {
        return this.attachmentId;
    }
    
    public void setAttachmentId(Integer attachmentId) {
        this.attachmentId = attachmentId;
    }

    
    @Column(name="format", nullable=false, length=15)
    public String getFormat() {
        return this.format;
    }
    
    public void setFormat(String format) {
        this.format = format;
    }

    
    @Column(name="name", nullable=false, length=50)
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    
    @Column(name="content")
    public byte[] getContent() {
        return this.content;
    }
    
    public void setContent(byte[] content) {
        this.content = content;
    }


    @Column(name="url")
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Column(name = "system_path")
    public String getSystemPath() {
        return systemPath;
    }

    public void setSystemPath(String systemPath) {
        this.systemPath = systemPath;
    }

    @Column(name = "context")
    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    @Transient
    public  String getByteString()
    {
        byte[] encoded= Base64.getEncoder().encode(content);
        return  new String(encoded);
    }

    @Override
    public String toString() {
        return "Attachment{" +
                "attachmentId=" + attachmentId +
                ", format='" + format + '\'' +
                ", name='" + name + '\'' +
                ", url='" + url + '\'' +
                ", systemPath='" + systemPath + '\'' +
                ", context='" + context + '\'' +
                '}';
    }
}


