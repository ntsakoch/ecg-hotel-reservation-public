package za.org.ecg.dao;

import za.org.ecg.dao.entity.ApplicationPropertyConfig;
import za.org.ecg.dao.exception.EcgDAOException;

/**
 * Created by Ntsako on 2016/12/23.
 */
public interface ApplicationPropertyConfigDao extends GenericDao<ApplicationPropertyConfig>{
}
