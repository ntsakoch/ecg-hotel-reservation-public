package za.org.ecg.dao.impl;

import org.springframework.stereotype.Repository;
import za.org.ecg.dao.PreferenceCategoryDao;
import za.org.ecg.dao.entity.PreferenceCategory;

/**
 * Created by root on 2016/09/28.
 */
@Repository("preferenceCategoryDao")
public class PreferenceCategoryDaoImpl extends AbstractDao<PreferenceCategory> implements PreferenceCategoryDao {

    public PreferenceCategoryDaoImpl() {
        super(PreferenceCategory.class);
    }
}
