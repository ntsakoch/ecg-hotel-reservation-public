package za.org.ecg.dao;

import za.org.ecg.dao.entity.CustomerPreference;

/**
 * Created by F4817273 on 2016/09/22.
 */
public interface CustomerPreferenceDao extends GenericDao<CustomerPreference> {
}
