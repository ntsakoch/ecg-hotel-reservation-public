package za.org.ecg.dao;

import za.org.ecg.dao.entity.FeatureCategory;

/**
 * Created by F4817273 on 2016/09/22.
 */
public interface FeatureCategoryDao extends GenericDao<FeatureCategory> {
}
