package za.org.ecg.dao;

import za.org.ecg.dao.entity.Currency;

/**
 * Created by Ntsako on 2017/01/18.
 */
public interface CurrencyDao extends GenericDao<Currency> {
}
