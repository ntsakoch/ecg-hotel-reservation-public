package za.org.ecg.dao.entity;

import java.io.Serializable;

/**
 *
 * @author F4817273
 */
public interface EntityBean extends Serializable{
   String CATALOG="ecg_bookings";
   default Integer getId()
   {
      return -1;
   }
}
