package za.org.ecg.dao.impl;


import org.hibernate.HibernateException;
import org.hibernate.criterion.Projections;
import org.springframework.stereotype.Repository;
import za.org.ecg.dao.CustomerAccountDao;
import za.org.ecg.dao.entity.CustomerAccount;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by root on 2016/09/28.
 */
@Repository("customerAccountDao")
public class CustomerAccountDaoImpl extends AbstractDao<CustomerAccount> implements CustomerAccountDao {

    public CustomerAccountDaoImpl() {
        super(CustomerAccount.class);
    }

    @Override
    public List<CustomerAccount> findByMinimumOutstanding(double minimumBalance) throws EcgDAOException {
        Map<String,Object> params=new HashMap<>();
        params.put("minimumOutstandingBalance",minimumBalance);
        return  findByNamedQuery("CustomerAccount.findByMinimumOutstandingBalance",params);
    }

    public long count() throws EcgDAOException {
       throw new UnsupportedOperationException("Operation Not Supported.");
    }
}
