package za.org.ecg.dao;

import za.org.ecg.dao.entity.CustomerDiscount;

/**
 * Created by F4817273 on 2016/09/22.
 */
public interface CustomerDiscountDao extends GenericDao<CustomerDiscount> {
}
