package za.org.ecg.dao;

import za.org.ecg.dao.entity.Customer;
import za.org.ecg.dao.exception.EcgDAOException;

/**
 * Created by f4817273 on 2016/09/05.
 */
public interface CustomerDao extends GenericDao<Customer> {
    Customer findByIdNumber(String idNumber) throws EcgDAOException;
}
