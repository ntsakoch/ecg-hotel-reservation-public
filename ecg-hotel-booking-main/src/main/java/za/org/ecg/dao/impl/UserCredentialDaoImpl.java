package za.org.ecg.dao.impl;

import org.springframework.stereotype.Repository;
import za.org.ecg.dao.UserCredentialDao;
import za.org.ecg.dao.entity.UserCredential;


/**
 * Created by F4817273 on 2016/09/05.
 */
@Repository("userCredentialDao")
public class UserCredentialDaoImpl extends AbstractDao<UserCredential> implements UserCredentialDao {

    public UserCredentialDaoImpl() {
        super(UserCredential.class);
    }
}
