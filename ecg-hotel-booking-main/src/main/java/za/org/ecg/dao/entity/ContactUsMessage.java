package za.org.ecg.dao.entity;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import static javax.persistence.GenerationType.IDENTITY;
import static za.org.ecg.dao.entity.EntityBean.CATALOG;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author F4817273
 */

@Entity
@Table(name="contact_us_messages"
    ,catalog=CATALOG
)
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "ContactUsMessage.findAll", query = "SELECT c FROM ContactUsMessage c"),
        @NamedQuery(name = "ContactUsMessage.findByContactUsMessageId", query = "SELECT c FROM ContactUsMessage c WHERE c.contactUsMessageId = :contactUsMessageId"),
        @NamedQuery(name = "ContactUsMessage.findByContactUsMessageCategoryId", query = "SELECT c FROM ContactUsMessage c WHERE c.contactUsMessageCategoryId = :contactUsMessageCategoryId"),
        @NamedQuery(name = "ContactUsMessage.findBySenderFullname", query = "SELECT c FROM ContactUsMessage c WHERE c.senderFullname = :senderFullname"),
        @NamedQuery(name = "ContactUsMessage.findBySubject", query = "SELECT c FROM ContactUsMessage c WHERE c.subject = :subject"),
        @NamedQuery(name = "ContactUsMessage.findByMessage", query = "SELECT c FROM ContactUsMessage c WHERE c.message = :message"),
        @NamedQuery(name = "ContactUsMessage.findByStatus", query = "SELECT c FROM ContactUsMessage c WHERE c.status = :status"),
        @NamedQuery(name = "ContactUsMessage.findByReplyMethod", query = "SELECT c FROM ContactUsMessage c WHERE c.replyMethod = :replyMethod"),
        @NamedQuery(name = "ContactUsMessage.findByReplyChannel", query = "SELECT c FROM ContactUsMessage c WHERE c.replyChannel = :replyChannel"),
        @NamedQuery(name = "ContactUsMessage.findByAttachmentId", query = "SELECT c FROM ContactUsMessage c WHERE c.attachmentId = :attachmentId"),
        @NamedQuery(name = "ContactUsMessage.findByUserId", query = "SELECT c FROM ContactUsMessage c WHERE c.userId = :userId"),
        @NamedQuery(name = "ContactUsMessage.findByDateSentRange", query = "SELECT c FROM ContactUsMessage c WHERE c.dateSent BETWEEN :fromDate AND :toDate"),
        @NamedQuery(name = "ContactUsMessage.findByDateSent", query = "SELECT c FROM ContactUsMessage c WHERE c.dateSent = :dateSent")})

public class ContactUsMessage  implements EntityBean {

     private Integer contactUsMessageId;
     private int contactUsMessageCategoryId;
     private int userId;
     private String senderFullname;
     private String subject;
     private String message;
     private String status;
     private String replyMethod;
     private String replyChannel;
     private int attachmentId;
     private Date dateSent;
    private ContactUsMessageCategory category;
    private Attachment attachment;

    public ContactUsMessage() {
    }
    public ContactUsMessage(Integer contactUsMessageId) {
        this.contactUsMessageId=contactUsMessageId;
    }
    public ContactUsMessage(int contactUsMessageCategoryId, String senderFullname, String subject, String message, String status, String replyMethod, String replyChannel, int attachmentId, Date dateSent) {
       this.contactUsMessageCategoryId = contactUsMessageCategoryId;
       this.senderFullname = senderFullname;
       this.subject = subject;
       this.message = message;
       this.status = status;
       this.replyMethod = replyMethod;
       this.replyChannel = replyChannel;
       this.attachmentId = attachmentId;
       this.dateSent = dateSent;
    }
   
    @Id @GeneratedValue(strategy=IDENTITY)
    @Column(name="contact_us_message_id", unique=true, nullable=false)
    public Integer getContactUsMessageId() {
        return this.contactUsMessageId;
    }
    
    public void setContactUsMessageId(Integer contactUsMessageId) {
        this.contactUsMessageId = contactUsMessageId;
    }

    
    @Column(name="contact_us_message_category_id", updatable = false,insertable = false)
    public int getContactUsMessageCategoryId() {
        return this.contactUsMessageCategoryId;
    }
    
    public void setContactUsMessageCategoryId(int contactUsMessageCategoryId) {
        this.contactUsMessageCategoryId = contactUsMessageCategoryId;
    }

    @Column(name="user_id", nullable=true)
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Column(name="sender_fullname", nullable=false)
    public String getSenderFullname() {
        return this.senderFullname;
    }
    
    public void setSenderFullname(String senderFullname) {
        this.senderFullname = senderFullname;
    }

    
    @Column(name="subject", nullable=false)
    public String getSubject() {
        return this.subject;
    }
    
    public void setSubject(String subject) {
        this.subject = subject;
    }

    
    @Column(name="message", nullable=false, length=1000)
    public String getMessage() {
        return this.message;
    }
    
    public void setMessage(String message) {
        this.message = message;
    }

    
    @Column(name="status", nullable=false, length=100)
    public String getStatus() {
        return this.status;
    }
    
    public void setStatus(String status) {
        this.status = status;
    }

    
    @Column(name="reply_method", nullable=false, length=100)
    public String getReplyMethod() {
        return this.replyMethod;
    }
    
    public void setReplyMethod(String replyMethod) {
        this.replyMethod = replyMethod;
    }

    
    @Column(name="reply_channel", nullable=false)
    public String getReplyChannel() {
        return this.replyChannel;
    }
    
    public void setReplyChannel(String replyChannel) {
        this.replyChannel = replyChannel;
    }

    
    @Column(name="attachment_id", nullable=false,updatable = false,insertable = false)
    public int getAttachmentId() {
        return this.attachmentId;
    }
    
    public void setAttachmentId(int attachmentId) {
        this.attachmentId = attachmentId;
    }

    
    @Column(name="date_sent", nullable=false)
    public Date getDateSent() {
        return this.dateSent;
    }
    
    public void setDateSent(Date dateSent) {
        this.dateSent = dateSent;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "contact_us_message_category_id")
    @NotFound(action = NotFoundAction.EXCEPTION)
    public ContactUsMessageCategory getCategory() {
        return category;
    }

    public void setCategory(ContactUsMessageCategory category) {
        this.category = category;
    }

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "attachment_id")
    @NotFound(action = NotFoundAction.IGNORE)
    public Attachment getAttachment() {
        return attachment;
    }

    public void setAttachment(Attachment attachment) {
        this.attachment = attachment;
    }

    @Override
    public String toString() {
        return "ContactUsMessage{" +
                "contactUsMessageId=" + contactUsMessageId +
                ", contactUsMessageCategoryId=" + contactUsMessageCategoryId +
                ", userId=" + userId +
                ", senderFullname='" + senderFullname + '\'' +
                ", subject='" + subject + '\'' +
                ", message='" + message + '\'' +
                ", status='" + status + '\'' +
                ", replyMethod='" + replyMethod + '\'' +
                ", replyChannel='" + replyChannel + '\'' +
                ", attachmentId=" + attachmentId +
                ", dateSent=" + dateSent +
                '}';
    }
}


