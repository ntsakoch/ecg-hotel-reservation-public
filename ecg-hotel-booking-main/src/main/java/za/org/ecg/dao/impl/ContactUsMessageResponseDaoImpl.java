package za.org.ecg.dao.impl;

import org.springframework.stereotype.Repository;
import za.org.ecg.dao.ContactUsMessageResponseDao;
import za.org.ecg.dao.entity.ContactUsMessageResponse;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by root on 2016/09/28.
 */
@Repository("contactUsMessageResponseDao")
public class ContactUsMessageResponseDaoImpl extends AbstractDao<ContactUsMessageResponse> implements ContactUsMessageResponseDao {
    public ContactUsMessageResponseDaoImpl() {
        super(ContactUsMessageResponse.class);
    }

    @Override
    public List<ContactUsMessageResponse> findByOriginalContactUsMessageId(int originalMessageId) throws EcgDAOException {
            Map<String,Object> params=new HashMap<>();
            params.put("contactUsMessageId",originalMessageId);
            return  findByNamedQuery("ContactUsMessageResponse.findByContactUsMessageId",params);
    }
}
