/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.org.ecg.dao.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import static za.org.ecg.dao.entity.EntityBean.CATALOG;

/**
 *
 * @author F4817273
 */
@Entity
@Table(name = "physical_addresses", catalog = CATALOG)
public class PhysicalAddress implements  EntityBean {

    private static final long serialVersionUID = 1L;

    private Integer physicalAddressId;
    private String addressLine1;
    private String addressLine2;
    private String city;
    private String suburb;
    private String streetName;
    private String streetNumber;
    private String postalCode;
    private User userInfo;
    private Integer userId;

    public PhysicalAddress(Integer physicalAddressId) {
        this.physicalAddressId = physicalAddressId;
    }

    public PhysicalAddress() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "physical_address_id")
    public Integer getPhysicalAddressId() {
        return physicalAddressId;
    }


    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    @NotFound(action = NotFoundAction.EXCEPTION)
    @JsonIgnore
    public User getUserInfo() {
        return userInfo;
    }

    @Column(name = "user_id",insertable = false,updatable = false)
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
  
    public void setUserInfo(User userInfo) {
        this.userInfo = userInfo;
    }
    public void setPhysicalAddressId(Integer id) {
        this.physicalAddressId = id;
    }

    @Column(name = "city")
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Column(name = "code")
    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    @Column(name = "address_line_1")
    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    @Column(name = "address_line_2")
    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    @Column(name = "suburb")
    public String getSuburb() {
        return suburb;
    }

    public void setSuburb(String suburb) {
        this.suburb = suburb;
    }

    @Column(name = "street_name")
    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    @Column(name = "street_no")
    public String getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (physicalAddressId != null ? physicalAddressId.hashCode() : super.hashCode());
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PhysicalAddress)) {
            return false;
        }
        PhysicalAddress other = (PhysicalAddress) object;
        if ((this.physicalAddressId == null && other.physicalAddressId != null) || (this.physicalAddressId != null && !this.physicalAddressId.equals(other.physicalAddressId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "PhysicalAddress{" +
                "physicalAddressId=" + physicalAddressId +
                ", addressLine1='" + addressLine1 + '\'' +
                ", addressLine2='" + addressLine2 + '\'' +
                ", city='" + city + '\'' +
                ", suburb='" + suburb + '\'' +
                ", streetName='" + streetName + '\'' +
                ", streetNumber='" + streetNumber + '\'' +
                ", postalCode='" + postalCode + '\'' +
                '}';
    }
}
