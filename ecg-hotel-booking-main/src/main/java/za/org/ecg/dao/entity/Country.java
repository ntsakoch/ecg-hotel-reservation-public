package za.org.ecg.dao.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import za.org.ecg.dao.cache.EnableEntityCaching;
import za.org.ecg.dao.impl.CountryDaoImpl;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import java.util.Set;

import static za.org.ecg.dao.entity.EntityBean.CATALOG;

/**
 * Created by Ntsako on 2017/01/18.
 */
@Entity
@Table(name = "countries", catalog = CATALOG)
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "Country.findByCountryCode", query = "SELECT c FROM Country c WHERE c.countryCode=:countryCode"),
        @NamedQuery(name = "Country.findAllActive", query = "SELECT c FROM Country c WHERE c.active=1")})
@EnableEntityCaching(daoImplementer = CountryDaoImpl.class)
public class Country implements EntityBean {
    private int countryId;
    private int defaultLanguageId;
    private boolean active;
    private String countryCode;
    private String countryName;
    private Language defaultLanguage;
    private Set<Language> languages;
    private Set<CountryCity> cities;

    public Country() {
    }

    public Country(int countryId) {
        this.countryId = countryId;
    }

    public Country(int countryId, int currencyId, int defaultLanguageId, String countryCode, String countryName) {
        this.countryId = countryId;
        this.defaultLanguageId = defaultLanguageId;
        this.countryCode = countryCode;
        this.countryName = countryName;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "country_id")
    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    @Column(name = "default_lang_id",updatable = false,insertable = false)
    public int getDefaultLanguageId() {
        return defaultLanguageId;
    }

    public void setDefaultLanguageId(int defaultLanguageId) {
        this.defaultLanguageId = defaultLanguageId;
    }

    @Column(name = "country_code")
    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    @Column(name = "country_name")
    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "default_lang_id")
    @NotFound(action = NotFoundAction.IGNORE)
    @JsonIgnore
    public Language getDefaultLanguage() {
        return defaultLanguage;
    }

    public void setDefaultLanguage(Language defaultLanguage) {
        this.defaultLanguage = defaultLanguage;
    }

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "country_id")
    @Fetch(value = FetchMode.SUBSELECT)
    public Set<Language> getLanguages() {
        return languages;
    }

    public void setLanguages(Set<Language> languages) {
        this.languages = languages;
    }

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "country_id")
    @Fetch(value = FetchMode.SUBSELECT)
    public Set<CountryCity> getCities() {
        return cities;
    }

    public void setCities(Set<CountryCity> cities) {
        this.cities = cities;
    }

    @Column(name = "is_active")
    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "Country{" +
                "countryId=" + countryId +
                ", defaultLanguageId=" + defaultLanguageId +
                ", countryCode='" + countryCode + '\'' +
                ", countryName='" + countryName + '\'' +
                '}';
    }

    @Override
    @Transient
    public Integer getId() {
        return countryId;
    }
}
