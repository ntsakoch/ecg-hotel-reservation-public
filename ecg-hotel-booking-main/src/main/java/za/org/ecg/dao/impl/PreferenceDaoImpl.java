package za.org.ecg.dao.impl;

import org.springframework.stereotype.Repository;
import za.org.ecg.dao.PreferenceDao;
import za.org.ecg.dao.entity.Preference;

/**
 * Created by root on 2016/09/28.
 */
@Repository("preferenceDao")
public class PreferenceDaoImpl extends AbstractDao<Preference> implements PreferenceDao {

    public PreferenceDaoImpl() {
        super(Preference.class);
    }

}
