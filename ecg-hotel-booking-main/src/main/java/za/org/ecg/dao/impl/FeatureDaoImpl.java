package za.org.ecg.dao.impl;

import org.springframework.stereotype.Repository;
import za.org.ecg.dao.FeatureDao;
import za.org.ecg.dao.entity.Feature;

/**
 * Created by root on 2016/09/28.
 */
@Repository("featureDao")
public class FeatureDaoImpl extends AbstractDao<Feature> implements FeatureDao {

    public FeatureDaoImpl() {
        super(Feature.class);
    }
}
