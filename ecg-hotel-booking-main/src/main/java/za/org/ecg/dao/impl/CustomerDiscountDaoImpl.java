package za.org.ecg.dao.impl;

import org.springframework.stereotype.Repository;
import za.org.ecg.dao.CustomerDiscountDao;
import za.org.ecg.dao.entity.CustomerDiscount;

/**
 * Created by root on 2016/09/28.
 */
@Repository("customerDiscountDao")
public class CustomerDiscountDaoImpl extends AbstractDao<CustomerDiscount> implements CustomerDiscountDao {
    public CustomerDiscountDaoImpl(){
        super(CustomerDiscount.class);
    }
}
