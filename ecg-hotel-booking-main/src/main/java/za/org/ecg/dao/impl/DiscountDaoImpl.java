package za.org.ecg.dao.impl;

import org.springframework.stereotype.Repository;
import za.org.ecg.dao.DiscountDao;
import za.org.ecg.dao.entity.Discount;

/**
 * Created by root on 2016/09/28.
 */
@Repository("discountDao")
public class DiscountDaoImpl extends AbstractDao<Discount> implements DiscountDao {

    public DiscountDaoImpl() {
        super(Discount.class);
    }

}
