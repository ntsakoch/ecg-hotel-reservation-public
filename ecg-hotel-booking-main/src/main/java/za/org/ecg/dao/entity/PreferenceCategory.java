package za.org.ecg.dao.entity;

import za.org.ecg.dao.cache.EnableEntityCaching;
import za.org.ecg.dao.impl.PreferenceCategoryDaoImpl;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import static javax.persistence.GenerationType.IDENTITY;
import static za.org.ecg.dao.entity.EntityBean.CATALOG;

import java.io.Serializable;

/**
 *
 * @author F4817273
 */
@Entity
@Table(name="preference_categories"
    ,catalog=CATALOG
)
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "PreferenceCategory.findAll", query = "SELECT p FROM PreferenceCategory p"),
        @NamedQuery(name = "PreferenceCategory.findByPreferenceCategoryId", query = "SELECT p FROM PreferenceCategory p WHERE p.preferenceCategoryId = :preferenceCategoryId"),
        @NamedQuery(name = "PreferenceCategory.findByCategoryName", query = "SELECT p FROM PreferenceCategory p WHERE p.categoryName = :categoryName"),
        @NamedQuery(name = "PreferenceCategory.findByCategoryDescription", query = "SELECT p FROM PreferenceCategory p WHERE p.categoryDescription = :categoryDescription"),
        @NamedQuery(name = "PreferenceCategory.findByCategoryCode", query = "SELECT p FROM PreferenceCategory p WHERE p.categoryCode = :categoryCode")})
@EnableEntityCaching(daoImplementer = PreferenceCategoryDaoImpl.class)
public class PreferenceCategory  implements EntityBean {


     private Integer preferenceCategoryId;
     private String categoryName;
     private String categoryDescription;
     private String categoryCode;

    public PreferenceCategory() {
    }
    public PreferenceCategory(Integer preferenceCategoryId) {
        this.preferenceCategoryId=preferenceCategoryId;
    }
    public PreferenceCategory(String categoryName, String categoryDescription, String categoryCode) {
       this.categoryName = categoryName;
       this.categoryDescription = categoryDescription;
       this.categoryCode = categoryCode;
    }
   
    @Id @GeneratedValue(strategy=IDENTITY)
    @Column(name="preference_category_id", unique=true, nullable=false)
    public Integer getPreferenceCategoryId() {
        return this.preferenceCategoryId;
    }
    
    public void setPreferenceCategoryId(Integer preferenceCategoryId) {
        this.preferenceCategoryId = preferenceCategoryId;
    }

    
    @Column(name="category_name", nullable=false, length=50)
    public String getCategoryName() {
        return this.categoryName;
    }
    
    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    
    @Column(name="category_description", nullable=false)
    public String getCategoryDescription() {
        return this.categoryDescription;
    }
    
    public void setCategoryDescription(String categoryDescription) {
        this.categoryDescription = categoryDescription;
    }

    
    @Column(name="category_code", nullable=false, length=50)
    public String getCategoryCode() {
        return this.categoryCode;
    }
    
    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    @Override
    public String toString() {
        return "PreferenceCategory{" +
                "preferenceCategoryId=" + preferenceCategoryId +
                ", categoryName='" + categoryName + '\'' +
                ", categoryDescription='" + categoryDescription + '\'' +
                ", categoryCode='" + categoryCode + '\'' +
                '}';
    }

    @Override
    @Transient
    public Integer getId() {
        return preferenceCategoryId;
    }
}


