/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package za.org.ecg.dao.impl;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import za.org.ecg.dao.UserDao;
import za.org.ecg.dao.entity.ActionAudit;
import za.org.ecg.dao.entity.User;
import za.org.ecg.dao.entity.UserCredential;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author 
 */
@Repository("userDao")
public class UserDaoImpl extends AbstractDao<User> implements UserDao {

    public UserDaoImpl() {
        super(User.class);
    }

    public User findByIdNumber(String id) throws EcgDAOException {
        Map<String,Object> params=new HashMap<String,Object>();
        params.put("identityNo",id);
        List<User> users=findByNamedQuery("User.findByIdentityNo",params);
        return !users.isEmpty()?users.get(0):new User(-1);
    }

    public User authenticateUser(String username, String password) throws EcgDAOException{
            Query query = getSession().getNamedQuery("UserCredential.login");
            query.setParameter("username", username);
            query.setParameter("password", password);
            List<UserCredential> userCredentials = query.list();
            return !userCredentials.isEmpty() ?  findById(userCredentials.get(0).getUserId()) : new User(-2);
    }

    @Override
    public User findByUsername(String username) throws EcgDAOException {
            Query query=getSession().getNamedQuery("UserCredential.findByUsername");
            query.setParameter("username", username);
            List<UserCredential> userCredentials = query.list();
            return !userCredentials.isEmpty() ?  findById(userCredentials.get(0).getUserId()) : new User(-2);
    }

    @Override
    public List<ActionAudit> findAllUsersActions(int userId) throws  EcgDAOException
    {
        try {
            Query query = getSession().getNamedQuery("ActionAudit.findByUserId");
            query.setParameter("userId", userId);
            return query.list();
        }
        catch (HibernateException hExc)
        {
            throw  new EcgDAOException(hExc.getMessage(),hExc);
        }
    }

}
