package za.org.ecg.dao.impl;

import org.springframework.stereotype.Repository;
import za.org.ecg.dao.MailMessageDao;
import za.org.ecg.dao.entity.MailMessage;

/**
 * Created by root on 2016/09/28.
 */
@Repository("mailMessageDao")
public class MailMessageDaoImpl extends AbstractDao<MailMessage> implements MailMessageDao {

    public MailMessageDaoImpl() {
        super(MailMessage.class);
    }
}
