package za.org.ecg.dao.entity;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import za.org.ecg.dao.cache.EnableEntityCaching;
import za.org.ecg.dao.impl.LocationAddressDaoImpl;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import static za.org.ecg.dao.entity.EntityBean.CATALOG;

/**
 * Created by Ntsako on 2017/02/21.
 */
@Entity
@Table(name="location_addresses"
        ,catalog=CATALOG
)
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "LocationAddress.FindBySuburbName", query = "SELECT l FROM LocationAddress l WHERE l.suburbName=:suburbName")})
@EnableEntityCaching(daoImplementer = LocationAddressDaoImpl.class)
public class LocationAddress implements EntityBean {
    private int locationAddressId;
    private int countryCityId;
    private  int streetNumber;
    private String streetName;
    private String suburbName;
    private String postalCode;
    private CountryCity countryCity;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "location_address_id")
    public int getLocationAddressId() {
        return locationAddressId;
    }

    public void setLocationAddressId(int locationAddressId) {
        this.locationAddressId = locationAddressId;
    }

    @Column(name = "country_city_id",insertable = false,updatable = false)
    public int getCountryCityId() {
        return countryCityId;
    }

    public void setCountryCityId(int countryCityId) {
        this.countryCityId = countryCityId;
    }

    @Column(name = "street_number")
    public int getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(int streetNumber) {
        this.streetNumber = streetNumber;
    }

    @Column(name = "street_name")
    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    @Column(name = "suburb_name")
    public String getSuburbName() {
        return suburbName;
    }

    public void setSuburbName(String suburbName) {
        this.suburbName = suburbName;
    }

    @Column(name = "postal_code")
    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "country_city_id")
    @NotFound(action = NotFoundAction.IGNORE)
    public CountryCity getCountryCity() {
        return countryCity;
    }

    public void setCountryCity(CountryCity countryCity) {
        this.countryCity = countryCity;
    }

    @Override
    @Transient
    public Integer getId() {
        return locationAddressId;
    }

    @Override
    public String toString() {
        return "LocationAddress{" +
                "locationAddressId=" + locationAddressId +
                ", countryCityId=" + countryCityId +
                ", streetNumber=" + streetNumber +
                ", streetName='" + streetName + '\'' +
                ", suburbName='" + suburbName + '\'' +
                ", postalCode='" + postalCode + '\'' +
                '}';
    }
}
