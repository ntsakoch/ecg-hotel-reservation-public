package za.org.ecg.dao;

import za.org.ecg.dao.entity.ContactUsMessage;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.Date;
import java.util.List;

/**
 * Created by F4817273 on 2016/09/22.
 */
public interface ContactUsMessageDao extends GenericDao<ContactUsMessage> {

    List<ContactUsMessage> findByUserId(int userId) throws EcgDAOException;
    List<ContactUsMessage> findByStatus(String status) throws EcgDAOException;
    List<ContactUsMessage> findByFullName(String fullName) throws EcgDAOException;
    List<ContactUsMessage> findByDateRange(Date fromDate, Date toDate) throws EcgDAOException;

}
