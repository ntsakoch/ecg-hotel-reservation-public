package za.org.ecg.dao.entity;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import static za.org.ecg.dao.entity.EntityBean.CATALOG;

/**
 *
 * @author F4817273
 */
@Entity
@Table(name = "admin_users", catalog = CATALOG)
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "AdminUser.findAll", query = "SELECT a FROM AdminUser a"),
        @NamedQuery(name = "AdminUser.findByAdminUserId", query = "SELECT a FROM AdminUser a WHERE a.adminUserId = :adminUserId"),
        @NamedQuery(name = "AdminUser.findByUserId", query = "SELECT a FROM AdminUser a WHERE a.userId = :userId")})
public class AdminUser implements  EntityBean {

    private static final long serialVersionUID = 1L;

    private Integer adminUserId;
    private User userInfo;
    private Integer userId;

    public AdminUser(Integer adminUserId) {
        this.adminUserId = adminUserId;
    }

    public AdminUser() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "admin_user_id")
    public Integer getAdminUserId() {
        return adminUserId;
    }

    public void setAdminUserId(Integer adminUserId) {
        this.adminUserId = adminUserId;
    }

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL,orphanRemoval = true)
    @JoinColumn(name = "user_id")
    @NotFound(action = NotFoundAction.EXCEPTION)
    @Fetch(value = FetchMode.JOIN)
    public User getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(User user) {
        this.userInfo = user;
    }

    @Column(name = "user_id",insertable = false, updatable = false)
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "AdminUser{" +
                "adminUserId=" + adminUserId +
                ", userInfo=" + userInfo +
                '}';
    }
}
