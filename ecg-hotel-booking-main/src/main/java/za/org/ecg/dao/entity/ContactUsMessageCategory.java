package za.org.ecg.dao.entity;

import za.org.ecg.dao.cache.EnableEntityCaching;
import za.org.ecg.dao.impl.ContactUsMessageCategoryDaoImpl;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import static za.org.ecg.dao.entity.EntityBean.CATALOG;
import static javax.persistence.GenerationType.IDENTITY;

/**
 *
 * @author F4817273
 */

@Entity
@Table(name="contact_us_message_categories"
    ,catalog=CATALOG
)
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "ContactUsMessageCategory.findAll", query = "SELECT c FROM ContactUsMessageCategory c"),
        @NamedQuery(name = "ContactUsMessageCategory.findByContactUsMessageCategoryId", query = "SELECT c FROM ContactUsMessageCategory c WHERE c.contactUsMessageCategoryId = :contactUsMessageCategoryId"),
        @NamedQuery(name = "ContactUsMessageCategory.findByCategoryCode", query = "SELECT c FROM ContactUsMessageCategory c WHERE c.categoryCode = :categoryCode"),
        @NamedQuery(name = "ContactUsMessageCategory.findByCategoryName", query = "SELECT c FROM ContactUsMessageCategory c WHERE c.categoryName = :categoryName")})
@EnableEntityCaching(daoImplementer = ContactUsMessageCategoryDaoImpl.class)
public class ContactUsMessageCategory  implements EntityBean {

     private Integer contactUsMessageCategoryId;
     private String categoryCode;
     private String categoryName;

    public ContactUsMessageCategory() {
    }
    public ContactUsMessageCategory(Integer contactUsMessageCategoryId) {
        this.contactUsMessageCategoryId=contactUsMessageCategoryId;
    }
    public ContactUsMessageCategory(String categoryCode, String categoryName) {
       this.categoryCode = categoryCode;
       this.categoryName = categoryName;
    }
   
    @Id @GeneratedValue(strategy=IDENTITY)
    @Column(name="contact_us_message_category_id", unique=true, nullable=false)
    public Integer getContactUsMessageCategoryId() {
        return this.contactUsMessageCategoryId;
    }
    
    public void setContactUsMessageCategoryId(Integer contactUsMessageCategoryId) {
        this.contactUsMessageCategoryId = contactUsMessageCategoryId;
    }

    
    @Column(name="category_code", nullable=false, length=100)
    public String getCategoryCode() {
        return this.categoryCode;
    }
    
    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    
    @Column(name="category_name", nullable=false)
    public String getCategoryName() {
        return this.categoryName;
    }
    
    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    @Override
    public String toString() {
        return "ContactUsMessageCategory{" +
                "contactUsMessageCategoryId=" + contactUsMessageCategoryId +
                ", categoryCode='" + categoryCode + '\'' +
                ", categoryName='" + categoryName + '\'' +
                '}';
    }

    @Override
    @Transient
    public Integer getId() {
        return contactUsMessageCategoryId;
    }
}


