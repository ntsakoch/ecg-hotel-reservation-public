package za.org.ecg.dao.impl;

import org.springframework.stereotype.Repository;
import za.org.ecg.utils.CommonUtils;
import za.org.ecg.dao.RoomDao;
import za.org.ecg.dao.entity.Room;
import za.org.ecg.dao.exception.EcgDAOException;
import za.org.ecg.utils.DateUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by F4817273 on 2016/09/23.
 */
@Repository("roomDao")
public class RoomDaoImpl extends AbstractDao<Room> implements RoomDao {


    /*
     * SELECT DISTINCT a.room_id 'a_room_id',b.room_id 'b_room_id', b.checkin_date, b.checkout_date FROM rooms a
     LEFT JOIN customer_bookings AS b ON a.room_id=b.room_id
     WHERE b.checkin_date NOT BETWEEN date('2017-03-02') AND date('2017-03-10')
     AND b.checkout_date NOT BETWEEN date('2017-03-02') AND date('2017-03-10')
     OR b.room_id IS NULL
     */
    private static final String SELECT_UN_AVAILABLE_ROOM_QUERY="SELECT DISTINCT a.room_id,'r_id' FROM rooms a \n" +
            "INNER JOIN customer_bookings AS b ON a.room_id=b.room_id\n" +
            "WHERE b.checkin_date  BETWEEN date(:checkinDate) AND date(:checkoutDate) " +
            "OR b.checkout_date  BETWEEN date(:checkinDate) AND date(:checkoutDate) ";

    private final  static String SELECT_AVAILABLE_ROOM_QUERY="SELECT room_id,'r_id' FROM rooms WHERE room_id NOT IN (%s)";

    private final  static String SELECT_AVAILABLE_ROOM_QUERY_2="SELECT room_id,'r_id' FROM rooms WHERE room_id NOT IN (%s) \n" +
            "AND max_num_children >=:minNumChildren AND max_num_adults >=:minNumAdults ";


    public RoomDaoImpl() {
        super(Room.class);
    }

    @Override
    public List<Room> getAllAvailableRooms(Date checkinDate, Date checkoutDate) throws EcgDAOException {
        String unAvailableRoomIds = getUnAvailableRoomIds(checkinDate, checkoutDate);
        List<Object[]> result = executeSQLQuery(String.format(SELECT_AVAILABLE_ROOM_QUERY,unAvailableRoomIds),null);
        return findBySQLQueryResult(result,0);
    }

    private String getUnAvailableRoomIds(Date checkinDate, Date checkoutDate) throws EcgDAOException {
        String checkinDateStr= DateUtils.getInstance().format(checkinDate, CommonUtils.DEFAULT_DATE_FORMAT);
        String checkoutDateStr= DateUtils.getInstance().format(checkoutDate, CommonUtils.DEFAULT_DATE_FORMAT);
        Map<String, Object> params = getRequiredParams(checkinDateStr, checkoutDateStr);
        List<Object[]> unAvailableRoomsResults = executeSQLQuery(SELECT_UN_AVAILABLE_ROOM_QUERY, params);
        StringBuffer roomIDs=new StringBuffer();
        if(!unAvailableRoomsResults.isEmpty()) {
            for (Object[] objArr : unAvailableRoomsResults) {
                int roomId = Integer.parseInt(String.valueOf(objArr[0]));
                roomIDs.append(roomId).append(",");
            }
            roomIDs.deleteCharAt(roomIDs.length() - 1);
            return roomIDs.toString();
        }
        return "0";
    }

    @Override
    public List<Room> getAllAvailableRooms(Date checkinDate, Date checkoutDate, int numChildren, int numAdults) throws EcgDAOException {
        String unAvailableRoomIds = getUnAvailableRoomIds(checkinDate, checkoutDate);
        HashMap<String,Object> params=new HashMap<>();
        params.put("minNumChildren",numChildren);
        params.put("minNumAdults",numAdults);
        List<Object[]> result = executeSQLQuery(String.format(SELECT_AVAILABLE_ROOM_QUERY_2,unAvailableRoomIds), params);
        return findBySQLQueryResult(result,0);
    }


    @Override
    public List<Room> getTopListedRooms() throws EcgDAOException {
        return  findByNamedQuery("Room.findTopListed",null);
    }

    @Override
    public List<Room> getRoomsOnSpecial() throws EcgDAOException {
        return  findByNamedQuery("Room.findOnSpecial",null);
    }

    private Map<String, Object> getRequiredParams(String checkinDateStr, String checkoutDateStr) {
        Map<String, Object> params = new HashMap<>();
        params.put("checkinDate", checkinDateStr);
        params.put("checkoutDate", checkoutDateStr);
        return params;
    }
}
