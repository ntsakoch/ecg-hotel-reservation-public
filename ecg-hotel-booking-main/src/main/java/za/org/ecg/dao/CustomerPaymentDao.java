package za.org.ecg.dao;

import za.org.ecg.dao.entity.CustomerPayment;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.List;

/**
 * Created by F4817273 on 2016/09/22.
 */
public interface CustomerPaymentDao extends GenericDao<CustomerPayment> {
   List<CustomerPayment> findByCustomerIdNumber(String customerIdNumber) throws EcgDAOException;
}
