package za.org.ecg.dao;

import za.org.ecg.dao.entity.Room;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.Date;
import java.util.List;

/**
 * Created by F4817273 on 2016/09/22.
 */
public interface RoomDao extends GenericDao<Room> {

    List<Room> getAllAvailableRooms(Date checkinDate, Date checkoutDate) throws EcgDAOException;

    List<Room> getAllAvailableRooms(Date checkinDate, Date checkoutDate,int numChildren,int numAdults) throws EcgDAOException;

    List<Room> getTopListedRooms() throws  EcgDAOException;

    List<Room> getRoomsOnSpecial() throws  EcgDAOException;

}
