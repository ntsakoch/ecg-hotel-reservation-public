/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.org.ecg.dao.hibernate;

import java.io.File;
import java.net.URI;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author Ntsako
 */
public class HibernateSessionUtil {

    private static final Logger LOG = Logger.getLogger(HibernateSessionUtil.class);
    private boolean initialized;
    private SessionFactory sessionFactory;
    private final String HIBERNATE_CONFIG_FILE = "/za/org/ecg/db/hibernate/hibernate.cfg.xml";
    private static HibernateSessionUtil INSTANCE;

    private HibernateSessionUtil() {
        init();
    }

    public static HibernateSessionUtil getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new HibernateSessionUtil();
        }
        return INSTANCE;
    }

    private void init() {
        try {
            if (!initialized) {
                LOG.info("Initializing hibernate session factory....");
                Configuration configuration = new Configuration();
                URI fileURI = getClass().getResource(HIBERNATE_CONFIG_FILE).toURI();
                File configFile = new File(fileURI);
                sessionFactory = configuration.configure(configFile).buildSessionFactory();
                initialized = true;
                LOG.info("Done Intializing hibernate session factory!");
            }
        } catch (Exception ex) {
            LOG.error("An error occured while initializing hibernate session factory: ", ex);
        }

    }

    public SessionFactory getSessionFactory() {
        if (initialized) {
            return sessionFactory;
        }
        throw new IllegalStateException("Not initialized!");
    }

    public Session getCurrentSession() {
         return this.sessionFactory.getCurrentSession();
    }
   public Session openNewSession()
   {
     return this.sessionFactory.openSession();  
   }
   
    public void destroySession() {
        if (initialized && !sessionFactory.isClosed()) {
            sessionFactory.close();
            sessionFactory = null;
            INSTANCE = null;
            LOG.info("Hibernate session destroyed.");
        }
    }

    public boolean isInitialized() {
        return initialized;
    }


}
