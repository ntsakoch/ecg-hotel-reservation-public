package za.org.ecg.dao.entity;

import za.org.ecg.dao.cache.EnableEntityCaching;
import za.org.ecg.dao.impl.ApplicationPermissionDaoImpl;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import static za.org.ecg.dao.entity.EntityBean.CATALOG;

/**
 *
 * @author F4817273
 */

@Entity
@Table(name="application_permissions"
    ,catalog=CATALOG
)
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "ApplicationPermission.findAll", query = "SELECT a FROM ApplicationPermission a"),
        @NamedQuery(name = "ApplicationPermission.findByApplicationPermissionId", query = "SELECT a FROM ApplicationPermission a WHERE a.applicationPermissionId = :applicationPermissionId"),
        @NamedQuery(name = "ApplicationPermission.findByPermission", query = "SELECT a FROM ApplicationPermission a WHERE a.permission = :permission"),
        @NamedQuery(name = "ApplicationPermission.findByDescription", query = "SELECT a FROM ApplicationPermission a WHERE a.description = :description")})
@EnableEntityCaching(daoImplementer = ApplicationPermissionDaoImpl.class)
public class ApplicationPermission  implements EntityBean {

     private static final long serialVersionUID = 1L;

     private Integer applicationPermissionId;
     private String permission;
     private String description;

    public ApplicationPermission() {
    }
    public ApplicationPermission(int id) {
        this.applicationPermissionId=id;
    }
    public ApplicationPermission(String permission, String desc) {
        this.permission = permission;
       this.description = desc;
    }
   
    @Id @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="application_permission_id", unique=true, nullable=false)
    public Integer getApplicationPermissionId() {
        return this.applicationPermissionId;
    }
    
    public void setApplicationPermissionId(Integer applicationPermissionId) {
        this.applicationPermissionId = applicationPermissionId;
    }


    @Column(name="permission", nullable=false, length=10)
    public String getPermission() {
        return this.permission;
    }
    
    public void setPermission(String permission) {
        this.permission = permission;
    }

    
    @Column(name="description", nullable=false, length=100)
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String desc) {
        this.description = desc;
    }

    @Override
    public String toString() {
        return "ApplicationPermission{" +
                "applicationPermissionId=" + applicationPermissionId +
                ", permission='" + permission + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    @Override
    @Transient
    public Integer getId() {
        return applicationPermissionId;
    }
}


