package za.org.ecg.dao;

import za.org.ecg.dao.entity.Preference;

/**
 * Created by F4817273 on 2016/09/22.
 */
public interface PreferenceDao extends GenericDao<Preference> {
}
