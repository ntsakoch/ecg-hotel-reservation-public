package za.org.ecg.dao;

import za.org.ecg.dao.entity.Country;
import za.org.ecg.dao.exception.EcgDAOException;

/**
 * Created by Ntsako on 2017/01/18.
 */
public interface CountryDao extends GenericDao<Country> {
    Country findByCountryCode(String countryCode) throws EcgDAOException;
}
