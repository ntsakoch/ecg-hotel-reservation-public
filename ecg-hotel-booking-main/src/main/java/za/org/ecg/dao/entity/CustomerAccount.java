package za.org.ecg.dao.entity;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import static javax.persistence.GenerationType.IDENTITY;
import static za.org.ecg.dao.entity.EntityBean.CATALOG;
import java.io.Serializable;
import java.util.Set;

/**
 *
 * @author F4817273
 */

@Entity
@Table(name="customer_accounts"
    ,catalog=CATALOG
)
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "CustomerAccount.findByCustomerId", query = "SELECT c FROM CustomerAccount c WHERE c.customerId = :customerId"),
        @NamedQuery(name = "CustomerAccount.findByAvailableBalance", query = "SELECT c FROM CustomerAccount c WHERE c.availableBalance = :availableBalance"),
        @NamedQuery(name = "CustomerAccount.findByMinimumOutstandingBalance", query = "SELECT c FROM CustomerAccount c WHERE c.outstandingBalance >= :minimumOutstandingBalance")})

public class CustomerAccount  implements EntityBean {

     private Integer customerAccountId;
     private int customerId;
     private int currencyId;
     private Currency currency;
     private Customer customerDetails;
     private double availableBalance;
     private double outstandingBalance;
     private Set<CustomerPayment> payments;

    public CustomerAccount() {
    }
    public CustomerAccount(Integer customerAccountId) {
        this.customerAccountId=customerAccountId;
    }
    public CustomerAccount(int customerId, double availableBalance, double outstandingBalance) {
       this.customerId = customerId;
       this.availableBalance = availableBalance;
       this.outstandingBalance = outstandingBalance;
    }
   
    @Id @GeneratedValue(strategy=IDENTITY)
    @Column(name="customer_account_id", unique=true, nullable=false)
    public Integer getCustomerAccountId() {
        return this.customerAccountId;
    }
    
    public void setCustomerAccountId(Integer customerAccountId) {
        this.customerAccountId = customerAccountId;
    }

    @Column(name="customer_id", nullable=false,insertable = false,updatable = false)
    public int getCustomerId() {
        return this.customerId;
    }
    
    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    @Column(name="available_balance", nullable=false)
    public double getAvailableBalance() {
        return this.availableBalance;
    }
    
    public void setAvailableBalance(double availableBalance) {
        this.availableBalance = availableBalance;
    }

    @Column(name="outstanding_balance", nullable=false)
    public double getOutstandingBalance() {
        return this.outstandingBalance;
    }
    
    public void setOutstandingBalance(double outstandingBalance) {
        this.outstandingBalance = outstandingBalance;
    }

    @OneToMany(fetch = FetchType.LAZY,cascade = CascadeType.ALL,orphanRemoval = true)
    @JoinColumn(name = "customer_account_id")
    @Fetch(value = FetchMode.SUBSELECT)
    public Set<CustomerPayment> getPayments() {
        return payments;
    }

    public void setPayments(Set<CustomerPayment> payments) {
        this.payments = payments;
    }

    @Column(name="currency_id", nullable=false,updatable = false,insertable = false)
    public int getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(int currencyId) {
        this.currencyId = currencyId;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "currency_id")
    @Fetch(value = FetchMode.JOIN)
    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    @OneToOne(fetch = FetchType.EAGER,orphanRemoval = true,cascade = CascadeType.ALL)
    @JoinColumn(name = "customer_id")
    @Fetch(value = FetchMode.JOIN)
    public Customer getCustomerDetails() {
        return customerDetails;
    }

    public void setCustomerDetails(Customer customerDetails) {
        this.customerDetails = customerDetails;
    }

    @Override
    public String toString() {
        return "CustomerAccount{" +
                "customerAccountId=" + customerAccountId +
                ", customerId=" + customerId +
                ", currencyId=" + currencyId +
                ", currency=" + currency +
                ", availableBalance=" + availableBalance +
                ", outstandingBalance=" + outstandingBalance +
                '}';
    }
}


