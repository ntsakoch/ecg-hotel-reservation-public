package za.org.ecg.dao.impl;


import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;
import za.org.ecg.dao.CustomerBookingDao;
import za.org.ecg.dao.entity.CustomerBooking;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.*;

/**
 * Created by F4817273 on 2016/09/23.
 */
@Repository("customerBookingDao")
public class CustomerBookingDaoImpl extends AbstractDao<CustomerBooking> implements CustomerBookingDao {
    private static final String FIND_BY_OUTSTANDING_BOOKINGS_QUERY ="SELECT  customer_booking_id,'cid'\n" +
            "FROM customer_bookings a\n" +
            "INNER JOIN customer_accounts AS b ON a.customer_id= b.customer_id\n" +
            "WHERE b.outstanding_balance >=:minimumOutstanding\n";

    private static final String FIND_BY_IDNUMBER_QUERY="SELECT a.customer_booking_id,'bid',a.customer_id,'cid',c.user_id,'uid' FROM customer_bookings a\n" +
            "INNER JOIN customers AS b ON a.customer_id=b.customer_id\n" +
            "INNER JOIN users AS c ON b.user_id=c.user_id\n"+
            "AND c.identity_no=:id_number";

    public CustomerBookingDaoImpl() {
        super(CustomerBooking.class);
    }

   @Override
    public List<CustomerBooking> findByStatus(String status) throws EcgDAOException {
       Map<String,Object> params=new HashMap<>();
       params.put("status",status);
        return findByNamedQuery("CustomerBooking.findByStatus",params);
    }

    @Override
    public List<CustomerBooking> findByCheckinDate(Date date) throws EcgDAOException{
        return findByDate(date,"checkinDate");
    }

    @Override
    public List<CustomerBooking> findByCheckoutDate(Date date) throws EcgDAOException {
        return findByDate(date,"checkoutDate");
    }

    @Override
    public List<CustomerBooking> findByCheckoutDateRange(Date from, Date to) throws EcgDAOException {
        Map<String,Object> params=new HashMap<>();
        params.put("fromDate",from);
        params.put("toDate",to);
        return findByNamedQuery("CustomerBooking.findByCheckoutDateRange",params);
    }

    @Override
    public List<CustomerBooking> findByCheckinDateRange(Date from, Date to) throws EcgDAOException {
        Map<String,Object> params=new HashMap<>();
        params.put("fromDate",from);
        params.put("toDate",to);
        return findByNamedQuery("CustomerBooking.findByCheckinDateRange",params);
    }

    @Override
    public List<CustomerBooking> findByDateCreationRange(Date from, Date to) throws EcgDAOException {
        Map<String,Object> params=new HashMap<>();
        params.put("fromDate",from);
        params.put("toDate",to);
        return findByNamedQuery("CustomerBooking.findByDateCreationRange",params);
    }

    @Override
    public List<CustomerBooking> findByCustomerIdNumber(String idNumber) throws EcgDAOException {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("id_number", idNumber);
            List<Object[]> result = executeSQLQuery(FIND_BY_IDNUMBER_QUERY, params);
            return findBySQLQueryResult(result, 0);
    }

    @Override
    public List<CustomerBooking> findAllOutstandingBookings(double minimumAmount) throws EcgDAOException {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("minimumOutstanding", minimumAmount);
            List<Object[]> result = executeSQLQuery(FIND_BY_OUTSTANDING_BOOKINGS_QUERY, params);
            return findBySQLQueryResult(result, 0);

    }

}
