/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package za.org.ecg.dao.impl;

import org.springframework.stereotype.Repository;
import za.org.ecg.dao.entity.AdminUser;
import za.org.ecg.dao.AdminUserDao;

/**
 *
 * @author F4817273
 */
@Repository("adminUserDao")
public class AdminUserDaoImpl extends AbstractDao<AdminUser> implements AdminUserDao {

    public AdminUserDaoImpl() {
        super(AdminUser.class);
    }

}
