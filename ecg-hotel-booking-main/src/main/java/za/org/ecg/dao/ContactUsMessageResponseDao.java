package za.org.ecg.dao;

import za.org.ecg.dao.entity.ContactUsMessageResponse;
import za.org.ecg.dao.exception.EcgDAOException;

import java.util.List;

/**
 * Created by F4817273 on 2016/09/22.
 */
public interface ContactUsMessageResponseDao extends GenericDao<ContactUsMessageResponse> {
    List<ContactUsMessageResponse> findByOriginalContactUsMessageId(int originalMessageId) throws EcgDAOException;
}
