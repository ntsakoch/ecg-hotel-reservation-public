package za.org.ecg.dao.impl;

import org.springframework.stereotype.Repository;
import za.org.ecg.dao.FeatureCategoryDao;
import za.org.ecg.dao.entity.FeatureCategory;

/**
 * Created by root on 2016/09/28.
 */
@Repository("featureCategoryDao")
public class FeatureCategoryDaoImpl extends AbstractDao<FeatureCategory> implements FeatureCategoryDao {
    public FeatureCategoryDaoImpl() {
        super(FeatureCategory.class);
    }
}
