package za.org.ecg.dao.impl;

import org.springframework.stereotype.Repository;
import za.org.ecg.dao.RoomFeatureDao;
import za.org.ecg.dao.entity.RoomFeature;

/**
 * Created by root on 2016/09/28.
 */
@Repository("roomFeatureDao")
public class RoomFeatureDaoImpl extends AbstractDao<RoomFeature> implements RoomFeatureDao {

    public RoomFeatureDaoImpl() {
        super(RoomFeature.class);
    }

}
