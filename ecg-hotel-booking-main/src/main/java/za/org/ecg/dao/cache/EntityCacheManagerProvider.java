package za.org.ecg.dao.cache;

/**
 * Created by Ntsako on 2016/11/11.
 */
public class EntityCacheManagerProvider {

    private final static EntityCacheManagerProvider instance=new EntityCacheManagerProvider();
    private EntityCacheManager cacheManager;

    private EntityCacheManagerProvider(){}

    public  static EntityCacheManagerProvider getInstance()
    {
        return  instance;
    }

    public EntityCacheManager getCacheManager()
    {
        return this.cacheManager==null? DefaultEntityCacheManager.getInstance(): this.cacheManager;
    }
    public void setCacheManager(EntityCacheManager cacheManager)
    {
        this.cacheManager=cacheManager;
    }
}
