package za.org.ecg.dao;

import za.org.ecg.dao.entity.UserCredential;

/**
 * Created by F4817273 on 2016/09/05.
 */
public interface UserCredentialDao extends GenericDao<UserCredential> {
}
