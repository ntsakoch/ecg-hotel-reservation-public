package za.org.ecg.test.spring.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Ntsako on 2016/12/23.
 */
@Configuration
@ComponentScan(basePackages = {"za.org.ecg.springmvc.service"})
public class ServiceBeanConfiguration {
}
