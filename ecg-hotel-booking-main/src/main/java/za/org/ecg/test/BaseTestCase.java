package za.org.ecg.test;

import junit.framework.TestCase;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import za.org.ecg.ECGHotelBooking;
import za.org.ecg.dao.cache.EntityCacheException;
import za.org.ecg.dao.cache.EntityCacheManagerProvider;
import za.org.ecg.springmvc.configuration.DataSourceConfiguration;
import za.org.ecg.springmvc.configuration.ProxyInterceptorConfiguration;
import za.org.ecg.test.spring.configuration.ServiceBeanConfiguration;

/**
 * Created by Ntsako on 2016/12/20.
 */
public class BaseTestCase extends TestCase{

    private static ApplicationContext applicationContext;
    static {
        DOMConfigurator.configure(ECGHotelBooking.class.getResource("/za/org/ecg/log4j.xml"));
       applicationContext=new AnnotationConfigApplicationContext(ProxyInterceptorConfiguration.class, ServiceBeanConfiguration.class,DataSourceConfiguration.class);
        try {
           // EntityCacheManagerProvider.getInstance().getCacheManager().init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @BeforeClass
    public  static void setUpClass() {

    }

    @Before
    public void setup() {

    }

    @After
    public void tearDown() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    protected  Object getBean(Class<?> beanClass )
    {
        if(applicationContext!=null)
        {
            return applicationContext.getBean(beanClass);
        }
        return null;
    }
    protected  Object getBean(String beanNAme )
    {
        if(applicationContext!=null)
        {
            return applicationContext.getBean(beanNAme);
        }
        return null;
    }
}
